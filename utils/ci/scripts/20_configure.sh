#!/bin/bash

################################################################################
# Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# This script configures the cmake project with its dependencies
################################################################################

# joins arguments using the cmake list separator (;)
function join_paths()
{
  local IFS=\;
  echo "$*"
}

MYDIR="$(dirname "$(readlink -f $0)")"

# wipe build and dist directories and pyOpenPASS results
cd "$MYDIR/../../../.." || exit 1
rm -rf build dist

mkdir -p "$MYDIR/../../../../build"
cd "$MYDIR/../../../../build" || exit 1

DEPS=(
  "$PWD/../deps/direct_deploy/units"
  "$PWD/../deps/direct_deploy/mantleapi"
  "$PWD/../deps/direct_deploy/stochastics"
  "$PWD/../deps/direct_deploy/yase"
  "$PWD/../deps/direct_deploy/openscenario_api"

)

# preparations for building on Windows/MSYS
if [[ "${OSTYPE}" = "msys" ]]; then
  # set the correct CMake generator
  CMAKE_GENERATOR_ARG="-GMSYS Makefiles"

  # prepare dependency paths
  # it seems cmake doesn't like MSYS paths starting with drive letters (e.g. /c/thirdParty/...)
  # cygpath is used here to format the paths in "mixed format" (e.g. c:/thirdparty/...)
  # also, add the mingw64 base path to allow resolving of runtime dependencies during install
  OLD_DEPS=(${DEPS[@]})
  OLD_DEPS+=("/mingw64")
  DEPS=()
  for DEP in "${OLD_DEPS[@]}"; do
    DEPS+=("$(cygpath -a -m ${DEP})")
  done
fi

cmake --version
cmake \
  "$CMAKE_GENERATOR_ARG" \
  -D CMAKE_PREFIX_PATH="$(join_paths ${DEPS[@]})" \
  -D CMAKE_INSTALL_PREFIX="$PWD/../dist/openScenarioEngine" \
  -D CMAKE_BUILD_TYPE=Release \
  -D USE_CCACHE=OFF \
  "$MYDIR/../../../engine"
