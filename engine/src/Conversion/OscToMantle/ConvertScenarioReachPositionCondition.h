/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Execution/i_environment.h>
#include <openScenarioLib/generated/v1_3/api/ApiClassInterfacesV1_3.h>

#include <memory>
#include <string>
#include <utility>

#include "Conversion/OscToMantle/ConvertScenarioPosition.h"

namespace OpenScenarioEngine::v1_3
{
struct ReachPositionCondition
{
  double tolerance{};
  std::optional<mantle_api::Pose> pose;
};

inline ReachPositionCondition ConvertScenarioReachPositionCondition(
    const std::shared_ptr<mantle_api::IEnvironment>& environment,
    const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IReachPositionCondition>& reachPositionCondition)
{
  return {
      reachPositionCondition->GetTolerance(),
      ConvertScenarioPosition(environment, reachPositionCondition->GetPosition())

  };
}

}  // namespace OpenScenarioEngine::v1_3