/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <openScenarioLib/generated/v1_3/api/EnumerationsV1_3.h>

#include <functional>

namespace OpenScenarioEngine::v1_3
{
/// Definition of condition state evaluation
enum class ConditionEdge
{
  kRising,
  kFalling,
  kRisingOrFalling,
  kNone
};

/// Evaluator for configure condition edges
class ConditionEdgeEvaluator
{
public:
  /// Constructs an ConditonEdgeEvaluator
  ///
  /// \param conditionEdge defines, how state transitions or levels shall be evaluated
  explicit ConditionEdgeEvaluator(ConditionEdge conditionEdge);

  /// Checks if the configured condition edge is satisfied
  ///
  /// \param state  current state of condition
  /// \returns true if condition matches specified condition edge
  bool is_satisfied(bool state);

private:
  std::function<void(bool)> EvalFunction(ConditionEdge conditionEdge);

  std::function<void(bool)> update_evaluated_state_;  ///!< function for updating internal state
  bool evaluated_state_{false};                       ///!< result of last evaluation
  bool last_state_{false};                            ///!< last state
};
}  // namespace OpenScenarioEngine::v1_3
