/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/action_node.h>

#include <cassert>
#include <utility>

#include "Storyboard/MotionControlAction/LaneOffsetAction_impl.h"
#include "Storyboard/MotionControlAction/MotionControlAction.h"
#include "Utils/EntityBroker.h"

namespace OpenScenarioEngine::v1_3::Node
{
class LaneOffsetAction : public yase::ActionNode
{
public:
  LaneOffsetAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ILaneOffsetAction> laneOffsetAction)
      : yase::ActionNode{"LaneOffsetAction"},
        laneOffsetAction_{laneOffsetAction}
  {
  }

  void onInit() override{};

private:
  yase::NodeStatus tick() override
  {
    assert(impl_);
    const auto is_finished = impl_->Step();
    return is_finished ? yase::NodeStatus::kSuccess : yase::NodeStatus::kRunning;
  };

  void lookupAndRegisterData(yase::Blackboard& blackboard) final
  {
    auto environment = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");
    const auto entityBroker = blackboard.get<EntityBroker::Ptr>("EntityBroker");

    impl_ = std::make_unique<OpenScenarioEngine::v1_3::MotionControlAction<OpenScenarioEngine::v1_3::LaneOffsetAction>>(
        OpenScenarioEngine::v1_3::LaneOffsetAction::Values{
            entityBroker->GetEntities(),
            laneOffsetAction_->GetContinuous(),
            ConvertScenarioLaneOffsetActionDynamics(laneOffsetAction_->GetLaneOffsetActionDynamics()),
            [=]()
            { return ConvertScenarioLaneOffsetTarget(laneOffsetAction_->GetLaneOffsetTarget()); }},
        OpenScenarioEngine::v1_3::LaneOffsetAction::Interfaces{
            environment});
  }

  std::unique_ptr<OpenScenarioEngine::v1_3::MotionControlAction<OpenScenarioEngine::v1_3::LaneOffsetAction>> impl_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ILaneOffsetAction> laneOffsetAction_;
};

}  // namespace OpenScenarioEngine::v1_3::Node
