/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/action_node.h>

#include <cassert>
#include <utility>

#include "Storyboard/MotionControlAction/MotionControlAction.h"
#include "Storyboard/MotionControlAction/SynchronizeAction_impl.h"
#include "Utils/EntityBroker.h"

namespace OpenScenarioEngine::v1_3::Node
{
class SynchronizeAction : public yase::ActionNode
{
public:
  SynchronizeAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ISynchronizeAction> synchronizeAction)
      : yase::ActionNode{"SynchronizeAction"},
        synchronizeAction_{synchronizeAction}
  {
  }

  void onInit() override{};

private:
  yase::NodeStatus tick() override
  {
    assert(impl_);
    const auto is_finished = impl_->Step();
    return is_finished ? yase::NodeStatus::kSuccess : yase::NodeStatus::kRunning;
  };

  void lookupAndRegisterData(yase::Blackboard& blackboard) final
  {
    auto environment = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");
    const auto entityBroker = blackboard.get<EntityBroker::Ptr>("EntityBroker");

    impl_ = std::make_unique<OpenScenarioEngine::v1_3::MotionControlAction<OpenScenarioEngine::v1_3::SynchronizeAction>>(
        OpenScenarioEngine::v1_3::SynchronizeAction::Values{
            entityBroker->GetEntities(),
            synchronizeAction_->GetTargetTolerance(),
            synchronizeAction_->GetTargetToleranceMaster(),
            [=]()
            { return ConvertScenarioPosition(environment, synchronizeAction_->GetTargetPositionMaster()); },
            [=]()
            { return ConvertScenarioPosition(environment, synchronizeAction_->GetTargetPosition()); },
            ConvertScenarioFinalSpeed(environment, synchronizeAction_->GetFinalSpeed()),
            ConvertScenarioEntity(synchronizeAction_->GetMasterEntityRef())},
        OpenScenarioEngine::v1_3::SynchronizeAction::Interfaces{
            environment});
  }

  std::unique_ptr<OpenScenarioEngine::v1_3::MotionControlAction<OpenScenarioEngine::v1_3::SynchronizeAction>> impl_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ISynchronizeAction> synchronizeAction_;
};

}  // namespace OpenScenarioEngine::v1_3::Node
