/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>

#include <utility>

#include "Conversion/OscToMantle/ConvertScenarioCoordinateSystem.h"
#include "Conversion/OscToMantle/ConvertScenarioRelativeDistanceType.h"
#include "Conversion/OscToMantle/ConvertScenarioRule.h"
#include "Conversion/OscToMantle/ConvertScenarioTimeToCollisionConditionTarget.h"
#include "Utils/EntityBroker.h"

namespace OpenScenarioEngine::v1_3
{
struct TTCCalculation
{
  TTCCalculation(const std::shared_ptr<mantle_api::IEnvironment>& environment,
                 const mantle_api::IEntity& triggeringEntity)
      : environment_{environment},
        triggeringEntity_{triggeringEntity}
  {
  }

  /// @brief Calculates the time to collision (TTC) between an
  ///        Entity of interest (normally the triggering) and a target entity
  ///
  /// @param[in] entity  Entity to which the TTC shall be calculated
  /// @returns time to collision
  [[nodiscard]] units::time::second_t operator()(const Entity& entity);

  /// @brief Calculates the time to collision (TTC) between an
  ///       Entity of interest (normally the triggering) and a target position
  ///
  /// @param[in] pose     Pose to which the TTC shall be calculated
  /// @returns time to collision
  [[nodiscard]] units::time::second_t operator()(const std::optional<mantle_api::Pose>& pose);

  const std::shared_ptr<mantle_api::IEnvironment>& environment_;
  const mantle_api::IEntity& triggeringEntity_;
};

class TimeToCollisionCondition
{
public:
  struct Values
  {
    std::string triggeringEntity;
    bool alongRoute;
    bool freespace;
    TimeToCollisionConditionTarget timeToCollisionConditionTarget;
    CoordinateSystem coordinateSystem;
    RelativeDistanceType relativeDistanceType;
    Rule<double> rule;
  };
  struct Interfaces
  {
    std::shared_ptr<mantle_api::IEnvironment> environment;
  };

  TimeToCollisionCondition(Values values, Interfaces interfaces)
      : values{std::move(values)},
        mantle{std::move(interfaces)} {};

  [[nodiscard]] bool IsSatisfied() const;

private:
  Values values;
  Interfaces mantle;
};

}  // namespace OpenScenarioEngine::v1_3