/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>
#include <openScenarioLib/generated/v1_3/impl/ApiClassImplV1_3.h>

#include "Conversion/OscToMantle/ConvertScenarioRelativeTargetLane.h"

using namespace units::literals;
using namespace mantle_api;

TEST(ConvertScenarioRelativeTargetLaneTest, GivenNoRelativeEntity_ThenThrowError)
{
  auto namedRef = std::make_shared<NET_ASAM_OPENSCENARIO::INamedReference<NET_ASAM_OPENSCENARIO::v1_3::IEntity>>();
  auto relativeTargetLane = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::RelativeTargetLaneImpl>();
  relativeTargetLane->SetEntityRef(namedRef);
  auto mockEnvironment = std::make_shared<mantle_api::MockEnvironment>();
  auto uniqueId = OpenScenarioEngine::v1_3::ConvertScenarioRelativeTargetLane(mockEnvironment, relativeTargetLane);
  EXPECT_EQ(mantle_api::UniqueId(0), uniqueId);
}
