/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioLongitudinalDistance.h"

#include <stdexcept>

#include "Conversion/OscToMantle/ConvertScenarioEntity.h"
#include "Utils/EntityUtils.h"

namespace OpenScenarioEngine::v1_3
{
LongitudinalDistance ConvertScenarioLongitudinalDistance(const std::shared_ptr<mantle_api::IEnvironment>& environment,
                                                         bool isSetDistance,
                                                         double longitudinalDistance,
                                                         bool isSetTimeGap,
                                                         double longitudinalTimeGap,
                                                         const std::shared_ptr<NET_ASAM_OPENSCENARIO::INamedReference<NET_ASAM_OPENSCENARIO::v1_3::IEntity>>& entityRef)
{
  if (isSetDistance)
  {
    return longitudinalDistance;
  }

  if (isSetTimeGap)
  {
    const auto& refEntity = EntityUtils::GetEntityByName(environment, ConvertScenarioEntity(entityRef));
    return longitudinalTimeGap * refEntity.GetVelocity().Length().value();
  }

  throw std::runtime_error(R"(Warning: LongitudinalDistanceAction: please set either "distance" or "timegap")");
}
}  // namespace OpenScenarioEngine::v1_3