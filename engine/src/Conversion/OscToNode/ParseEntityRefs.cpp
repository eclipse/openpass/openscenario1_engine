/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToNode/ParseEntityRefs.h"

#include <algorithm>
#include <iterator>

#include "Conversion/OscToMantle/ConvertScenarioEntity.h"

namespace OpenScenarioEngine::v1_3
{
std::vector<std::string> parse(std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IEntityRef>> entityRefs)
{
  std::vector<std::string> transformedEntities;
  entityRefs.reserve(entityRefs.size());
  std::transform(
      begin(entityRefs),
      end(entityRefs),
      std::back_inserter(transformedEntities),
      [](const auto &e) { return ConvertScenarioEntity(e); });
  return transformedEntities;
}

}  // namespace OpenScenarioEngine::v1_3