/*******************************************************************************
 * Copyright (c) 2025 Ansys, Inc.
 * Copyright (c) 2025 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Node/EventNode.h"

namespace OpenScenarioEngine::v1_3::Node
{

EventNode::EventNode(const std::string& name,
                     yase::BehaviorNode::Ptr actions,
                     yase::BehaviorNode::Ptr start_trigger,
                     EventPriority priority)
    : TriggerableCompositeNode{name}, event_priority_{priority}, start_trigger_{start_trigger}
{
  set(std::move(actions), StopTriggerPtr{nullptr}, StartTriggerPtr{std::move(start_trigger)});
}

void EventNode::lookupAndRegisterData(yase::Blackboard& blackboard)
{
  event_prioritizer_ = blackboard.get<std::shared_ptr<OpenScenarioEngine::v1_3::IEventPrioritizer> >("EventPrioritizer");
  event_prioritizer_->RegisterEvent(name(), event_priority_);
}

yase::NodeStatus EventNode::tick()
{
  if (event_prioritizer_->ShouldStopChild(name()) || event_prioritizer_->ShouldSkipChild(name()))
  {
    return yase::NodeStatus::kSuccess;
  }

  auto current_status = TriggerableCompositeNode::tick();

  if (start_trigger_ == nullptr || start_trigger_->status() == yase::NodeStatus::kSuccess)
  {
    event_prioritizer_->EventStarted(name());
  }

  return current_status;
}

}  // namespace OpenScenarioEngine::v1_3::Node
