/********************************************************************************
 * Copyright (c) 2023-2025 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Common/i_identifiable.h>
#include <MantleAPI/Traffic/i_controller.h>

#include <map>
#include <memory>
#include <optional>
#include <stdexcept>
#include <unordered_map>
#include <utility>
#include <vector>

namespace OpenScenarioEngine::v1_3
{
/// List of user defined controllers referenced by unique_id
using UserDefinedControllers = std::map<mantle_api::UniqueId, mantle_api::IController*>;

/// Bundle default (internal) and user defined controllers of an entity
struct EntityControllers
{
  std::pair<mantle_api::UniqueId, mantle_api::IController*> internal{};  ///< internal controller (mandatory)
  UserDefinedControllers user_defined{};                                 ///< list of potential user defined controllers
};

/// Internally used housekeeping of assigned controllers
using Controllers = std::map<mantle_api::UniqueId, EntityControllers>;

class IControllerService
{
public:
  virtual ~IControllerService() = default;

  /// Retrieves the controllers of a given entity, if available
  ///
  /// @param entity_id entity of interest
  /// @returns registered controllers if available, or std::nullopt if not
  [[nodiscard]] virtual std::optional<EntityControllers> GetControllers(
      mantle_api::UniqueId entity_id) const = 0;

  /// Returns all controller ids of the entity
  /// @param entity_id referenced entity
  /// @return all controllers as vector
  virtual std::vector<mantle_api::UniqueId> GetControllerIds(mantle_api::UniqueId entity_id) = 0;

  /// Retrieve a controller id from the registered controllers of an entity
  ///
  /// @note Call only, if user defined controllers are available. Otherwise it throws.
  /// @param controller_ref     If set, the user defined controllers the given entity_controllers
  ///                           are scanned for the corresponding reference. Throws if not available.
  ///                           If omitted (compatibility with openSCENARIO 1.1) one and only one
  ///                           user defined controller must be registered.
  /// @param entity_controllers Controllers of a specific entity
  /// @returns Unique id of the registered controller
  [[nodiscard]] virtual mantle_api::UniqueId GetControllerId(
      std::optional<std::string> controller_ref,
      const EntityControllers& entity_controllers) const = 0;

  /// Change controller state for a given entity
  /// @param entity_id     referenced entity
  /// @param controller_id referenced controller
  /// @param lateral       desired state of the corresponding motion domain
  /// @param longitudinal  desired state of the corresponding motion domain
  virtual void ChangeState(
      mantle_api::UniqueId entity_id,
      mantle_api::UniqueId controller_id,
      mantle_api::IController::LateralState lateral,
      mantle_api::IController::LongitudinalState longitudinal) = 0;

  /// Resets internal state of the controller service by clearing mapping
  /// between entity Ids and its controllers and also between controller Ids
  /// and references.
  virtual void ResetControllerMappings() = 0;
};

/// Pointer type of internally used housekeeping container
using ControllerServicePtr = std::shared_ptr<IControllerService>;

}  // namespace OpenScenarioEngine::v1_3
