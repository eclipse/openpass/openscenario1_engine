/*******************************************************************************
 * Copyright (c) 2023-2025 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <gmock/gmock.h>
#include <Stochastics/StochasticsInterface.h>

namespace OpenScenarioEngine::v1_3
{
class MockStochastics : public StochasticsInterface
{
public:
  MOCK_METHOD(double, GetUniformDistributed, (double a, double b), (override));
  MOCK_METHOD(int, GetBinomialDistributed, (int upperRangeNum, double probSuccess), (override));
  MOCK_METHOD(double, GetNormalDistributed, (double mean, double stdDeviation), (override));
  MOCK_METHOD(double, GetExponentialDistributed, (double lambda), (override));
  MOCK_METHOD(double, GetGammaDistributed, (double mean, double stdDeviation), (override));
  MOCK_METHOD(double, GetGammaDistributedShapeScale, (double shape, double scale), (override));
  MOCK_METHOD(double, GetLogNormalDistributed, (double mean, double stdDeviation), (override));
  MOCK_METHOD(double, GetLogNormalDistributedMuSigma, (double mu, double sigma), (override));
  MOCK_METHOD(double, GetSpecialDistributed, (std::string distributionName, std::vector<double> args), (override));
  MOCK_METHOD(double, GetRandomCdfLogNormalDistributed, (double mean, double stdDeviation), (override));
  MOCK_METHOD(double, GetPercentileLogNormalDistributed, (double mean, double stdDeviation, double probability), (override));
  MOCK_METHOD(double, SampleFromDiscreteDistribution, (const DiscreteDistributionInformation* distribution), (override));
  MOCK_METHOD(DiscreteDistributionInformation*, ConditionOnRandomVariables, (const DiscreteDistributionInformation* distribution, std::vector<double> conditions, int dimUnconditionedVariable), (override));
  MOCK_METHOD(std::uint32_t, GetRandomSeed, (), (const, override));
  MOCK_METHOD(void, ReInit, (), (override));
  MOCK_METHOD(void, InitGenerator, (std::uint32_t seed), (override));
  MOCK_METHOD(bool, Instantiate, (std::string), (override));

  static inline std::shared_ptr<MockStochastics> make_shared()
  {
    return std::make_shared<MockStochastics>();
  }
};

}  // namespace testing::OpenScenarioEngine::v1_3