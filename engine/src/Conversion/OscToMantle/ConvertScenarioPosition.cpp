/********************************************************************************
 * Copyright (c) 2021-2025 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioPosition.h"

#include <openScenarioLib/generated/v1_3/api/ApiClassInterfacesV1_3.h>

#include "Utils/Logger.h"

namespace OpenScenarioEngine::v1_3
{
namespace detail
{
template <class... Ts>
struct overloaded : Ts...
{
  using Ts::operator()...;
};
template <class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;

void FillOrientation(mantle_api::IEnvironment& environment,
                     const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IOrientation>& orientation_ptr,
                     const mantle_api::Position& position,
                     mantle_api::Pose& pose)
{
  if (orientation_ptr != nullptr)
  {
    auto scenario_orientation = mantle_api::Orientation3<units::angle::radian_t>{units::angle::radian_t(orientation_ptr->GetH()),
                                                                                 units::angle::radian_t(orientation_ptr->GetP()),
                                                                                 units::angle::radian_t(orientation_ptr->GetR())};

    if (orientation_ptr->GetType() == NET_ASAM_OPENSCENARIO::v1_3::ReferenceContext::RELATIVE)
    {
      mantle_api::Orientation3<units::angle::radian_t> track_orientation =
          std::visit(
              overloaded{
                  [&environment](const mantle_api::OpenDriveRoadPosition& road_pos)
                  { return environment.GetConverter()->GetRoadOrientation(road_pos); },
                  [&environment](const mantle_api::OpenDriveLanePosition& lane_pos)
                  { return environment.GetConverter()->GetLaneOrientation(lane_pos); },
                  [&environment, &pose](const mantle_api::LatLonPosition&)
                  { return environment.GetQueryService().GetLaneOrientation(pose.position); },
                  [&environment, &pose](const mantle_api::Vec3<units::length::meter_t>&)
                  { return environment.GetQueryService().GetLaneOrientation(pose.position); },
              },
              position);

      pose.orientation = track_orientation + scenario_orientation;
    }
    else
    {
      pose.orientation = scenario_orientation;
    }
  }
}

mantle_api::Pose ConvertLanePosition(mantle_api::IEnvironment& environment,
                                     const NET_ASAM_OPENSCENARIO::v1_3::ILanePosition& lane_position)
{
  const mantle_api::OpenDriveLanePosition opendrive_lane_position{
      lane_position.GetRoadId(),
      atoi(lane_position.GetLaneId().c_str()),
      units::length::meter_t(lane_position.GetS()),
      units::length::meter_t(lane_position.GetOffset())};

  mantle_api::Pose pose{};
  pose.position = environment.GetConverter()->Convert(opendrive_lane_position);
  detail::FillOrientation(environment, lane_position.GetOrientation(), opendrive_lane_position, pose);
  return pose;
}

mantle_api::Pose ConvertRoadPosition(mantle_api::IEnvironment& environment,
                                     const NET_ASAM_OPENSCENARIO::v1_3::IRoadPosition& road_position)
{
  const mantle_api::OpenDriveRoadPosition opendrive_road_position{
      road_position.GetRoadId(),
      units::length::meter_t(road_position.GetS()),
      units::length::meter_t(road_position.GetT())};

  mantle_api::Pose pose{};
  pose.position = environment.GetConverter()->Convert(opendrive_road_position);
  detail::FillOrientation(environment, road_position.GetOrientation(), opendrive_road_position, pose);
  return pose;
}

mantle_api::Pose ConvertGeoPosition(mantle_api::IEnvironment& environment,
                                    const NET_ASAM_OPENSCENARIO::v1_3::IGeoPosition& geo_position)
{
  mantle_api::LatLonPosition lat_lon_position = ConvertToMantleLatLonPosition(geo_position);

  mantle_api::Pose pose{};
  pose.position = environment.GetConverter()->Convert(lat_lon_position);
  if (environment.GetQueryService().IsPositionOnLane(pose.position))  // 2D check
  {
    // Change z position to be on road surface of top-most road
    pose.position.z = environment.GetQueryService().GetLaneHeightAtPosition(pose.position);
  }
  detail::FillOrientation(environment, geo_position.GetOrientation(), lat_lon_position, pose);
  return pose;
}

mantle_api::Pose ConvertWorldPosition(const NET_ASAM_OPENSCENARIO::v1_3::IWorldPosition& world_position)
{
  mantle_api::Pose pose{};
  pose.position.x = units::length::meter_t{world_position.GetX()};
  pose.position.y = units::length::meter_t{world_position.GetY()};
  pose.position.z = units::length::meter_t{world_position.GetZ()};
  // There is only an absolute orientation for a world position according to the standard
  pose.orientation.yaw = units::angle::radian_t{world_position.GetH()};
  pose.orientation.pitch = units::angle::radian_t{world_position.GetP()};
  pose.orientation.roll = units::angle::radian_t{world_position.GetR()};
  return pose;
}

mantle_api::Pose ConvertRelativeLanePosition(
    mantle_api::IEnvironment& environment,
    const NET_ASAM_OPENSCENARIO::v1_3::IRelativeLanePosition& relative_lane_position)
{
  mantle_api::Pose pose{};
  if (relative_lane_position.IsSetDs())
  {
    Logger::Error("ConvertRelativeLanePosition: \"ds\" is not implemented for RelativeLanePosition.");
    return pose;
  }

  if (!relative_lane_position.IsSetDsLane())
  {
    Logger::Error("ConvertRelativeLanePosition: \"dsLane\" is not set, required for RelativeLanePosition.");
    return pose;
  }

  const auto ref_entity_name = relative_lane_position.GetEntityRef()->GetNameRef();
  if (const auto& ref_entity = environment.GetEntityRepository().Get(ref_entity_name))
  {
    const auto d_lane = relative_lane_position.GetDLane();
    const auto ds_lane = relative_lane_position.GetDsLane();
    const auto offset = relative_lane_position.GetOffset();

    const mantle_api::Pose pose_ref{environment.GetGeometryHelper()->TranslateGlobalPositionLocally(
                                        ref_entity.value().get().GetPosition(),
                                        ref_entity.value().get().GetOrientation(),
                                        -ref_entity.value().get().GetProperties()->bounding_box.geometric_center),
                                    ref_entity.value().get().GetOrientation()};

    const auto calculated_pose = environment.GetQueryService().FindRelativeLanePoseAtDistanceFrom(
        pose_ref, d_lane, units::length::meter_t(ds_lane), units::length::meter_t(offset));
    if (calculated_pose.has_value())
    {
      pose.position = calculated_pose.value().position;
      detail::FillOrientation(environment, relative_lane_position.GetOrientation(), pose.position, pose);
      return pose;
    }

    Logger::Error("ConvertRelativeLanePosition: Conversion from RelativeLanePosition is not possible. Please check the scenario.");
  }
  else
  {
    Logger::Error("ConvertRelativeLanePosition: Entity with name = \"" + ref_entity_name + "\" for RelativeLanePosition does not exist. Please adjust the scenario.");
  }
  return pose;
}

}  // namespace detail

std::optional<mantle_api::Pose> ConvertScenarioPosition(const std::shared_ptr<mantle_api::IEnvironment>& environment_ptr,
                                                        const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IPosition>& pos)
{
  auto& environment = *environment_ptr;

  if (const auto& world_position = pos->GetWorldPosition())
  {
    return detail::ConvertWorldPosition(*world_position);
  }

  if (const auto& lane_position = pos->GetLanePosition())
  {
    return detail::ConvertLanePosition(environment, *lane_position);
  }

  if (const auto& road_position = pos->GetRoadPosition())
  {
    return detail::ConvertRoadPosition(environment, *road_position);
  }

  if (const auto& relative_lane_position = pos->GetRelativeLanePosition())
  {
    return detail::ConvertRelativeLanePosition(environment, *relative_lane_position);
  }

  if (auto geo_position = pos->GetGeoPosition())
  {
    return detail::ConvertGeoPosition(environment, *geo_position);
  }

  Logger::Error("ConvertScenarioPosition: Conversion from this type of OpenSCENARIO position to mantle API position not implemented yet");

  return {};
}

mantle_api::LatLonPosition ConvertToMantleLatLonPosition(const NET_ASAM_OPENSCENARIO::v1_3::IGeoPosition& geo_position)
{
  mantle_api::LatLonPosition lat_lon_position{};

  // lat/lon in degree has higher priority since lat/lon in rad was deprecated in OSC1.2
  if (geo_position.IsSetLatitudeDeg())
  {
    lat_lon_position.latitude = units::angle::degree_t(geo_position.GetLatitudeDeg());
  }
  else if (geo_position.IsSetLatitude())
  {
    lat_lon_position.latitude = units::angle::radian_t(geo_position.GetLatitude());
  }
  else
  {
    throw std::runtime_error("GeoPosition has neither latitudeDeg nor latitude defined. Please adjust the scenario.");
  }

  if (geo_position.IsSetLongitudeDeg())
  {
    lat_lon_position.longitude = units::angle::degree_t(geo_position.GetLongitudeDeg());
  }
  else if (geo_position.IsSetLongitude())
  {
    lat_lon_position.longitude = units::angle::radian_t(geo_position.GetLongitude());
  }
  else
  {
    throw std::runtime_error("GeoPosition has neither longitudeDeg nor longitude defined. Please adjust the scenario.");
  }

  return lat_lon_position;
}

}  // namespace OpenScenarioEngine::v1_3
