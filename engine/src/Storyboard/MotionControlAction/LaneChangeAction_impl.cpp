/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/MotionControlAction/LaneChangeAction_impl.h"

#include <MantleAPI/Common/position.h>
#include <MantleAPI/Traffic/control_strategy.h>
#include <MantleAPI/Traffic/entity_helper.h>
#include <units.h>

using namespace units::literals;

namespace OpenScenarioEngine::v1_3
{
void LaneChangeAction::SetControlStrategy()
{
  SetupControlStrategy();
  for (const auto& actor : values.entities)
  {
    if (auto entity = mantle.environment->GetEntityRepository().Get(actor))
    {
      mantle.environment->UpdateControlStrategies(
          entity.value().get().GetUniqueId(),
          {control_strategy_});
    }
    else
    {
      throw std::runtime_error("LaneChangeAction: Entity with name \"" + actor +
                               "\" does not exist. Please adjust the scenario.");
    }
  }
}

bool LaneChangeAction::HasControlStrategyGoalBeenReached(const std::string& actor)
{
  if (auto entity = mantle.environment->GetEntityRepository().Get(actor))
  {
    return mantle.environment->HasControlStrategyGoalBeenReached(
        entity.value().get().GetUniqueId(),
        control_strategy_->type);
  }

  return false;
}

void LaneChangeAction::SetupControlStrategy()
{
  control_strategy_->target_lane_offset = units::length::meter_t{values.targetLaneOffset};
  control_strategy_->transition_dynamics = values.laneChangeActionDynamics.transitionDynamics;
  control_strategy_->target_lane_id = static_cast<mantle_api::LaneId>(values.GetLaneChangeTarget());
}

mantle_api::MovementDomain LaneChangeAction::GetMovementDomain() const
{
  return control_strategy_->movement_domain;
}

}  // namespace OpenScenarioEngine::v1_3
