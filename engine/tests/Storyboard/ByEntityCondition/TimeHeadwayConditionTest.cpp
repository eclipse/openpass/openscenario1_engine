/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

#include "Conversion/OscToMantle/ConvertScenarioRule.h"
#include "Storyboard/ByEntityCondition/TimeHeadwayCondition_impl.h"
#include "TestUtils/TestLogger.h"
#include "Utils/Constants.h"

using namespace mantle_api;

using units::literals::operator""_m;
using units::literals::operator""_mps;

using testing::_;
using testing::HasSubstr;
using testing::Return;

class TimeHeadwayConditionTestFixture : public ::testing::Test
{
protected:
  void SetUp() override
  {
    LOGGER = std::make_unique<testing::OpenScenarioEngine::v1_3::TestLogger>();
    mock_environment_ = std::make_shared<MockEnvironment>();
  }

  std::shared_ptr<MockEnvironment> mock_environment_;
  OpenScenarioEngine::v1_3::TimeHeadwayCondition::Values condition_values_{.triggeringEntity = "Vehicle1",
                                                                           .alongRoute = false,
                                                                           .freespace = false,
                                                                           .entityRef = "Ego",
                                                                           .coordinateSystem = OpenScenarioEngine::v1_3::CoordinateSystem::kEntity,
                                                                           .relativeDistanceType = OpenScenarioEngine::v1_3::RelativeDistanceType::kLongitudinal,
                                                                           .rule = OpenScenarioEngine::v1_3::Rule(NET_ASAM_OPENSCENARIO::v1_3::Rule::RuleEnum::EQUAL_TO, 10.0)};
  std::unique_ptr<testing::OpenScenarioEngine::v1_3::TestLogger> LOGGER;
};

TEST_F(TimeHeadwayConditionTestFixture,
       GivenConditionWithUnsupportedRelativeDistanceTypeLateral_WhenEvaluate_ThenReturnsFalse)
{
  condition_values_.relativeDistanceType = OpenScenarioEngine::v1_3::RelativeDistanceType::kLateral;

  auto time_headway_condition = OpenScenarioEngine::v1_3::TimeHeadwayCondition(condition_values_,
                                                                               {mock_environment_});
  EXPECT_FALSE(time_headway_condition.IsSatisfied());

  EXPECT_THAT(LOGGER->LastLogLevel(), mantle_api::LogLevel::kError);
  EXPECT_THAT(LOGGER->LastLogMessage(), HasSubstr("RelativeDistanceType is not supported"));
}

TEST_F(TimeHeadwayConditionTestFixture,
       GivenConditionWithUnsupportedRelativeDistanceTypeEuclidianDistance_WhenEvaluate_ThenReturnsFalse)
{
  condition_values_.relativeDistanceType = OpenScenarioEngine::v1_3::RelativeDistanceType::kEuclidian_distance;

  auto time_headway_condition = OpenScenarioEngine::v1_3::TimeHeadwayCondition(condition_values_,
                                                                               {mock_environment_});
  EXPECT_FALSE(time_headway_condition.IsSatisfied());

  EXPECT_THAT(LOGGER->LastLogLevel(), mantle_api::LogLevel::kError);
  EXPECT_THAT(LOGGER->LastLogMessage(), HasSubstr("RelativeDistanceType is not supported"));
}

TEST_F(TimeHeadwayConditionTestFixture,
       GivenConditionWithUnsupportedCoordinateSystemTrajectory_WhenEvaluate_ThenReturnsFalse)
{
  condition_values_.coordinateSystem = OpenScenarioEngine::v1_3::CoordinateSystem::kTrajectory;

  auto time_headway_condition = OpenScenarioEngine::v1_3::TimeHeadwayCondition(condition_values_,
                                                                               {mock_environment_});
  EXPECT_FALSE(time_headway_condition.IsSatisfied());

  EXPECT_THAT(LOGGER->LastLogLevel(), mantle_api::LogLevel::kError);
  EXPECT_THAT(LOGGER->LastLogMessage(), HasSubstr("CoordinateSystem is not supported"));
}

TEST_F(TimeHeadwayConditionTestFixture,
       GivenConditionWithUnsupportedCoordinateSystemRoad_WhenEvaluate_ThenReturnsFalse)
{
  condition_values_.coordinateSystem = OpenScenarioEngine::v1_3::CoordinateSystem::kRoad;

  auto time_headway_condition = OpenScenarioEngine::v1_3::TimeHeadwayCondition(condition_values_,
                                                                               {mock_environment_});
  EXPECT_FALSE(time_headway_condition.IsSatisfied());

  EXPECT_THAT(LOGGER->LastLogLevel(), mantle_api::LogLevel::kError);
  EXPECT_THAT(LOGGER->LastLogMessage(), HasSubstr("CoordinateSystem is not supported"));
}

TEST_F(TimeHeadwayConditionTestFixture,
       GivenConditionWithCoordinateSystemLaneButCanNotCalculateDistance_WhenEvaluate_ThenThrowRuntimeError)
{
  condition_values_.coordinateSystem = OpenScenarioEngine::v1_3::CoordinateSystem::kLane;

  auto& mocked_query_service = dynamic_cast<const mantle_api::MockQueryService&>(mock_environment_->GetQueryService());

  EXPECT_CALL(mocked_query_service, GetLongitudinalLaneDistanceBetweenPositions(_, _))
      .Times(1)
      .WillRepeatedly(Return(std::nullopt));

  auto time_headway_condition = OpenScenarioEngine::v1_3::TimeHeadwayCondition(condition_values_,
                                                                               {mock_environment_});

  EXPECT_THROW(time_headway_condition.IsSatisfied(), std::runtime_error);
}

TEST_F(
    TimeHeadwayConditionTestFixture,
    GivenConditionWithTriggeringEntityVelocityZero_WhenEvaluate_ThenNoThrowRuntimeSatisfiedWithDefaultTimeHeadwayValue)
{
  using TriggeringEntityVelocity = mantle_api::Vec3<units::velocity::meters_per_second_t>;

  auto rule = OpenScenarioEngine::v1_3::Rule(NET_ASAM_OPENSCENARIO::v1_3::Rule::RuleEnum::EQUAL_TO, OpenScenarioEngine::v1_3::limits::kTimeHeadwayMax.value());
  condition_values_.rule = rule;
  condition_values_.coordinateSystem = OpenScenarioEngine::v1_3::CoordinateSystem::kEntity;

  auto& mocked_geo_helper = dynamic_cast<const mantle_api::MockGeometryHelper&>(*(mock_environment_->GetGeometryHelper()));
  auto& mocked_entity = dynamic_cast<mantle_api::MockVehicle&>(mock_environment_->GetEntityRepository().Get("").value().get());
  EXPECT_CALL(mocked_geo_helper, TranslateGlobalPositionLocally(_, _, _))
      .WillRepeatedly(Return(mantle_api::Vec3<units::length::meter_t>{10.0_m, 0_m, 0_m}));
  EXPECT_CALL(mocked_entity, GetVelocity())
      .WillOnce(Return(TriggeringEntityVelocity{0.0_mps, 0.0_mps, 0.0_mps}));

  auto time_headway_condition = OpenScenarioEngine::v1_3::TimeHeadwayCondition(condition_values_,
                                                                               {mock_environment_});

  EXPECT_EQ(time_headway_condition.IsSatisfied(), true);
}

using RelativeDistance = units::length::meter_t;
using TriggeringEntityVelocity = mantle_api::Vec3<units::velocity::meters_per_second_t>;
using Freespace = bool;
using ConditionValue = double;
using ExpectedSatisfiedStatus = bool;
using TimeHeadwayConditionCoordinateSystemTestInputType =
    std::tuple<RelativeDistance, TriggeringEntityVelocity, ConditionValue, ExpectedSatisfiedStatus>;

// clang-format off
const auto kTimeHeadwayConditionCoordinateSystemTestInputs =
    testing::Values(
      std::make_tuple(RelativeDistance{50_m}, TriggeringEntityVelocity{5.0_mps, 0.0_mps, 0.0_mps}, ConditionValue{3.0},  ExpectedSatisfiedStatus(false)),
      std::make_tuple(RelativeDistance{50_m}, TriggeringEntityVelocity{5.0_mps, 0.0_mps, 0.0_mps}, ConditionValue{10.0}, ExpectedSatisfiedStatus(true)),
      std::make_tuple(RelativeDistance{50_m}, TriggeringEntityVelocity{3.0_mps, 4.0_mps, 0.0_mps}, ConditionValue{10.0}, ExpectedSatisfiedStatus(true))
    );
const auto kFreespaceInputs = testing::Values(true, false);
// clang-format on

class TimeHeadwayConditionTestForCoordinateSystem
    : public TimeHeadwayConditionTestFixture,
      public testing::WithParamInterface<std::tuple<TimeHeadwayConditionCoordinateSystemTestInputType, Freespace>>
{
protected:
  void SetUp() override
  {
    TimeHeadwayConditionTestFixture::SetUp();

    relative_distance_ = std::get<0>(std::get<0>(GetParam()));
    triggering_entity_velocity_ = std::get<1>(std::get<0>(GetParam()));
    condition_value_ = std::get<2>(std::get<0>(GetParam()));
    expected_satisfied_status_ = std::get<3>(std::get<0>(GetParam()));
    freespace_ = std::get<1>(GetParam());

    condition_values_.freespace = freespace_;
  }

  RelativeDistance relative_distance_{};
  TriggeringEntityVelocity triggering_entity_velocity_{};
  Freespace freespace_{false};
  ConditionValue condition_value_{};
  ExpectedSatisfiedStatus expected_satisfied_status_{false};
};

class TimeHeadwayConditionTestForCoordinateSystemWithFreespaceTrue : public TimeHeadwayConditionTestForCoordinateSystem
{
};

class TimeHeadwayConditionTestForCoordinateSystemWithFreespaceFalse : public TimeHeadwayConditionTestForCoordinateSystem
{
};

INSTANTIATE_TEST_CASE_P(TimeHeadwayConditionTestForCoordinateSystemInputs,
                        TimeHeadwayConditionTestForCoordinateSystem,
                        testing::Combine(kTimeHeadwayConditionCoordinateSystemTestInputs, kFreespaceInputs));

INSTANTIATE_TEST_CASE_P(TimeHeadwayConditionTestForCoordinateSystemInputs,
                        TimeHeadwayConditionTestForCoordinateSystemWithFreespaceTrue,
                        testing::Combine(kTimeHeadwayConditionCoordinateSystemTestInputs, testing::Values(true)));

INSTANTIATE_TEST_CASE_P(TimeHeadwayConditionTestForCoordinateSystemInputs,
                        TimeHeadwayConditionTestForCoordinateSystemWithFreespaceFalse,
                        testing::Combine(kTimeHeadwayConditionCoordinateSystemTestInputs, testing::Values(false)));

TEST_P(TimeHeadwayConditionTestForCoordinateSystem,
       GivenCoordinateSystemIsEntityWithTestInputs_WhenEvaluate_ThenExpectCorrectSatisfiedStatus)
{
  auto rule = OpenScenarioEngine::v1_3::Rule(NET_ASAM_OPENSCENARIO::v1_3::Rule::RuleEnum::EQUAL_TO, condition_value_);
  condition_values_.rule = rule;
  condition_values_.coordinateSystem = OpenScenarioEngine::v1_3::CoordinateSystem::kEntity;

  auto& mocked_geo_helper = dynamic_cast<const mantle_api::MockGeometryHelper&>(*(mock_environment_->GetGeometryHelper()));
  auto& mocked_entity = dynamic_cast<mantle_api::MockVehicle&>(mock_environment_->GetEntityRepository().Get("").value().get());

  // Test though stubbed function TranslateGlobalPositionLocally that is used by both
  // CalculateLongitudinalFreeSpaceDistance and CalculateRelativeLongitudinalDistance
  EXPECT_CALL(mocked_geo_helper, TranslateGlobalPositionLocally(_, _, _))
      .WillRepeatedly(Return(mantle_api::Vec3<units::length::meter_t>{relative_distance_, 0_m, 0_m}));
  EXPECT_CALL(mocked_entity, GetVelocity()).WillOnce(Return(triggering_entity_velocity_));

  auto time_headway_condition = OpenScenarioEngine::v1_3::TimeHeadwayCondition(condition_values_,
                                                                               {mock_environment_});

  EXPECT_EQ(time_headway_condition.IsSatisfied(), expected_satisfied_status_);
}

TEST_P(TimeHeadwayConditionTestForCoordinateSystemWithFreespaceTrue,
       GivenCoordinateSystemIsLaneWithTestInputsAndFreespaceTrue_WhenEvaluate_ThenExpectCorrectSatisfiedStatus)
{
  auto rule = OpenScenarioEngine::v1_3::Rule(NET_ASAM_OPENSCENARIO::v1_3::Rule::RuleEnum::EQUAL_TO, condition_value_);
  condition_values_.rule = rule;
  condition_values_.coordinateSystem = OpenScenarioEngine::v1_3::CoordinateSystem::kLane;

  auto& mocked_query_service = dynamic_cast<const mantle_api::MockQueryService&>(mock_environment_->GetQueryService());
  auto& mocked_entity = dynamic_cast<mantle_api::MockVehicle&>(mock_environment_->GetEntityRepository().Get("").value().get());
  auto& mocked_geo_helper = dynamic_cast<const mantle_api::MockGeometryHelper&>(*(mock_environment_->GetGeometryHelper()));

  EXPECT_CALL(mocked_entity, GetVelocity()).WillOnce(Return(triggering_entity_velocity_));

  const auto headmost_corner = mantle_api::Vec3<units::length::meter_t>{3.0_m, 0_m, 0_m};
  const auto rearmost_corner = mantle_api::Vec3<units::length::meter_t>{-2.0_m, 0_m, 0_m};
  const auto other_corner = mantle_api::Vec3<units::length::meter_t>{0.0_m, 0_m, 0_m};
  const auto entity_length = units::math::abs(headmost_corner.x) + units::math::abs(rearmost_corner.x);

  EXPECT_CALL(mocked_query_service, GetLongitudinalLaneDistanceBetweenPositions(_, _))
      .WillOnce(Return(relative_distance_ + entity_length));

  EXPECT_CALL(mocked_query_service, FindLanePoseAtDistanceFrom(_, _, _))
      .WillOnce(Return(mantle_api::Pose{}));

  testing::InSequence s;
  // called two iterations for the trigger entity and reference entity respectively
  EXPECT_CALL(mocked_geo_helper, TranslateGlobalPositionLocally(_, _, _))
      .Times(8)
      .WillOnce(Return(headmost_corner))
      .WillOnce(Return(rearmost_corner))
      .WillRepeatedly(Return(other_corner));
  EXPECT_CALL(mocked_geo_helper, TranslateGlobalPositionLocally(_, _, _))
      .Times(8)
      .WillOnce(Return(headmost_corner))
      .WillOnce(Return(rearmost_corner))
      .WillRepeatedly(Return(other_corner));

  auto time_headway_condition = OpenScenarioEngine::v1_3::TimeHeadwayCondition(condition_values_,
                                                                               {mock_environment_});

  EXPECT_EQ(time_headway_condition.IsSatisfied(), expected_satisfied_status_);
}

TEST_P(TimeHeadwayConditionTestForCoordinateSystemWithFreespaceFalse,
       GivenCoordinateSystemIsLaneWithTestInputsAndFreespaceFalse_WhenEvaluate_ThenExpectCorrectSatisfiedStatus)
{
  auto rule = OpenScenarioEngine::v1_3::Rule(NET_ASAM_OPENSCENARIO::v1_3::Rule::RuleEnum::EQUAL_TO, condition_value_);
  condition_values_.rule = rule;
  condition_values_.coordinateSystem = OpenScenarioEngine::v1_3::CoordinateSystem::kLane;

  auto& mocked_query_service = dynamic_cast<const mantle_api::MockQueryService&>(mock_environment_->GetQueryService());
  auto& mocked_entity = dynamic_cast<mantle_api::MockVehicle&>(mock_environment_->GetEntityRepository().Get("").value().get());

  EXPECT_CALL(mocked_entity, GetVelocity()).WillOnce(Return(triggering_entity_velocity_));

  EXPECT_CALL(mocked_query_service, GetLongitudinalLaneDistanceBetweenPositions(_, _))
      .WillOnce(Return(relative_distance_));

  auto time_headway_condition = OpenScenarioEngine::v1_3::TimeHeadwayCondition(condition_values_,
                                                                               {mock_environment_});

  EXPECT_EQ(time_headway_condition.IsSatisfied(), expected_satisfied_status_);
}
