/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Utils/EntityCreator.h"

#include <MantleAPI/Traffic/entity_properties.h>
#include <openScenarioLib/generated/v1_3/catalog/CatalogHelperV1_3.h>

#include "Utils/Constants.h"

using namespace units::literals;

namespace OpenScenarioEngine::v1_3
{
std::map<NET_ASAM_OPENSCENARIO::v1_3::VehicleCategory::VehicleCategoryEnum, mantle_api::VehicleClass> map_vehicle_class{
    {NET_ASAM_OPENSCENARIO::v1_3::VehicleCategory::UNKNOWN, mantle_api::VehicleClass::kOther},
    {NET_ASAM_OPENSCENARIO::v1_3::VehicleCategory::BICYCLE, mantle_api::VehicleClass::kBicycle},
    {NET_ASAM_OPENSCENARIO::v1_3::VehicleCategory::BUS, mantle_api::VehicleClass::kBus},
    {NET_ASAM_OPENSCENARIO::v1_3::VehicleCategory::CAR, mantle_api::VehicleClass::kMedium_car},
    {NET_ASAM_OPENSCENARIO::v1_3::VehicleCategory::MOTORBIKE, mantle_api::VehicleClass::kMotorbike},
    {NET_ASAM_OPENSCENARIO::v1_3::VehicleCategory::SEMITRAILER, mantle_api::VehicleClass::kSemitrailer},
    {NET_ASAM_OPENSCENARIO::v1_3::VehicleCategory::TRAILER, mantle_api::VehicleClass::kTrailer},
    {NET_ASAM_OPENSCENARIO::v1_3::VehicleCategory::TRAIN, mantle_api::VehicleClass::kTrain},
    {NET_ASAM_OPENSCENARIO::v1_3::VehicleCategory::TRAM, mantle_api::VehicleClass::kTram},
    {NET_ASAM_OPENSCENARIO::v1_3::VehicleCategory::TRUCK, mantle_api::VehicleClass::kHeavy_truck},
    {NET_ASAM_OPENSCENARIO::v1_3::VehicleCategory::VAN, mantle_api::VehicleClass::kDelivery_van}};

std::map<NET_ASAM_OPENSCENARIO::v1_3::PedestrianCategory::PedestrianCategoryEnum, mantle_api::EntityType>
    map_entity_type{{NET_ASAM_OPENSCENARIO::v1_3::PedestrianCategory::UNKNOWN, mantle_api::EntityType::kOther},
                    {NET_ASAM_OPENSCENARIO::v1_3::PedestrianCategory::PEDESTRIAN, mantle_api::EntityType::kPedestrian},
                    {NET_ASAM_OPENSCENARIO::v1_3::PedestrianCategory::ANIMAL, mantle_api::EntityType::kAnimal},
                    {NET_ASAM_OPENSCENARIO::v1_3::PedestrianCategory::WHEELCHAIR, mantle_api::EntityType::kVehicle}};

// MiscObjectCategory is not mapped one to one to StaticObjectType due to differences between the two enum classes
std::map<NET_ASAM_OPENSCENARIO::v1_3::MiscObjectCategory::MiscObjectCategoryEnum, mantle_api::StaticObjectType> map_static_object_type{
    {NET_ASAM_OPENSCENARIO::v1_3::MiscObjectCategory::UNKNOWN, mantle_api::StaticObjectType::kOther},
    {NET_ASAM_OPENSCENARIO::v1_3::MiscObjectCategory::BARRIER, mantle_api::StaticObjectType::kBarrier},
    {NET_ASAM_OPENSCENARIO::v1_3::MiscObjectCategory::BUILDING, mantle_api::StaticObjectType::kBuilding},
    {NET_ASAM_OPENSCENARIO::v1_3::MiscObjectCategory::CROSSWALK, mantle_api::StaticObjectType::kOther},
    {NET_ASAM_OPENSCENARIO::v1_3::MiscObjectCategory::GANTRY, mantle_api::StaticObjectType::kOverheadStructure},
    {NET_ASAM_OPENSCENARIO::v1_3::MiscObjectCategory::NONE, mantle_api::StaticObjectType::kOther},
    {NET_ASAM_OPENSCENARIO::v1_3::MiscObjectCategory::OBSTACLE, mantle_api::StaticObjectType::kOther},
    {NET_ASAM_OPENSCENARIO::v1_3::MiscObjectCategory::PARKING_SPACE, mantle_api::StaticObjectType::kOther},
    {NET_ASAM_OPENSCENARIO::v1_3::MiscObjectCategory::PATCH, mantle_api::StaticObjectType::kOther},
    {NET_ASAM_OPENSCENARIO::v1_3::MiscObjectCategory::POLE, mantle_api::StaticObjectType::kPole},
    {NET_ASAM_OPENSCENARIO::v1_3::MiscObjectCategory::RAILING, mantle_api::StaticObjectType::kBarrier},
    {NET_ASAM_OPENSCENARIO::v1_3::MiscObjectCategory::ROAD_MARK, mantle_api::StaticObjectType::kOther},
    {NET_ASAM_OPENSCENARIO::v1_3::MiscObjectCategory::SOUND_BARRIER, mantle_api::StaticObjectType::kOther},
    {NET_ASAM_OPENSCENARIO::v1_3::MiscObjectCategory::STREET_LAMP, mantle_api::StaticObjectType::kEmittingStructure},
    {NET_ASAM_OPENSCENARIO::v1_3::MiscObjectCategory::TRAFFIC_ISLAND, mantle_api::StaticObjectType::kOther},
    {NET_ASAM_OPENSCENARIO::v1_3::MiscObjectCategory::TREE, mantle_api::StaticObjectType::kVegetation},
    {NET_ASAM_OPENSCENARIO::v1_3::MiscObjectCategory::VEGETATION, mantle_api::StaticObjectType::kVegetation},
    {NET_ASAM_OPENSCENARIO::v1_3::MiscObjectCategory::WIND, mantle_api::StaticObjectType::kOther}};

EntityCreator::EntityCreator(std::shared_ptr<mantle_api::IEnvironment> environment)
    : environment_(environment) {}

void EntityCreator::CreateEntity(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IScenarioObject> scenario_object)
{
  auto entity_object = scenario_object->GetEntityObject();

  if (!entity_object)
  {
    throw std::runtime_error(std::string("entityObject missing in ScenarioObject " + scenario_object->GetName()));
  }

  if (entity_object->GetVehicle() != nullptr)
  {
    CreateVehicle(entity_object->GetVehicle(), scenario_object->GetName());
  }
  else if (entity_object->GetPedestrian() != nullptr)
  {
    CreatePedestrian(entity_object->GetPedestrian(), scenario_object->GetName());
  }
  else if (entity_object->GetMiscObject() != nullptr)
  {
    CreateMiscObject(entity_object->GetMiscObject(), scenario_object->GetName());
  }
  else if (entity_object->GetCatalogReference() != nullptr)
  {
    CreateCatalogReferenceEntity(entity_object->GetCatalogReference(), scenario_object->GetName());
  }
}

void EntityCreator::CreateVehicle(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IVehicle> vehicle,
                                  const std::string& name)
{
  mantle_api::VehicleProperties properties;
  properties.type = mantle_api::EntityType::kVehicle;
  properties.classification = GetVehicleClass(vehicle->GetVehicleCategory());
  properties.model = vehicle->GetModel3d();
  properties.mass = units::mass::kilogram_t(vehicle->GetMass());
  FillBoundingBoxProperties(vehicle->GetBoundingBox(), vehicle->GetName(), properties);
  FillGenericProperties(*vehicle, properties);

  const auto& performance{vehicle->GetPerformance()};

  properties.performance.max_speed = units::velocity::meters_per_second_t(performance->GetMaxSpeed());
  properties.performance.max_acceleration =
      units::acceleration::meters_per_second_squared_t(performance->GetMaxAcceleration());
  properties.performance.max_deceleration =
      units::acceleration::meters_per_second_squared_t(performance->GetMaxDeceleration());

  if (performance->IsSetMaxAccelerationRate())
  {
    properties.performance.max_acceleration_rate =
        units::jerk::meters_per_second_cubed_t(performance->GetMaxAccelerationRate());
  }

  if (performance->IsSetMaxDecelerationRate())
  {
    properties.performance.max_deceleration_rate =
        units::jerk::meters_per_second_cubed_t(performance->GetMaxDecelerationRate());
  }

  FillAxleProperties(vehicle->GetAxles()->GetFrontAxle(), vehicle->GetBoundingBox(), name, properties.front_axle);
  FillAxleProperties(vehicle->GetAxles()->GetRearAxle(), vehicle->GetBoundingBox(), name, properties.rear_axle);

  if (IsDrivingFunctionControlledEntity(name))
  {
    properties.is_host = true;
  }

  environment_->GetEntityRepository().Create(name, properties);
}

void EntityCreator::CreatePedestrian(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IPedestrian> pedestrian,
                                     const std::string& name)
{
  if (pedestrian->GetPedestrianCategory().GetFromLiteral(pedestrian->GetPedestrianCategory().GetLiteral()) ==
      NET_ASAM_OPENSCENARIO::v1_3::PedestrianCategory::PedestrianCategoryEnum::WHEELCHAIR)
  {
    mantle_api::VehicleProperties vehicle_properties;
    vehicle_properties.classification = mantle_api::VehicleClass::kWheelchair;
    FillEntityPropertiesForPedestrian(pedestrian, vehicle_properties);
    environment_->GetEntityRepository().Create(name, vehicle_properties);
  }
  else
  {
    mantle_api::PedestrianProperties pedestrian_properties;
    FillEntityPropertiesForPedestrian(pedestrian, pedestrian_properties);
    environment_->GetEntityRepository().Create(name, pedestrian_properties);
  }
}

void EntityCreator::CreateMiscObject(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IMiscObject> misc_object,
                                     const std::string& name)
{
  mantle_api::StaticObjectProperties properties;
  properties.type = mantle_api::EntityType::kStatic;
  properties.static_object_type = GetStaticObjectType(misc_object->GetMiscObjectCategory());
  properties.model = misc_object->GetModel3d();
  FillBoundingBoxProperties(misc_object->GetBoundingBox(), misc_object->GetName(), properties);
  FillGenericProperties(*misc_object, properties);
  SetVerticalOffset(misc_object, properties);
  environment_->GetEntityRepository().Create(name, properties);
}

void EntityCreator::CreateCatalogReferenceEntity(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ICatalogReference> catalog_reference,
    const std::string& name)
{
  auto catalog_element = catalog_reference->GetRef();
  if (catalog_element == nullptr)
  {
    throw std::runtime_error(
        std::string("CatalogReference " + catalog_reference->GetEntryName() + " without ref."));
  }

  if (NET_ASAM_OPENSCENARIO::v1_3::CatalogHelper::IsVehicle(catalog_element))
  {
    CreateVehicle(NET_ASAM_OPENSCENARIO::v1_3::CatalogHelper::AsVehicle(catalog_element), name);
  }
  else if (NET_ASAM_OPENSCENARIO::v1_3::CatalogHelper::IsPedestrian(catalog_element))
  {
    CreatePedestrian(NET_ASAM_OPENSCENARIO::v1_3::CatalogHelper::AsPedestrian(catalog_element), name);
  }
  else if (NET_ASAM_OPENSCENARIO::v1_3::CatalogHelper::IsMiscObject(catalog_element))
  {
    CreateMiscObject(NET_ASAM_OPENSCENARIO::v1_3::CatalogHelper::AsMiscObject(catalog_element), name);
  }
}

void EntityCreator::FillBoundingBoxProperties(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IBoundingBox> bounding_box,
                                              const std::string& name,
                                              mantle_api::EntityProperties& properties)
{
  if (!bounding_box)
  {
    throw std::runtime_error(std::string("Entity " + name + " without bounding box."));
  }
  auto dimensions = bounding_box->GetDimensions();
  if (!dimensions)
  {
    throw std::runtime_error(std::string("Bounding box of entity " + name + " without Dimensions."));
  }

  properties.bounding_box.dimension.length = units::length::meter_t(dimensions->GetLength());
  properties.bounding_box.dimension.width = units::length::meter_t(dimensions->GetWidth());
  properties.bounding_box.dimension.height = units::length::meter_t(dimensions->GetHeight());

  auto center = bounding_box->GetCenter();
  if (!center)
  {
    throw std::runtime_error(std::string("Bounding box of entity " + name + " without Center."));
  }

  properties.bounding_box.geometric_center.x = units::length::meter_t(center->GetX());
  properties.bounding_box.geometric_center.y = units::length::meter_t(center->GetY());
  properties.bounding_box.geometric_center.z = units::length::meter_t(center->GetZ());
}

void EntityCreator::FillAxleProperties(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IAxle> open_scenario_axle,
                                       std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IBoundingBox> bounding_box,
                                       const std::string& name,
                                       mantle_api::Axle& axle)
{
  if (open_scenario_axle == nullptr)
  {
    throw std::runtime_error(std::string("Entity " + name + " is missing axle information."));
  }

  axle.bb_center_to_axle_center = {
      units::length::meter_t(open_scenario_axle->GetPositionX() - bounding_box->GetCenter()->GetX()),
      0.0_m,
      units::length::meter_t(open_scenario_axle->GetPositionZ() - bounding_box->GetCenter()->GetZ())};

  axle.max_steering = units::angle::radian_t(open_scenario_axle->GetMaxSteering());
  axle.track_width = units::length::meter_t(open_scenario_axle->GetTrackWidth());
  axle.wheel_diameter = units::length::meter_t(open_scenario_axle->GetWheelDiameter());
}

void EntityCreator::FillEntityPropertiesForPedestrian(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IPedestrian> pedestrian,
    mantle_api::EntityProperties& properties)
{
  properties.type = GetEntityTypeFromPedestrianCategory(pedestrian->GetPedestrianCategory());
  properties.model = pedestrian->GetModel3d();
  FillBoundingBoxProperties(pedestrian->GetBoundingBox(), pedestrian->GetName(), properties);
  FillGenericProperties(*pedestrian, properties);
}

mantle_api::VehicleClass EntityCreator::GetVehicleClass(NET_ASAM_OPENSCENARIO::v1_3::VehicleCategory vehicle_category)
{
  if (map_vehicle_class.find(vehicle_category.GetFromLiteral(vehicle_category.GetLiteral())) !=
      map_vehicle_class.end())
  {
    return map_vehicle_class[vehicle_category.GetFromLiteral(vehicle_category.GetLiteral())];
  }
  throw std::runtime_error(std::string("No vehicle class for vehicle category " + vehicle_category.GetLiteral()));
}

mantle_api::StaticObjectType EntityCreator::GetStaticObjectType(NET_ASAM_OPENSCENARIO::v1_3::MiscObjectCategory misc_object_category)
{
  if (map_static_object_type.find(misc_object_category.GetFromLiteral(misc_object_category.GetLiteral())) !=
      map_static_object_type.end())
  {
    return map_static_object_type[misc_object_category.GetFromLiteral(misc_object_category.GetLiteral())];
  }
  throw std::runtime_error(std::string("No static object type for misc object category " + misc_object_category.GetLiteral()));
}

mantle_api::EntityType EntityCreator::GetEntityTypeFromPedestrianCategory(
    NET_ASAM_OPENSCENARIO::v1_3::PedestrianCategory pedestrian_category)
{
  if (map_entity_type.find(pedestrian_category.GetFromLiteral(pedestrian_category.GetLiteral())) !=
      map_entity_type.end())
  {
    return map_entity_type[pedestrian_category.GetFromLiteral(pedestrian_category.GetLiteral())];
  }
  throw std::runtime_error(std::string("No entity type for pedestrian category " + pedestrian_category.GetLiteral()));
}

void EntityCreator::SetVerticalOffset(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IMiscObject> misc_object, mantle_api::StaticObjectProperties& entity_properties)
{
  if (misc_object->GetProperties() == nullptr)
  {
    return;
  }
  for (const auto& property : misc_object->GetProperties()->GetProperties())
  {
    if (property->GetName() == "mount_height")
    {
      entity_properties.vertical_offset = units::length::meter_t(std::stod(property->GetValue()));
    }
  }
}
}  // namespace OpenScenarioEngine::v1_3
