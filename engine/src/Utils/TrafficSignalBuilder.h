/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Common/time_utils.h>
#include <agnostic_behavior_tree/composite/parallel_node.h>
#include <agnostic_behavior_tree/decorator/stop_at_node.h>

#include <memory>
#include <optional>
#include <string>
#include <vector>

namespace OpenScenarioEngine::v1_3
{
struct TrafficSignalController
{
  struct Phase
  {
    std::string name;
    mantle_api::Time duration;
    std::vector<std::pair<std::string, std::string>> traffic_signal_states;
  };

  struct Delay
  {
    mantle_api::Time value;
    std::string reference;
  };

  std::string name;
  std::vector<Phase> phases;
  std::optional<Delay> delay;
};

class TrafficSignalBuilder
{
public:
  void Add(const TrafficSignalController& controller);
  std::shared_ptr<yase::StopAtNode> Create();

private:
  std::shared_ptr<yase::ParallelNode> traffic_signals_{std::make_shared<yase::ParallelNode>("TrafficSignals")};
};

}  // namespace OpenScenarioEngine::v1_3