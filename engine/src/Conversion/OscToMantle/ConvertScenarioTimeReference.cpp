
/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioTimeReference.h"

#include <units.h>

#include <optional>

namespace OpenScenarioEngine::v1_3
{
ReferenceContext ConvertDomainReference(const NET_ASAM_OPENSCENARIO::v1_3::ReferenceContext& referenceContext)
{
  switch (NET_ASAM_OPENSCENARIO::v1_3::ReferenceContext::GetFromLiteral(referenceContext.GetLiteral()))
  {
    case NET_ASAM_OPENSCENARIO::v1_3::ReferenceContext::ABSOLUTE:
      return mantle_api::ReferenceContext::kAbsolute;
    case NET_ASAM_OPENSCENARIO::v1_3::ReferenceContext::RELATIVE:
      return mantle_api::ReferenceContext::kRelative;
    case NET_ASAM_OPENSCENARIO::v1_3::ReferenceContext::UNKNOWN:
      return {};
  }
  throw std::runtime_error("FollowTrajectoryAction: the given ReferenceContext is not supported.");
}

TimeReference ConvertScenarioTimeReference(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITimeReference>& timeReference)
{
  if (const auto timing = timeReference->GetTiming())
  {
    mantle_api::FollowTrajectoryControlStrategy::TrajectoryTimeReference trajectoryTimeReference;
    trajectoryTimeReference.domainAbsoluteRelative = ConvertDomainReference(timing->GetDomainAbsoluteRelative());
    trajectoryTimeReference.scale = timing->GetScale();
    trajectoryTimeReference.offset = units::time::second_t(timing->GetOffset());

    return trajectoryTimeReference;
  }
  return std::nullopt;
}

}  // namespace OpenScenarioEngine::v1_3