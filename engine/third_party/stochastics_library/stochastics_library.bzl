load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_TAG = "v0.11.1"

def stochastics_library():
    maybe(
        http_archive,
        name = "stochastics_library",
        url = "https://gitlab.eclipse.org/eclipse/openpass/stochastics-library/-/archive/{tag}/stochastics-library-{tag}.tar.gz".format(tag = _TAG),
        sha256 = "b2c0d60a848b7a2e62be49bd2c3f2576903cf78b8bc21e65b8346274e46c15a6",
        strip_prefix = "stochastics-library-{tag}".format(tag = _TAG),
    )
