/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioLateralDisplacement.h"

#include <stdexcept>

namespace OpenScenarioEngine::v1_3
{
mantle_api::LateralDisplacementDirection ConvertScenarioLateralDisplacement(
    NET_ASAM_OPENSCENARIO::v1_3::LateralDisplacement lateralDisplacement)
{
  if (lateralDisplacement == NET_ASAM_OPENSCENARIO::v1_3::LateralDisplacement::ANY)
  {
    return mantle_api::LateralDisplacementDirection::kAny;
  }
  else if (lateralDisplacement == NET_ASAM_OPENSCENARIO::v1_3::LateralDisplacement::LEFT_TO_REFERENCED_ENTITY)
  {
    return mantle_api::LateralDisplacementDirection::kLeft;
  }
  else if (lateralDisplacement == NET_ASAM_OPENSCENARIO::v1_3::LateralDisplacement::RIGHT_TO_REFERENCED_ENTITY)
  {
    return mantle_api::LateralDisplacementDirection::kRight;
  }
  else
  {
    throw std::runtime_error(
        "ConvertScenarioLateralDisplacement: Unable to resolve LateralDisplacementDirection from"
        "LateralDisplacement. Unsupported literal.");
  }
}

}  // namespace OpenScenarioEngine::v1_3