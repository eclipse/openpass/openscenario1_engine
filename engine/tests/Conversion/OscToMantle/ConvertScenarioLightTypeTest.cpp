/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gtest/gtest.h>
#include <openScenarioLib/generated/v1_3/impl/ApiClassImplV1_3.h>

#include <string>

#include "Conversion/OscToMantle/ConvertScenarioLightType.h"

class VehicleLightTypeConverterTestFixture
    : public testing::TestWithParam<std::tuple<std::string, mantle_api::VehicleLightType>>
{
};

INSTANTIATE_TEST_SUITE_P(
    VehicleLightTypeConverter,
    VehicleLightTypeConverterTestFixture,
    testing::ValuesIn(std::vector<std::tuple<std::string, mantle_api::VehicleLightType>>{
        {"brakeLights",
         mantle_api::VehicleLightType::kBrakeLights},
        {"daytimeRunningLights",
         mantle_api::VehicleLightType::kDaytimeRunningLights},
        {"fogLights",
         mantle_api::VehicleLightType::kFogLights},
        {"fogLightsFront",
         mantle_api::VehicleLightType::kFogLightsFront},
        {"fogLightsRear",
         mantle_api::VehicleLightType::kFogLightsRear},
        {"highBeam",
         mantle_api::VehicleLightType::kHighBeam},
        {"indicatorLeft",
         mantle_api::VehicleLightType::kIndicatorLeft},
        {"indicatorRight",
         mantle_api::VehicleLightType::kIndicatorRight},
        {"licensePlateIllumination",
         mantle_api::VehicleLightType::kLicensePlateIllumination},
        {"lowBeam",
         mantle_api::VehicleLightType::kLowBeam},
        {"reversingLights",
         mantle_api::VehicleLightType::kReversingLights},
        {"specialPurposeLights",
         mantle_api::VehicleLightType::kSpecialPurposeLights},
        {"warningLights",
         mantle_api::VehicleLightType::kWarningLights}}));

TEST_P(VehicleLightTypeConverterTestFixture, GivenVehicleLightTypeAsString_ThenConvertIntoMantleApiLightType)
{
  std::string vehicle_light_type_string = std::get<0>(GetParam());
  auto vehicle_light_type = NET_ASAM_OPENSCENARIO::v1_3::VehicleLightType(vehicle_light_type_string);
  auto vehicle_light = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::VehicleLightImpl>();
  auto light_type = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::LightTypeImpl>();
  vehicle_light->SetVehicleLightType(vehicle_light_type);
  light_type->SetVehicleLight(vehicle_light);

  const auto converted_light_type = OpenScenarioEngine::v1_3::ConvertScenarioLightType(light_type);
  mantle_api::LightType expected_light_type = std::get<1>(GetParam());

  ASSERT_EQ(converted_light_type, expected_light_type);
}

TEST(ConvertScenarioUserDefinedLightTypeTest, GivenUserDefinedLightTypeAsString_ThenConvertIntoMantleApiLightType)
{
  std::string user_defined_light_type = "userDefined";
  auto user_defined_light = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::UserDefinedLightImpl>();
  auto light_type = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::LightTypeImpl>();
  user_defined_light->SetUserDefinedLightType(user_defined_light_type);
  light_type->SetUserDefinedLight(user_defined_light);

  const auto converted_light_type = OpenScenarioEngine::v1_3::ConvertScenarioLightType(light_type);
  mantle_api::LightType expected_light_type = user_defined_light_type;

  ASSERT_EQ(converted_light_type, expected_light_type);
}
