/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gtest/gtest.h>
#include <openScenarioLib/generated/v1_3/impl/ApiClassImplV1_3.h>

#include <string>

#include "Conversion/OscToMantle/ConvertScenarioLightState.h"

class VehicleLightModeConverterTestFixture
    : public testing::TestWithParam<std::tuple<std::string, mantle_api::LightMode>>
{
};

INSTANTIATE_TEST_SUITE_P(
    VehicleLightModeConverter,
    VehicleLightModeConverterTestFixture,
    testing::ValuesIn(std::vector<std::tuple<std::string, mantle_api::LightMode>>{
        {"flashing",
         mantle_api::LightMode::kFlashing},
        {"off",
         mantle_api::LightMode::kOff},
        {"on",
         mantle_api::LightMode::kOn}}));

TEST_P(VehicleLightModeConverterTestFixture, GivenVehicleLightModeAsString_ThenConvertIntoMantleApiLightState)
{
  std::string mode_string = std::get<0>(GetParam());
  auto mode = NET_ASAM_OPENSCENARIO::v1_3::LightMode(mode_string);
  auto light_state = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::LightStateImpl>();
  light_state->SetMode(mode);

  const auto converted_light_state = OpenScenarioEngine::v1_3::ConvertScenarioLightState(light_state);
  mantle_api::LightState expected_light_state = {std::get<1>(GetParam())};

  ASSERT_EQ(converted_light_state, expected_light_state);
}
