/********************************************************************************
 * Copyright (c) 2023-2025 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>

#include <limits>
#include <memory>

#include "MantleAPI/Common/i_identifiable.h"
#include "Storyboard/GenericAction/TrafficSinkAction.h"
#include "TestUtils/MockControllerService.h"

using namespace mantle_api;
using namespace units::literals;

TEST(TrafficSinkAction, GivenEntityWithinRadius_WhenActionIsStepped_ThenEntityIsDeletedAndActionContinues)
{
  auto mockEnvironment = std::make_shared<MockEnvironment>();
  auto& mockEntityRepository = static_cast<MockEntityRepository&>(mockEnvironment->GetEntityRepository());
  auto mockControllerService = std::make_shared<testing::OpenScenarioEngine::v1_3::MockControllerService>();
  auto& mockControllerRepository = static_cast<MockControllerRepository&>(mockEnvironment->GetControllerRepository());

  Vec3<units::length::meter_t> vehicle_position{10.1_m, 20.2_m, 30.3_m};
  std::vector<std::unique_ptr<mantle_api::IEntity>> vehicle_vec;

  ON_CALL(mockEntityRepository, GetEntities()).WillByDefault(
    [&vehicle_vec, &vehicle_position]() -> std::vector<std::unique_ptr<mantle_api::IEntity>>&
    {
      auto vehicle{std::make_unique<mantle_api::MockVehicle>()};
      vehicle_vec.emplace_back(std::move(vehicle));
      ON_CALL(dynamic_cast<mantle_api::MockVehicle&>(*vehicle_vec.back()), GetUniqueId).WillByDefault(testing::Return(1234));
      ON_CALL(dynamic_cast<mantle_api::MockVehicle&>(*vehicle_vec.back()), GetPosition).WillByDefault(testing::Return(vehicle_position));

      return vehicle_vec;
    });

  Pose target_pose{{10.0_m, 20.0_m, 30.0_m}, {}};  // not at 0, as other mocked entities are per default at 0

  ON_CALL(*mockControllerService, GetControllerIds(1234)).WillByDefault(testing::Return(std::vector<mantle_api::UniqueId>{1001,1002}));

  OpenScenarioEngine::v1_3::TrafficSinkAction action_under_test(
      {.radius = 10.0,
       .rate = std::numeric_limits<double>::infinity(),
       .GetPosition = [&]()
       { return target_pose; },
       .trafficDefinition = std::nullopt},
      {.environment = mockEnvironment},
      {.controllerService = mockControllerService});

  EXPECT_CALL(*mockControllerService, GetControllerIds(1234));
  EXPECT_CALL(mockControllerRepository, Delete(1002));
  EXPECT_CALL(mockControllerRepository, Delete(1001));
  EXPECT_CALL(mockEntityRepository, Delete(testing::Matcher<UniqueId>(testing::_))).Times(0);
  EXPECT_CALL(mockEntityRepository, Delete(1234)).Times(1);
  ASSERT_FALSE(action_under_test.Step());
}

TEST(TrafficSinkAction, GivenEntityLessOrEqual1mmToPosition_WhenActionIsStepped_ThenEntityIsDeletedAndActionContinues)
{
  auto mockEnvironment = std::make_shared<MockEnvironment>();
  auto& mockEntityRepository = static_cast<MockEntityRepository&>(mockEnvironment->GetEntityRepository());
  auto& mockControllerRepository = static_cast<MockControllerRepository&>(mockEnvironment->GetControllerRepository());
  auto mockControllerService = std::make_shared<testing::OpenScenarioEngine::v1_3::MockControllerService>();

  Vec3<units::length::meter_t> vehicle_position{1_mm,  // = EPSILON FOR DETECTION
                                                10_m,
                                                10_m};
  std::vector<std::unique_ptr<mantle_api::IEntity>> vehicle_vec;

  ON_CALL(mockEntityRepository, GetEntities()).WillByDefault(
    [&vehicle_vec, &vehicle_position]() -> std::vector<std::unique_ptr<mantle_api::IEntity>>&
    {
      auto vehicle{std::make_unique<mantle_api::MockVehicle>()};
      vehicle_vec.emplace_back(std::move(vehicle));
      ON_CALL(dynamic_cast<mantle_api::MockVehicle&>(*vehicle_vec.back()), GetUniqueId).WillByDefault(testing::Return(1234));
      ON_CALL(dynamic_cast<mantle_api::MockVehicle&>(*vehicle_vec.back()), GetPosition).WillByDefault(testing::Return(vehicle_position));
      return vehicle_vec;
    });

  ON_CALL(*mockControllerService, GetControllerIds(1234)).WillByDefault(testing::Return(std::vector<mantle_api::UniqueId>{1001,1002}));

  Pose target_pose{{0_m, 10_m, 10_m}, {}};  // not at 0, as other mocked entities are per default at 0
  OpenScenarioEngine::v1_3::TrafficSinkAction action_under_test(
      {.radius = 0,
       .rate = std::numeric_limits<double>::infinity(),
       .GetPosition = [&]
       { return target_pose; },
       .trafficDefinition = std::nullopt},
      {.environment = mockEnvironment},
      {.controllerService = mockControllerService});

  EXPECT_CALL(*mockControllerService, GetControllerIds(1234));
  EXPECT_CALL(mockControllerRepository, Delete(1002));
  EXPECT_CALL(mockControllerRepository, Delete(1001));
  EXPECT_CALL(mockEntityRepository, Delete(testing::Matcher<UniqueId>(testing::_))).Times(0);
  EXPECT_CALL(mockEntityRepository, Delete(1234)).Times(1);
  ASSERT_FALSE(action_under_test.Step());
}

TEST(TrafficSinkAction, GivenEntityMoreThan1mmApartToPosition_WhenActionIsStepped_ThenEntityIsNotDeletedAndActionContinues)
{
  auto mockEnvironment = std::make_shared<MockEnvironment>();
  auto& mockEntityRepository = static_cast<MockEntityRepository&>(mockEnvironment->GetEntityRepository());
  auto mockControllerService = std::make_shared<testing::OpenScenarioEngine::v1_3::MockControllerService>();
  auto& mockControllerRepository = static_cast<MockControllerRepository&>(mockEnvironment->GetControllerRepository());

  Vec3<units::length::meter_t> vehicle_position{1.001_mm,  // BEYOND EPSILON FOR DETECTIN
                                                10_m,
                                                10_m};
  std::vector<std::unique_ptr<mantle_api::IEntity>> vehicle_vec;

  ON_CALL(mockEntityRepository, GetEntities()).WillByDefault([&vehicle_vec, &vehicle_position]() -> std::vector<std::unique_ptr<mantle_api::IEntity>>&
                                                             {
      auto vehicle{std::make_unique<mantle_api::MockVehicle>()};
      vehicle_vec.emplace_back(std::move(vehicle));
      ON_CALL(dynamic_cast<mantle_api::MockVehicle&>(*vehicle_vec.back()), GetUniqueId).WillByDefault(testing::Return(1234));
      ON_CALL(dynamic_cast<mantle_api::MockVehicle&>(*vehicle_vec.back()), GetPosition).WillByDefault(testing::Return(vehicle_position));

      return vehicle_vec; });
  ON_CALL(*mockControllerService, GetControllerIds(1234)).WillByDefault(testing::Return(std::vector<mantle_api::UniqueId>{1001,1002}));

  Pose target_pose{{0_m, 10_m, 10_m}, {}};  // not at 0, as other mocked entities are per default at 0
  OpenScenarioEngine::v1_3::TrafficSinkAction action_under_test(
      {.radius = 0,
       .rate = std::numeric_limits<double>::infinity(),
       .GetPosition = [&]
       { return target_pose; },
       .trafficDefinition = std::nullopt},
      {.environment = mockEnvironment},
      {.controllerService = mockControllerService});

  EXPECT_CALL(*mockControllerService, GetControllerIds(1234)).Times(0);
  EXPECT_CALL(mockControllerRepository, Delete(1002)).Times(0);
  EXPECT_CALL(mockControllerRepository, Delete(1001)).Times(0);
  EXPECT_CALL(mockEntityRepository, Delete(testing::Matcher<UniqueId>(testing::_))).Times(0);
  ASSERT_FALSE(action_under_test.Step());
}

TEST(TrafficSinkAction, GivenOneEntityWithinRadius_WhenEntityIsAStaticObject_ThenStaticObjectIsNotDeleted)
{
  auto mockEnvironment = std::make_shared<MockEnvironment>();
  auto mockControllerService = std::make_shared<testing::OpenScenarioEngine::v1_3::MockControllerService>();
  auto& mockControllerRepository = static_cast<MockControllerRepository&>(mockEnvironment->GetControllerRepository());

  // the entity repository returns 1 vehicle, 1 pedestrian and 1 stationary object
  auto& mockEntityRepository = static_cast<MockEntityRepository&>(mockEnvironment->GetEntityRepository());

  Vec3<units::length::meter_t> entity_position{0.0_m, 0.0_m, 0.0_m};
  std::vector<std::unique_ptr<IEntity>> vehicle_vec;

  ON_CALL(mockEntityRepository, GetEntities()).WillByDefault(
    [&vehicle_vec, &entity_position]() -> std::vector<std::unique_ptr<IEntity>>&
    {
      auto object{std::make_unique<MockStaticObject>()};
      vehicle_vec.emplace_back(std::move(object));
      return vehicle_vec;
    });
  ON_CALL(*mockControllerService, GetControllerIds(1234)).WillByDefault(testing::Return(std::vector<mantle_api::UniqueId>{1001,1002}));

  Pose target_pose{{0.0_m, 0.0_m, 0.0_m}, {}};  // all mocked entities are at 0 per default!
  OpenScenarioEngine::v1_3::TrafficSinkAction action_under_test(
      {.radius = std::numeric_limits<double>::infinity(),
       .rate = std::numeric_limits<double>::infinity(),
       .GetPosition = [&]
       { return target_pose; },
       .trafficDefinition = std::nullopt},
      {.environment = mockEnvironment},
      {.controllerService = mockControllerService});

  EXPECT_CALL(*mockControllerService, GetControllerIds(1234)).Times(0);
  EXPECT_CALL(mockControllerRepository, Delete(1002)).Times(0);
  EXPECT_CALL(mockControllerRepository, Delete(1001)).Times(0);
  EXPECT_CALL(mockEntityRepository, Delete(testing::Matcher<UniqueId>(testing::_))).Times(0);
  ASSERT_FALSE(action_under_test.Step());
}

TEST(TrafficSinkAction, GivenEntityOutsideOfRadius_WhenActionIsStepped_EntityIsNotDeletedAndActionContinues)
{
  auto mockEnvironment = std::make_shared<MockEnvironment>();
  auto& mockEntityRepository = static_cast<MockEntityRepository&>(mockEnvironment->GetEntityRepository());
  auto mockControllerService = std::make_shared<testing::OpenScenarioEngine::v1_3::MockControllerService>();
  auto& mockControllerRepository = static_cast<MockControllerRepository&>(mockEnvironment->GetControllerRepository());

  Vec3<units::length::meter_t> vehicle_position{10.0_m, 20.0_m, 30.0_m};
  std::vector<std::unique_ptr<mantle_api::IEntity>> vehicle_vec;

  ON_CALL(mockEntityRepository, GetEntities()).WillByDefault(
    [&vehicle_vec, &vehicle_position]() -> std::vector<std::unique_ptr<mantle_api::IEntity>>&
    {
      auto vehicle{std::make_unique<mantle_api::MockVehicle>()};
      vehicle_vec.emplace_back(std::move(vehicle));
      ON_CALL(dynamic_cast<mantle_api::MockVehicle&>(*vehicle_vec.back()), GetUniqueId).WillByDefault(testing::Return(1234));
      ON_CALL(dynamic_cast<mantle_api::MockVehicle&>(*vehicle_vec.back()), GetPosition).WillByDefault(testing::Return(vehicle_position));

      return vehicle_vec;
    });
  ON_CALL(*mockControllerService, GetControllerIds(1234)).WillByDefault(testing::Return(std::vector<mantle_api::UniqueId>{1001,1002}));

  Pose target_pose{{1.0_m, 2.0_m, 3.0_m}, {}};
  OpenScenarioEngine::v1_3::TrafficSinkAction action_under_test(
      {.radius = 1.0,
       .rate = std::numeric_limits<double>::infinity(),
       .GetPosition = [&]
       { return target_pose; },
       .trafficDefinition = std::nullopt},
      {.environment = mockEnvironment},
      {.controllerService = mockControllerService});

  EXPECT_CALL(*mockControllerService, GetControllerIds(1234)).Times(0);
  EXPECT_CALL(mockControllerRepository, Delete(1002)).Times(0);
  EXPECT_CALL(mockControllerRepository, Delete(1001)).Times(0);
  EXPECT_CALL(mockEntityRepository, Delete(testing::Matcher<UniqueId>(testing::_))).Times(0);
  ASSERT_FALSE(action_under_test.Step());
}

TEST(TrafficSinkAction, GivenEntityAtPositionOfAction_WhenRadiusOfActionZero_ThenEntityIsDeleted)
{
  auto mockEnvironment = std::make_shared<MockEnvironment>();
  auto& mockEntityRepository = static_cast<MockEntityRepository&>(mockEnvironment->GetEntityRepository());
  auto mockControllerService = std::make_shared<testing::OpenScenarioEngine::v1_3::MockControllerService>();
  auto& mockControllerRepository = static_cast<MockControllerRepository&>(mockEnvironment->GetControllerRepository());

  Vec3<units::length::meter_t> vehicle_position{1.0_m, 2.0_m, 3.0_m};
  std::vector<std::unique_ptr<mantle_api::IEntity>> vehicle_vec;

  ON_CALL(mockEntityRepository, GetEntities()).WillByDefault(
    [&vehicle_vec, &vehicle_position]() -> std::vector<std::unique_ptr<mantle_api::IEntity>>&
    {
      auto vehicle{std::make_unique<mantle_api::MockVehicle>()};
      vehicle_vec.emplace_back(std::move(vehicle));
      ON_CALL(dynamic_cast<mantle_api::MockVehicle&>(*vehicle_vec.back()), GetUniqueId).WillByDefault(testing::Return(1234));
      ON_CALL(dynamic_cast<mantle_api::MockVehicle&>(*vehicle_vec.back()), GetPosition).WillByDefault(testing::Return(vehicle_position));

      return vehicle_vec;
    });
  ON_CALL(*mockControllerService, GetControllerIds(1234)).WillByDefault(testing::Return(std::vector<mantle_api::UniqueId>{1001,1002}));

  Pose target_pose{vehicle_position, {}};
  OpenScenarioEngine::v1_3::TrafficSinkAction action_under_test(
      {.radius = 0.0,
       .rate = std::numeric_limits<double>::infinity(),
       .GetPosition = [&]
       { return target_pose; },
       .trafficDefinition = std::nullopt},
      {.environment = mockEnvironment},
      {.controllerService = mockControllerService});

  EXPECT_CALL(*mockControllerService, GetControllerIds(1234));
  EXPECT_CALL(mockControllerRepository, Delete(1002));
  EXPECT_CALL(mockControllerRepository, Delete(1001));
  EXPECT_CALL(mockEntityRepository, Delete(testing::Matcher<UniqueId>(testing::_))).Times(0);
  EXPECT_CALL(mockEntityRepository, Delete(1234)).Times(1);
  ASSERT_FALSE(action_under_test.Step());
}
