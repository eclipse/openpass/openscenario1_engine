/********************************************************************************
 * Copyright (c) 2025 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioClothoidSpline.h"

#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Common/unit_definitions.h>
#include <openScenarioLib/generated/v1_3/api/ApiClassInterfacesV1_3.h>

#include <memory>
#include <optional>
#include <stdexcept>

namespace OpenScenarioEngine::v1_3::detail
{

using units::literals::operator""_rad;
using units::literals::operator""_m;

std::optional<mantle_api::Pose> ConvertWorldPosition(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IClothoidSplineSegment>& clothoid_spline_segment)
{
  if (clothoid_spline_segment->IsSetPositionStart())
  {
    const auto position_start = clothoid_spline_segment->GetPositionStart();

    if (!position_start->IsSetWorldPosition())
    {
      throw std::runtime_error("ConvertWorldPosition: World position is not set");
    }

    const auto world_position = position_start->GetWorldPosition();

    return std::make_optional<mantle_api::Pose>(
        {{units::length::meter_t(world_position->GetX()),
          units::length::meter_t(world_position->GetY()),
          world_position->IsSetZ() ? units::length::meter_t(world_position->GetZ()) : 0_m},
         {world_position->IsSetH() ? units::angle::radian_t(world_position->GetH()) : 0_rad,
          world_position->IsSetP() ? units::angle::radian_t(world_position->GetP()) : 0_rad,
          world_position->IsSetR() ? units::angle::radian_t(world_position->GetR()) : 0_rad}});
  }
  else
  {
    return std::nullopt;
  }
}

mantle_api::ClothoidSplineSegment ConvertClothoidSplineSegment(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IClothoidSplineSegment>& clothoid_spline_segment)
{
  if (!clothoid_spline_segment)
  {
    throw std::invalid_argument("ConvertClothoidSplineSegment: clothoid_spline_segment is not initialized");
  }

  mantle_api::ClothoidSplineSegment converted_clothoid_spline_segment;

  converted_clothoid_spline_segment.curvature_end = units::curve::curvature_t(clothoid_spline_segment->GetCurvatureEnd());
  converted_clothoid_spline_segment.curvature_start = units::curve::curvature_t(clothoid_spline_segment->GetCurvatureStart());

  if (clothoid_spline_segment->IsSetHOffset())
  {
    converted_clothoid_spline_segment.h_offset = units::angle::radian_t(clothoid_spline_segment->GetHOffset());
  }
  else
  {
    converted_clothoid_spline_segment.h_offset = 0.0_rad;
  }

  converted_clothoid_spline_segment.length = units::length::meter_t(clothoid_spline_segment->GetLength());

  converted_clothoid_spline_segment.position_start = ConvertWorldPosition(clothoid_spline_segment);

  return converted_clothoid_spline_segment;
}
}  // namespace OpenScenarioEngine::v1_3::detail

namespace OpenScenarioEngine::v1_3
{
mantle_api::ClothoidSpline ConvertClothoidSpline(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IClothoidSpline>& clothoid_spline)
{
  if (!clothoid_spline)
  {
    throw std::invalid_argument("ConvertClothoidSpline: clothoid_spline is not initialized");
  }

  mantle_api::ClothoidSpline converted_clothoid_spline;
  for (const auto& segment : clothoid_spline->GetSegments())
  {
    converted_clothoid_spline.segments.push_back(detail::ConvertClothoidSplineSegment(segment));
  }
  return converted_clothoid_spline;
}

}  // namespace OpenScenarioEngine::v1_3
