/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>
#include <openScenarioLib/generated/v1_3/api/ApiClassInterfacesV1_3.h>
#include <openScenarioLib/generated/v1_3/impl/ApiClassImplV1_3.h>

#include "Conversion/OscToMantle/ConvertScenarioSpeedActionTarget.h"

using namespace units::literals;

TEST(ConvertScenarioSpeedActionTargetTest, GivenNoRelativeTargetSpeedAndNoRelativeTargetSpeed_ThenThrowError)
{
  auto mockEnvironment = std::make_shared<mantle_api::MockEnvironment>();

  auto speed_action_target = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::SpeedActionTargetImpl>();

  EXPECT_THROW(OpenScenarioEngine::v1_3::ConvertScenarioSpeedActionTarget(mockEnvironment, speed_action_target), std::runtime_error);
}

TEST(ConvertScenarioSpeedActionTargetTest, GivenAbsoluteTargetSpeed_ThenConvertToSpeedActionTarget)
{
  auto mockEnvironment = std::make_shared<mantle_api::MockEnvironment>();

  auto absoluteTargetSpeed = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::AbsoluteTargetSpeedImpl>();
  absoluteTargetSpeed->SetValue(1.0);

  auto speed_action_target = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::SpeedActionTargetImpl>();
  speed_action_target->SetAbsoluteTargetSpeed(absoluteTargetSpeed);

  auto trajectoryTimeReference = OpenScenarioEngine::v1_3::ConvertScenarioSpeedActionTarget(mockEnvironment, speed_action_target);
  EXPECT_EQ(trajectoryTimeReference, 1.0_mps);
}

TEST(ConvertScenarioSpeedActionTargetTest, GivenRelativeTargetSpeedAndDeltaValueType_ThenConvertToSpeedActionTarget)
{
  auto mockEnvironment = std::make_shared<mantle_api::MockEnvironment>();
  auto namedRef = std::make_shared<NET_ASAM_OPENSCENARIO::INamedReference<NET_ASAM_OPENSCENARIO::v1_3::IEntity>>();
  auto relativeTargetSpeed = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::RelativeTargetSpeedImpl>();
  relativeTargetSpeed->SetEntityRef(namedRef);
  relativeTargetSpeed->SetSpeedTargetValueType(NET_ASAM_OPENSCENARIO::v1_3::SpeedTargetValueType::DELTA);
  relativeTargetSpeed->SetValue(1.0);

  auto speed_action_target = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::SpeedActionTargetImpl>();
  speed_action_target->SetRelativeTargetSpeed(relativeTargetSpeed);

  auto trajectoryTimeReference = OpenScenarioEngine::v1_3::ConvertScenarioSpeedActionTarget(mockEnvironment, speed_action_target);
  EXPECT_EQ(trajectoryTimeReference, 1.0_mps);
}

TEST(ConvertScenarioSpeedActionTargetTest, GivenRelativeTargetSpeedAndFactorValueType_ThenConvertToSpeedActionTarget)
{
  auto mockEnvironment = std::make_shared<mantle_api::MockEnvironment>();

  auto namedRef = std::make_shared<NET_ASAM_OPENSCENARIO::INamedReference<NET_ASAM_OPENSCENARIO::v1_3::IEntity>>();

  auto relativeTargetSpeed = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::RelativeTargetSpeedImpl>();
  relativeTargetSpeed->SetEntityRef(namedRef);
  relativeTargetSpeed->SetSpeedTargetValueType(NET_ASAM_OPENSCENARIO::v1_3::SpeedTargetValueType::FACTOR);
  relativeTargetSpeed->SetValue(1.0);

  auto speed_action_target = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::SpeedActionTargetImpl>();
  speed_action_target->SetRelativeTargetSpeed(relativeTargetSpeed);

  auto trajectoryTimeReference = OpenScenarioEngine::v1_3::ConvertScenarioSpeedActionTarget(mockEnvironment, speed_action_target);
  EXPECT_EQ(trajectoryTimeReference, 0.0_mps);
}