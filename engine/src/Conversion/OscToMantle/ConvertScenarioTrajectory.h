
/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Common/trajectory.h>
#include <MantleAPI/Traffic/control_strategy.h>
#include <openScenarioLib/generated/v1_3/api/ApiClassInterfacesV1_3.h>

#include <memory>
#include <string>

namespace OpenScenarioEngine::v1_3
{
struct Trajectory
{
};

inline mantle_api::Trajectory ConvertScenarioTrajectory(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITrajectory>& /*trajectory*/)
{
  return {};
}

}  // namespace OpenScenarioEngine::v1_3