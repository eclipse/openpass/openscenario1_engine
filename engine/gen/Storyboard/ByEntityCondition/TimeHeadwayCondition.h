/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/action_node.h>

#include <cassert>
#include <utility>

#include "Storyboard/ByEntityCondition/TimeHeadwayCondition_impl.h"
#include "Utils/EntityBroker.h"

namespace OpenScenarioEngine::v1_3::Node
{
class TimeHeadwayCondition : public yase::ActionNode
{
public:
  TimeHeadwayCondition(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITimeHeadwayCondition> timeHeadwayCondition)
      : yase::ActionNode{"TimeHeadwayCondition"},
        timeHeadwayCondition_{timeHeadwayCondition}
  {
  }

  void onInit() override{};

private:
  yase::NodeStatus tick() override
  {
    assert(impl_);

    const auto is_satisfied = impl_->IsSatisfied();

    if (is_satisfied)
    {
      if (entityBroker_)
      {
        entityBroker_->add_triggering(triggeringEntity_);
      }
      return yase::NodeStatus::kSuccess;
    }

    return yase::NodeStatus::kRunning;
  };

  void lookupAndRegisterData(yase::Blackboard& blackboard) final
  {
    triggeringEntity_ = blackboard.get<std::string>("TriggeringEntity");
    if (blackboard.exists("EntityBroker"))
    {
      entityBroker_ = blackboard.get<EntityBroker::Ptr>("EntityBroker");
    }

    auto environment = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");

    impl_ = std::make_unique<OpenScenarioEngine::v1_3::TimeHeadwayCondition>(
        OpenScenarioEngine::v1_3::TimeHeadwayCondition::Values{
            triggeringEntity_,
            timeHeadwayCondition_->GetAlongRoute(),
            timeHeadwayCondition_->GetFreespace(),
            ConvertScenarioEntity(timeHeadwayCondition_->GetEntityRef()),
            ConvertScenarioCoordinateSystem(timeHeadwayCondition_->GetCoordinateSystem()),
            ConvertScenarioRelativeDistanceType(timeHeadwayCondition_->GetRelativeDistanceType()),
            ConvertScenarioRule(timeHeadwayCondition_->GetRule(), timeHeadwayCondition_->GetValue())},
        OpenScenarioEngine::v1_3::TimeHeadwayCondition::Interfaces{
            environment});
  }

  std::unique_ptr<OpenScenarioEngine::v1_3::TimeHeadwayCondition> impl_{nullptr};
  std::string triggeringEntity_;
  EntityBroker::Ptr entityBroker_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITimeHeadwayCondition> timeHeadwayCondition_;
};

}  // namespace OpenScenarioEngine::v1_3::Node
