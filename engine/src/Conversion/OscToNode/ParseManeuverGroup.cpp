/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToNode/ParseManeuverGroup.h"

#include <memory>

#include "Conversion/OscToNode/ParseManeuver.h"
#include "Node/ManeuverGroupNode.h"

namespace OpenScenarioEngine::v1_3
{
yase::BehaviorNode::Ptr parse(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IManeuverGroup> maneuverGroup)
{
  auto maneuverGroupNode = std::make_shared<Node::ManeuverGroupNode>(
      "ManeuverGroup",
      maneuverGroup->GetActors());

  for (const auto& maneuver : maneuverGroup->GetManeuvers())
  {
    maneuverGroupNode->addChild(parse(maneuver));
  }
  return maneuverGroupNode;
}

}  // namespace OpenScenarioEngine::v1_3