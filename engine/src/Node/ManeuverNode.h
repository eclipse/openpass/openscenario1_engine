/*******************************************************************************
 * Copyright (c) 2025 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <agnostic_behavior_tree/decorator_node.h>

namespace OpenScenarioEngine::v1_3
{
class IEventPrioritizer;

namespace Node
{

/// @brief Runs a list of events. Nodes in the same maneuver can be skipped or overridden based on an event's priority.
class ManeuverNode : public yase::DecoratorNode
{
public:
  /// @brief Construct a new maneuver node
  ///
  /// @param name Name of the maneuver node
  explicit ManeuverNode(const std::string& name);

private:
  void lookupAndRegisterData(yase::Blackboard& blackboard) override;
  yase::NodeStatus tick() final;

  std::shared_ptr<IEventPrioritizer> eventPrioritizer_;
};

}  // namespace Node

}  // namespace OpenScenarioEngine::v1_3
