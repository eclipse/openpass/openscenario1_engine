
/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioTrafficDefinition.h"
#include "Conversion/OscToMantle/ConvertScenarioController.h"

namespace OpenScenarioEngine::v1_3
{

namespace detail
{

mantle_api::VehicleClass ConvertVehicleCategory(NET_ASAM_OPENSCENARIO::v1_3::VehicleCategory vehicle_category)
{
  const auto literal{vehicle_category.GetLiteral()};
  const auto category{vehicle_category.GetFromLiteral(literal)};

  switch(category)
  {
    case NET_ASAM_OPENSCENARIO::v1_3::VehicleCategory::VehicleCategoryEnum::UNKNOWN:
      return mantle_api::VehicleClass::kInvalid;
    case NET_ASAM_OPENSCENARIO::v1_3::VehicleCategory::VehicleCategoryEnum::BICYCLE:
      return mantle_api::VehicleClass::kBicycle;
    case NET_ASAM_OPENSCENARIO::v1_3::VehicleCategory::VehicleCategoryEnum::BUS:
      return mantle_api::VehicleClass::kBus;
    case NET_ASAM_OPENSCENARIO::v1_3::VehicleCategory::VehicleCategoryEnum::CAR:
      return mantle_api::VehicleClass::kMedium_car;
    case NET_ASAM_OPENSCENARIO::v1_3::VehicleCategory::VehicleCategoryEnum::MOTORBIKE:
      return mantle_api::VehicleClass::kMotorbike;
    case NET_ASAM_OPENSCENARIO::v1_3::VehicleCategory::VehicleCategoryEnum::SEMITRAILER:
      return mantle_api::VehicleClass::kSemitrailer;
    case NET_ASAM_OPENSCENARIO::v1_3::VehicleCategory::VehicleCategoryEnum::TRAILER:
      return mantle_api::VehicleClass::kTrailer;
    case NET_ASAM_OPENSCENARIO::v1_3::VehicleCategory::VehicleCategoryEnum::TRAIN:
      return mantle_api::VehicleClass::kTrain;
    case NET_ASAM_OPENSCENARIO::v1_3::VehicleCategory::VehicleCategoryEnum::TRAM:
      return mantle_api::VehicleClass::kTram;
    case NET_ASAM_OPENSCENARIO::v1_3::VehicleCategory::VehicleCategoryEnum::TRUCK:
      return mantle_api::VehicleClass::kHeavy_truck;
    case NET_ASAM_OPENSCENARIO::v1_3::VehicleCategory::VehicleCategoryEnum::VAN:
      return mantle_api::VehicleClass::kDelivery_van;
    default:
      return mantle_api::VehicleClass::kInvalid;
  }
}

VehicleCategoryDistribution ConvertVehicleCategoryDistributionEntry(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IVehicleCategoryDistributionEntry> entry)
{
  VehicleCategoryDistribution vehicle_category_distribution{};

  if(entry->IsSetCategory())
  {
    vehicle_category_distribution.category = ConvertVehicleCategory(entry->GetCategory());
  }

  if(entry->IsSetWeight())
  {
    vehicle_category_distribution.weight = entry->GetWeight();
  }

  return vehicle_category_distribution;
}

ControllerDistribution ConvertControllerDistributionEntry(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IControllerDistributionEntry> entry)
{
  ControllerDistribution controller_distribution{};

  if(entry->IsSetWeight())
  {
    controller_distribution.weight = entry->GetWeight();
  }

  controller_distribution.controller = ConvertScenarioController(entry->GetController(), entry->GetCatalogReference());
  
  return controller_distribution;
}

} // namespace detail

TrafficDefinition ConvertScenarioTrafficDefinition(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITrafficDefinition>& osc_traffic_definition)
{
  if(osc_traffic_definition->GetVehicleCategoryDistribution()->GetVehicleCategoryDistributionEntries().empty())
  {
    throw std::runtime_error("ConvertScenarioTrafficDefinition : the traffic definition has no vehicle distribution");
  }

  if(osc_traffic_definition->GetControllerDistribution()->GetControllerDistributionEntries().empty())
  {
    throw std::runtime_error("ConvertScenarioTrafficDefinition : the traffic definition has no controller distribution");
  }

  std::vector<VehicleCategoryDistribution> vehicle_category_distribution_entries;

  for(const auto& vehicle_category_distribution_entry : osc_traffic_definition->GetVehicleCategoryDistribution()->GetVehicleCategoryDistributionEntries())
  {
    auto converted{detail::ConvertVehicleCategoryDistributionEntry(vehicle_category_distribution_entry)};
    vehicle_category_distribution_entries.push_back(std::move(converted));
  }

  std::vector<ControllerDistribution> controller_distribution_entries;

  for(const auto& controller_distribution_entry : osc_traffic_definition->GetControllerDistribution()->GetControllerDistributionEntries())
  {
    auto converted{detail::ConvertControllerDistributionEntry(controller_distribution_entry)};
    controller_distribution_entries.push_back(std::move(converted));
  }

  return {osc_traffic_definition->GetName(), vehicle_category_distribution_entries, controller_distribution_entries};
}

}  // namespace OpenScenarioEngine::v1_3