/*******************************************************************************
 * Copyright (c) 2025 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <gmock/gmock.h>

#include "Utils/IEventPrioritizer.h"

namespace testing::OpenScenarioEngine::v1_3
{
class MockEventPrioritizer : public ::OpenScenarioEngine::v1_3::IEventPrioritizer
{
public:
  MOCK_METHOD(void, RegisterEvent, (const ::OpenScenarioEngine::v1_3::Event&, ::OpenScenarioEngine::v1_3::EventPriority), (override));
  MOCK_METHOD(void, EventStarted, (const ::OpenScenarioEngine::v1_3::Event&), (override));
  MOCK_METHOD(bool, ShouldSkipChild, (const ::OpenScenarioEngine::v1_3::Event&), (const, override));
  MOCK_METHOD(bool, ShouldStopChild, (const ::OpenScenarioEngine::v1_3::Event&), (const, override));
  MOCK_METHOD(void, UpdateOverriddenEvents, (), (override));

  static inline std::shared_ptr<MockEventPrioritizer> make_shared()
  {
    return std::make_shared<MockEventPrioritizer>();
  }
};

}  // namespace testing::OpenScenarioEngine::v1_3
