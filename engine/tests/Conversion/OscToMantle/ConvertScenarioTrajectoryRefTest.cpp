/********************************************************************************
 * Copyright (c) 2021-2025 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>
#include <openScenarioLib/generated/v1_3/api/ApiClassInterfacesV1_3.h>
#include <openScenarioLib/generated/v1_3/impl/ApiClassImplV1_3.h>

#include <functional>
#include <optional>
#include <stdexcept>
#include <tuple>
#include <vector>

#include "Conversion/OscToMantle/ConvertScenarioTrajectoryRef.h"
#include "TestUtils/ClothoidSplineUtils.h"
#include "TestUtils/TestLogger.h"

namespace
{

using namespace units::literals;

class ConvertScenarioTrajectoryRefTest : public ::testing::Test
{
protected:
  void SetTrajectoryShapeAndName()
  {
    trajectory->SetShape(trajectory_shape);
    trajectory->SetName("Trajectory1");
    trajectory_ref->SetTrajectory(trajectory);
  }

  void SetTrajectoryWithClothoidSpline()
  {
    expected_clothoid_spline.segments.push_back({
        2.0_curv,
        1.5_curv,
        1.5_rad,
        1.8_m,
        std::nullopt,
    });

    expected_clothoid_spline.segments.push_back({-6.8_curv,
                                                 9.4_curv,
                                                 2.3_rad,
                                                 1.5_m,
                                                 mantle_api::Pose{{6.1_m, -1.5_m, 3.9_m}, {1.4_rad, 2.0_rad, -1.4_rad}}});

    for (const auto& segment : expected_clothoid_spline.segments)
    {
      clothoid_spline_segments.push_back(testing::OpenScenarioEngine::v1_3::CreateClothoidSegment(segment));
    }

    auto clothoid_spline = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::ClothoidSplineImpl>();
    clothoid_spline->SetSegments(clothoid_spline_segments);
    trajectory_shape->SetClothoidSpline(clothoid_spline);

    SetTrajectoryShapeAndName();
  }

  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::PositionImpl> SetWorldPosition(double x, double y, double z, double h, double p, double r)
  {
    auto world_pos = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::WorldPositionImpl>();
    world_pos->SetX(x);
    world_pos->SetY(y);
    world_pos->SetZ(z);
    world_pos->SetH(h);
    world_pos->SetP(p);
    world_pos->SetR(r);
    auto pos = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::PositionImpl>();
    pos->SetWorldPosition(world_pos);
    return pos;
  }

  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::VertexImpl> SetVertex(double x, double y, double z, double h, double p, double r)
  {
    auto worldposition = SetWorldPosition(x, y, z, h, p, r);

    auto vertex = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::VertexImpl>();
    vertex->SetPosition(worldposition);
    vertex->SetTime(5.0);
    return vertex;
  }

  mantle_api::Vec3<units::length::meter_t> position1{1.0_m, 2.0_m, 0.0_m};
  mantle_api::Orientation3<units::angle::radian_t> orientation1{0.1_rad, 0.0_rad, 0.0_rad};
  mantle_api::Vec3<units::length::meter_t> position2{1.1_m, 2.1_m, 0.0_m};
  mantle_api::Orientation3<units::angle::radian_t> orientation2{0.2_rad, 0.0_rad, 0.0_rad};
  mantle_api::Vec3<units::length::meter_t> position3{1.2_m, 2.2_m, 0.0_m};
  mantle_api::Orientation3<units::angle::radian_t> orientation3{0.3_rad, 0.0_rad, 0.0_rad};

  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::PolylineImpl>
      polyline{std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::PolylineImpl>()};

  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ShapeImpl> trajectory_shape{std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::ShapeImpl>()};

  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::TrajectoryImpl> trajectory{std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::TrajectoryImpl>()};

  const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::TrajectoryRefImpl> trajectory_ref{std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::TrajectoryRefImpl>()};

  const std::shared_ptr<mantle_api::MockEnvironment> mockEnvironment{std::make_shared<mantle_api::MockEnvironment>()};

  std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IClothoidSplineSegmentWriter>> clothoid_spline_segments;

  std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IParameterDeclarationWriter>> parameter_declarations;

  mantle_api::ClothoidSpline expected_clothoid_spline;

  testing::OpenScenarioEngine::v1_3::TestLogger logger;
};

TEST_F(ConvertScenarioTrajectoryRefTest, GivenPolyLine_ReturnMantleAPITrajectoryType)
{
  std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IVertexWriter>> vertices{};
  vertices.push_back(SetVertex(position1.x.value(), position1.y.value(), position1.z.value(), orientation1.yaw.value(), orientation1.pitch.value(), orientation1.roll.value()));
  vertices.push_back(SetVertex(position2.x.value(), position2.y.value(), position2.z.value(), orientation2.yaw.value(), orientation2.pitch.value(), orientation2.roll.value()));
  vertices.push_back(SetVertex(position3.x.value(), position3.y.value(), position3.z.value(), orientation3.yaw.value(), orientation3.pitch.value(), orientation3.roll.value()));

  polyline->SetVertices(vertices);
  trajectory_shape->SetPolyline(polyline);

  SetTrajectoryShapeAndName();

  auto converted_trajectory_ref = OpenScenarioEngine::v1_3::ConvertScenarioTrajectoryRef(mockEnvironment, trajectory_ref, nullptr);

  mantle_api::Pose expected_pose1{position1, orientation1};
  mantle_api::Pose expected_pose2{position2, orientation2};
  mantle_api::Pose expected_pose3{position3, orientation3};

  auto& polyline = std::get<mantle_api::PolyLine>(converted_trajectory_ref->type);
  EXPECT_THAT(polyline, testing::SizeIs(3));
  EXPECT_EQ(polyline.at(0).time, 5.0_s);
  EXPECT_EQ(polyline.at(0).pose, expected_pose1);
  EXPECT_EQ(polyline.at(1).time, 5.0_s);
  EXPECT_EQ(polyline.at(1).pose, expected_pose2);
  EXPECT_EQ(polyline.at(2).time, 5.0_s);
  EXPECT_EQ(polyline.at(2).pose, expected_pose3);
}

class ConvertScenarioTrajectoryRefTestParam : public ConvertScenarioTrajectoryRefTest, public ::testing::WithParamInterface<std::tuple<std::optional<std::string>, mantle_api::TrajectoryReferencePoint>>
{
};

INSTANTIATE_TEST_CASE_P(
    ConvertScenarioTrajectoryRefTestInstParam,
    ConvertScenarioTrajectoryRefTestParam,
    ::testing::Values(
        std::tuple<std::optional<std::string>, mantle_api::TrajectoryReferencePoint>{"rear_axle", mantle_api::TrajectoryReferencePoint::kRearAxle},
        std::tuple<std::optional<std::string>, mantle_api::TrajectoryReferencePoint>{"front_axle", mantle_api::TrajectoryReferencePoint::kFrontAxle},
        std::tuple<std::optional<std::string>, mantle_api::TrajectoryReferencePoint>{"center_of_mass", mantle_api::TrajectoryReferencePoint::kCenterOfMass},
        std::tuple<std::optional<std::string>, mantle_api::TrajectoryReferencePoint>{"bounding_box_center", mantle_api::TrajectoryReferencePoint::kBoundingBoxCenter},
        std::tuple<std::optional<std::string>, mantle_api::TrajectoryReferencePoint>{std::nullopt, mantle_api::TrajectoryReferencePoint::kUndefined}));

TEST_P(ConvertScenarioTrajectoryRefTestParam, GivenTrajectoryWithClothoidSpline_ThenConvertedTrajectoryIsCorrect)
{
  SetTrajectoryWithClothoidSpline();

  parameter_declarations.push_back(std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::ParameterDeclarationImpl>());
  if (std::get<0>(GetParam()))
  {
    parameter_declarations.back()->SetName("reference");
    parameter_declarations.back()->SetValue(std::get<0>(GetParam()).value());
  }

  trajectory->SetParameterDeclarations(parameter_declarations);

  auto converted_trajectory_ref = OpenScenarioEngine::v1_3::ConvertScenarioTrajectoryRef(mockEnvironment, trajectory_ref, nullptr);

  mantle_api::ClothoidSpline converted_shape;

  ASSERT_NO_THROW(converted_shape = std::get<mantle_api::ClothoidSpline>(converted_trajectory_ref->type);) << "Expect trajectory type ClothoidSpline, throw exception";

  EXPECT_EQ(converted_trajectory_ref->name, trajectory->GetName());

  EXPECT_EQ(converted_trajectory_ref->reference, std::get<1>(GetParam()));

  EXPECT_EQ(converted_shape, expected_clothoid_spline);
}

TEST_F(ConvertScenarioTrajectoryRefTest, GivenTrajectoryWithClothoidSpline_WhenInvalidReferenceIsProvided_ThenException)
{
  SetTrajectoryWithClothoidSpline();

  parameter_declarations.push_back(std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::ParameterDeclarationImpl>());
  parameter_declarations.back()->SetName("reference");
  parameter_declarations.back()->SetValue("invalid_value");

  trajectory->SetParameterDeclarations(parameter_declarations);

  EXPECT_THROW(OpenScenarioEngine::v1_3::ConvertScenarioTrajectoryRef(mockEnvironment, trajectory_ref, nullptr), std::runtime_error);
}

using SetShapeFunction = std::function<void(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ShapeImpl>)>;

class ConvertScenarioTrajectoryRefTestShapeParam : public ConvertScenarioTrajectoryRefTest, public ::testing::WithParamInterface<SetShapeFunction>
{
};

INSTANTIATE_TEST_CASE_P(
  ConvertScenarioTrajectoryRefTestShapeInstParam,
  ConvertScenarioTrajectoryRefTestShapeParam,
  ::testing::Values([](std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ShapeImpl> shape){shape->SetClothoid(std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::ClothoidImpl>());}, 
                    [](std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ShapeImpl> shape){shape->SetNurbs(std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::NurbsImpl>());}));

TEST_P(ConvertScenarioTrajectoryRefTestShapeParam, GivenUnsupportedShape_ThenException)
{
  GetParam()(trajectory_shape);
  SetTrajectoryShapeAndName();
  EXPECT_EQ(std::nullopt, OpenScenarioEngine::v1_3::ConvertScenarioTrajectoryRef(mockEnvironment, trajectory_ref, nullptr));
  EXPECT_THAT(logger.LastLogLevel(), mantle_api::LogLevel::kError);
  EXPECT_THAT(logger.LastLogMessage(), testing::HasSubstr("Only Polyline and ClothoidSpline are supported!"));
}
}  // namespace
