"""
This module contains rule to pull nholthaus units
"""

load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_TAG = "2.3.4"

def units_nhh():
    maybe(
        http_archive,
        name = "units_nhh",
        build_file = Label("//:third_party/units/units.BUILD"),
        url = "https://github.com/nholthaus/units/archive/refs/tags/v{tag}.tar.gz".format(tag = _TAG),
        sha256 = "e7c7d307408c30bfd30c094beea8d399907ffaf9ac4b08f4045c890f2e076049",
        strip_prefix = "units-{tag}".format(tag = _TAG),
    )
