/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Common/pose.h>
#include <MantleAPI/Execution/i_environment.h>
#include <openScenarioLib/generated/v1_3/api/ApiClassInterfacesV1_3.h>

#include <memory>
#include <optional>

namespace OpenScenarioEngine::v1_3
{
std::optional<mantle_api::Pose> ConvertScenarioPosition(const std::shared_ptr<mantle_api::IEnvironment>& environment,
                                                        const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IPosition>& pos);

mantle_api::LatLonPosition ConvertToMantleLatLonPosition(const NET_ASAM_OPENSCENARIO::v1_3::IGeoPosition& geo_position);

}  // namespace OpenScenarioEngine::v1_3
