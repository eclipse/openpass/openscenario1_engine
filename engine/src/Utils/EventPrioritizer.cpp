/*******************************************************************************
 * Copyright (c) 2025 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Utils/EventPrioritizer.h"

namespace OpenScenarioEngine::v1_3
{

void EventPrioritizer::RegisterEvent(const Event& name, EventPriority priority)
{
  event_priorities_.insert({name, priority});
}

void EventPrioritizer::EventStarted(const Event& name)
{
  auto [_, just_started] = started_events_.insert(name);
  if (just_started &&
      (event_priorities_.at(name) == EventPriority::kOverride ||
       event_priorities_.at(name) == EventPriority::kOverwrite))
  {
    overriding_event_ = name;
  }
}

bool EventPrioritizer::ShouldSkipChild(const Event& name) const
{
  return event_priorities_.at(name) == EventPriority::kSkip;
}

bool EventPrioritizer::ShouldStopChild(const Event& name) const
{
  if (auto event = events_to_override_.find(name); event != events_to_override_.end())
  {
    return true;
  }
  return false;
}

void EventPrioritizer::UpdateOverriddenEvents()
{
  if (!overriding_event_.empty())
  {
    mark_events_overridden(overriding_event_);
  }
  overriding_event_.clear();
}

void EventPrioritizer::mark_events_overridden(const Event& overriding_event)
{
  for (const auto& event : started_events_)
  {
    if (event != overriding_event)
    {
      events_to_override_.insert(event);
    }
  }
}

}  // namespace OpenScenarioEngine::v1_3
