/********************************************************************************
 * Copyright (c) 2025 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Common/clothoid_spline.h>
#include <openScenarioLib/generated/v1_3/api/writer/ApiClassWriterInterfacesV1_3.h>

#include <memory>

namespace testing::OpenScenarioEngine::v1_3
{
std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IClothoidSplineSegmentWriter> CreateClothoidSegment(const mantle_api::ClothoidSplineSegment& clothoid_spline_segment);
}  // namespace testing::OpenScenarioEngine::v1_3
