/*******************************************************************************
 * Copyright (c) 2025 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <agnostic_behavior_tree/decorator/data_declaration_node.h>
#include <agnostic_behavior_tree/composite/parallel_node.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <memory>

#include "Node/EventNode.h"
#include "Node/ManeuverNode.h"
#include "Node/Testing/FakeActionNode.h"

using OpenScenarioEngine::v1_3::EventPriority;
using OpenScenarioEngine::v1_3::Node::EventNode;
using OpenScenarioEngine::v1_3::Node::ManeuverNode;
using testing::Return;
using testing::OpenScenarioEngine::v1_3::FakeActionNode;

enum class OscStatus : std::uint8_t
{
  kStandby = 0,
  kRunning,
  kSuccess,
  kSkipped,
  kUnknown,
};

class ManeuverNodeTest : public testing::Test
{
protected:
  using EventWithStartTrigger = std::pair<yase::BehaviorNode&, FakeActionNode&>;

  ManeuverNodeTest()
  {
    maneuver_node->setChild(events_node);
  }

  EventWithStartTrigger AddEventWithPriority(EventPriority priority)
  {
    auto mock_action_node = FakeActionNode::CREATE_PTR();
    auto mock_start_trigger = FakeActionNode::CREATE_PTR();
    if (priority == EventPriority::kSkip)
    {
      ON_CALL(*mock_action_node, tick()).WillByDefault(Return(yase::NodeStatus::kSuccess));
      ON_CALL(*mock_start_trigger, tick()).WillByDefault(Return(yase::NodeStatus::kRunning));
    }
    else
    {
      ON_CALL(*mock_action_node, tick()).WillByDefault(Return(yase::NodeStatus::kRunning));
      ON_CALL(*mock_start_trigger, tick()).WillByDefault(Return(yase::NodeStatus::kSuccess));
    }
    auto event = std::make_shared<EventNode>("Event " + std::to_string(events_node->childrenCount() + 1), mock_action_node, mock_start_trigger, priority);
    events_node->addChild(event);

    return EventWithStartTrigger{events_node->child(events_node->childrenCount() - 1), *mock_start_trigger};
  }

  static OscStatus GetEventStatus(const yase::BehaviorNode& event, const FakeActionNode& start_trigger)
  {
    auto osc_status = OscStatus::kUnknown;
    const auto& event_status = event.status();
    const auto& start_trigger_status = start_trigger.status();
    if (event_status == yase::NodeStatus::kRunning)
    {
      if (start_trigger_status == yase::NodeStatus::kSuccess)
      {
        osc_status = OscStatus::kRunning;
      }
      else
      {
        osc_status = OscStatus::kStandby;
      }
    }
    else if (event_status == yase::NodeStatus::kSuccess)
    {
      if (start_trigger_status == yase::NodeStatus::kSuccess)
      {
        osc_status = OscStatus::kSuccess;
      }
      else
      {
        osc_status = OscStatus::kSkipped;
      }
    }
    return osc_status;
  }

  std::shared_ptr<ManeuverNode> maneuver_node = std::make_shared<ManeuverNode>("Maneuver Node");
  std::shared_ptr<yase::ParallelNode> events_node = std::make_shared<yase::ParallelNode>("Events Node");
};

TEST_F(ManeuverNodeTest, GivenMultipleNodesWithParallelPriority_WhenManeuverTicks_ThenAllEventsRunning)
{
  auto [event1, start_trigger1] = AddEventWithPriority(EventPriority::kParallel);
  auto [event2, start_trigger2] = AddEventWithPriority(EventPriority::kParallel);
  auto [event3, start_trigger3] = AddEventWithPriority(EventPriority::kParallel);
  maneuver_node->distributeData();
  maneuver_node->executeTick();

  EXPECT_THAT(GetEventStatus(event1, start_trigger1), OscStatus::kRunning);
  EXPECT_THAT(GetEventStatus(event2, start_trigger2), OscStatus::kRunning);
  EXPECT_THAT(GetEventStatus(event3, start_trigger3), OscStatus::kRunning);

  maneuver_node->executeTick();

  EXPECT_THAT(GetEventStatus(event1, start_trigger1), OscStatus::kRunning);
  EXPECT_THAT(GetEventStatus(event2, start_trigger2), OscStatus::kRunning);
  EXPECT_THAT(GetEventStatus(event3, start_trigger3), OscStatus::kRunning);
}

TEST_F(ManeuverNodeTest, GivenMultipleNodesWithParallelorSkipPriority_WhenManeuverTicks_ThenSkippedEventsSucceeded)
{
  auto [event1, start_trigger1] = AddEventWithPriority(EventPriority::kParallel);
  auto [event2, start_trigger2] = AddEventWithPriority(EventPriority::kSkip);
  auto [event3, start_trigger3] = AddEventWithPriority(EventPriority::kParallel);
  maneuver_node->distributeData();
  maneuver_node->executeTick();

  EXPECT_THAT(GetEventStatus(event1, start_trigger1), OscStatus::kRunning);
  EXPECT_THAT(GetEventStatus(event2, start_trigger2), OscStatus::kSkipped);
  EXPECT_THAT(GetEventStatus(event3, start_trigger3), OscStatus::kRunning);
}

TEST_F(ManeuverNodeTest, GivenMultipleNodesWithOneOverridePriority_WhenManeuverTicks_ThenParallelEventsOverridden)
{
  auto [event1, start_trigger1] = AddEventWithPriority(EventPriority::kParallel);
  auto [event2, start_trigger2] = AddEventWithPriority(EventPriority::kSkip);
  auto [event3, start_trigger3] = AddEventWithPriority(EventPriority::kParallel);
  auto [event4, start_trigger4] = AddEventWithPriority(EventPriority::kOverride);

  maneuver_node->distributeData();
  maneuver_node->executeTick();

  EXPECT_THAT(GetEventStatus(event1, start_trigger1), OscStatus::kRunning);
  EXPECT_THAT(GetEventStatus(event2, start_trigger2), OscStatus::kSkipped);
  EXPECT_THAT(GetEventStatus(event3, start_trigger3), OscStatus::kRunning);
  EXPECT_THAT(GetEventStatus(event4, start_trigger4), OscStatus::kRunning);

  maneuver_node->executeTick();

  EXPECT_THAT(GetEventStatus(event1, start_trigger1), OscStatus::kSuccess);
  EXPECT_THAT(GetEventStatus(event2, start_trigger2), OscStatus::kSkipped);
  EXPECT_THAT(GetEventStatus(event3, start_trigger3), OscStatus::kSuccess);
  EXPECT_THAT(GetEventStatus(event4, start_trigger4), OscStatus::kRunning);
}

TEST_F(ManeuverNodeTest, GivenMultipleNodesWithOneOverridePriority_WhenManeuverTicks_ThenUnstartedParallelEventsNotOverridden)
{
  auto [event1, start_trigger1] = AddEventWithPriority(EventPriority::kParallel);
  EXPECT_CALL(start_trigger1, tick())
    .WillOnce(Return(yase::NodeStatus::kRunning))
    .WillRepeatedly(Return(yase::NodeStatus::kSuccess));
  auto [event2, start_trigger2] = AddEventWithPriority(EventPriority::kSkip);
  auto [event3, start_trigger3] = AddEventWithPriority(EventPriority::kParallel);
  auto [event4, start_trigger4] = AddEventWithPriority(EventPriority::kOverride);

  maneuver_node->distributeData();
  maneuver_node->executeTick();

  EXPECT_THAT(GetEventStatus(event1, start_trigger1), OscStatus::kStandby);
  EXPECT_THAT(GetEventStatus(event2, start_trigger2), OscStatus::kSkipped);
  EXPECT_THAT(GetEventStatus(event3, start_trigger3), OscStatus::kRunning);
  EXPECT_THAT(GetEventStatus(event4, start_trigger4), OscStatus::kRunning);

  maneuver_node->executeTick();

  EXPECT_THAT(GetEventStatus(event1, start_trigger1), OscStatus::kRunning);
  EXPECT_THAT(GetEventStatus(event2, start_trigger2), OscStatus::kSkipped);
  EXPECT_THAT(GetEventStatus(event3, start_trigger3), OscStatus::kSuccess);
  EXPECT_THAT(GetEventStatus(event4, start_trigger4), OscStatus::kRunning);

  maneuver_node->executeTick();

  EXPECT_THAT(GetEventStatus(event1, start_trigger1), OscStatus::kRunning);
  EXPECT_THAT(GetEventStatus(event2, start_trigger2), OscStatus::kSkipped);
  EXPECT_THAT(GetEventStatus(event3, start_trigger3), OscStatus::kSuccess);
  EXPECT_THAT(GetEventStatus(event4, start_trigger4), OscStatus::kRunning);
}


TEST_F(ManeuverNodeTest, GivenMultipleNodesWithMultipleOverridePriority_WhenManeuverTicks_ThenOnlyStartedEventsOverridden)
{
  auto [event1, start_trigger1] = AddEventWithPriority(EventPriority::kParallel);
  auto [event2, start_trigger2] = AddEventWithPriority(EventPriority::kSkip);
  auto [event3, start_trigger3] = AddEventWithPriority(EventPriority::kParallel);
  auto [event4, start_trigger4] = AddEventWithPriority(EventPriority::kOverride);
  auto [event5, start_trigger5] = AddEventWithPriority(EventPriority::kParallel);
  EXPECT_CALL(start_trigger5, tick())
    .WillOnce(Return(yase::NodeStatus::kRunning))
    .WillRepeatedly(Return(yase::NodeStatus::kSuccess));
  auto [event6, start_trigger6] = AddEventWithPriority(EventPriority::kOverride);
  EXPECT_CALL(start_trigger6, tick())
    .WillOnce(Return(yase::NodeStatus::kRunning))
    .WillOnce(Return(yase::NodeStatus::kRunning))
    .WillRepeatedly(Return(yase::NodeStatus::kSuccess));

  maneuver_node->distributeData();
  maneuver_node->executeTick();

  EXPECT_THAT(GetEventStatus(event1, start_trigger1), OscStatus::kRunning);
  EXPECT_THAT(GetEventStatus(event2, start_trigger2), OscStatus::kSkipped);
  EXPECT_THAT(GetEventStatus(event3, start_trigger3), OscStatus::kRunning);
  EXPECT_THAT(GetEventStatus(event4, start_trigger4), OscStatus::kRunning);
  EXPECT_THAT(GetEventStatus(event5, start_trigger5), OscStatus::kStandby);
  EXPECT_THAT(GetEventStatus(event6, start_trigger6), OscStatus::kStandby);

  maneuver_node->executeTick();

  EXPECT_THAT(GetEventStatus(event1, start_trigger1), OscStatus::kSuccess);
  EXPECT_THAT(GetEventStatus(event2, start_trigger2), OscStatus::kSkipped);
  EXPECT_THAT(GetEventStatus(event3, start_trigger3), OscStatus::kSuccess);
  EXPECT_THAT(GetEventStatus(event4, start_trigger4), OscStatus::kRunning);
  EXPECT_THAT(GetEventStatus(event5, start_trigger5), OscStatus::kRunning);
  EXPECT_THAT(GetEventStatus(event6, start_trigger6), OscStatus::kStandby);

  maneuver_node->executeTick();

  EXPECT_THAT(GetEventStatus(event1, start_trigger1), OscStatus::kSuccess);
  EXPECT_THAT(GetEventStatus(event2, start_trigger2), OscStatus::kSkipped);
  EXPECT_THAT(GetEventStatus(event3, start_trigger3), OscStatus::kSuccess);
  EXPECT_THAT(GetEventStatus(event4, start_trigger4), OscStatus::kRunning);
  EXPECT_THAT(GetEventStatus(event5, start_trigger5), OscStatus::kRunning);
  EXPECT_THAT(GetEventStatus(event6, start_trigger6), OscStatus::kRunning);

  maneuver_node->executeTick();

  EXPECT_THAT(GetEventStatus(event1, start_trigger1), OscStatus::kSuccess);
  EXPECT_THAT(GetEventStatus(event2, start_trigger2), OscStatus::kSkipped);
  EXPECT_THAT(GetEventStatus(event3, start_trigger3), OscStatus::kSuccess);
  EXPECT_THAT(GetEventStatus(event4, start_trigger4), OscStatus::kSuccess);
  EXPECT_THAT(GetEventStatus(event5, start_trigger5), OscStatus::kSuccess);
  EXPECT_THAT(GetEventStatus(event6, start_trigger6), OscStatus::kRunning);
}


TEST_F(ManeuverNodeTest, GivenMultipleNodesWithOneOverwritePriority_WhenManeuverTicks_ThenParallelEventsOverridden)
{
  auto [event1, start_trigger1] = AddEventWithPriority(EventPriority::kParallel);
  auto [event2, start_trigger2] = AddEventWithPriority(EventPriority::kSkip);
  auto [event3, start_trigger3] = AddEventWithPriority(EventPriority::kParallel);
  auto [event4, start_trigger4] = AddEventWithPriority(EventPriority::kOverwrite);

  maneuver_node->distributeData();
  maneuver_node->executeTick();

  EXPECT_THAT(GetEventStatus(event1, start_trigger1), OscStatus::kRunning);
  EXPECT_THAT(GetEventStatus(event2, start_trigger2), OscStatus::kSkipped);
  EXPECT_THAT(GetEventStatus(event3, start_trigger3), OscStatus::kRunning);
  EXPECT_THAT(GetEventStatus(event4, start_trigger4), OscStatus::kRunning);

  maneuver_node->executeTick();

  EXPECT_THAT(GetEventStatus(event1, start_trigger1), OscStatus::kSuccess);
  EXPECT_THAT(GetEventStatus(event2, start_trigger2), OscStatus::kSkipped);
  EXPECT_THAT(GetEventStatus(event3, start_trigger3), OscStatus::kSuccess);
  EXPECT_THAT(GetEventStatus(event4, start_trigger4), OscStatus::kRunning);
}
