/*******************************************************************************
 * Copyright (c) 2025 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Node/ManeuverNode.h"

#include "Utils/EventPrioritizer.h"

namespace OpenScenarioEngine::v1_3::Node
{
ManeuverNode::ManeuverNode(const std::string& name)
    : yase::DecoratorNode{name}, eventPrioritizer_{std::make_shared<EventPrioritizer>()}
{
}

yase::NodeStatus ManeuverNode::tick()
{
  eventPrioritizer_->UpdateOverriddenEvents();
  return child().executeTick();
}

void ManeuverNode::lookupAndRegisterData(yase::Blackboard &blackboard)
{
  blackboard.set("EventPrioritizer", eventPrioritizer_);
}

}  // namespace OpenScenarioEngine::v1_3::Node
