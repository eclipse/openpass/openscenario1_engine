/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <Stochastics/StochasticsInterface.h>

#include <optional>

#include "Conversion/OscToMantle/ConvertScenarioCentralSwarmObject.h"
#include "Conversion/OscToMantle/ConvertScenarioDirectionOfTravelDistribution.h"
#include "Conversion/OscToMantle/ConvertScenarioRange.h"
#include "Conversion/OscToMantle/ConvertScenarioTrafficDefinition.h"

namespace OpenScenarioEngine::v1_3
{
class TrafficSwarmActionBase
{
public:
  struct Values
  {
    double innerRadius;
    unsigned int numberOfVehicles;
    double offset;
    double semiMajorAxis;
    double semiMinorAxis;
    std::optional<double> velocity;
    CentralSwarmObject centralObject;
    TrafficDefinition trafficDefinition;
    std::optional<Range> initialSpeedRange;
    std::optional<DirectionOfTravelDistribution> directionOfTravelDistribution;
  };
  struct Interfaces
  {
    std::shared_ptr<mantle_api::IEnvironment> environment;
  };
  struct Services
  {
    std::shared_ptr<StochasticsInterface> stochastics;
  };

  TrafficSwarmActionBase(Values values, Interfaces interfaces, Services services)
      : values_{std::move(values)},
        mantle_{std::move(interfaces)},
        services_{std::move(services)} {};
  virtual ~TrafficSwarmActionBase() = default;
  virtual bool Step() = 0;

protected:
  Values values_;
  Interfaces mantle_;
  Services services_;
};

}  // namespace OpenScenarioEngine::v1_3
