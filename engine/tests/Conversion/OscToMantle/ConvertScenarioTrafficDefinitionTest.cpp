/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gtest/gtest.h>
#include <openScenarioLib/generated/v1_3/impl/ApiClassImplV1_3.h>

#include "Conversion/OscToMantle/ConvertScenarioTrafficDefinition.h"

using namespace NET_ASAM_OPENSCENARIO::v1_3;

namespace detail
{

const size_t kControllerPropertiesCount{2};
const std::array<std::string, kControllerPropertiesCount> kControllerPropertiesName{"france", "germany"};
const std::array<std::string, kControllerPropertiesCount> kControllerPropertiesValue{"paris", "berlin"};
const std::string kControllerName{"controller"};
const std::string kCatalogControllerName{"catalog_controller"};
const std::string kTrafficDefinitionName{"traffic_definition"};
const double kControllerWeight{3.14};
const double kCatalogControllerWeight{2.71};

std::vector<std::shared_ptr<IVehicleCategoryDistributionEntryWriter>> GetVehicleCategoryDistributionEntries()
{
  const size_t category_count{11};
  std::vector<std::shared_ptr<IVehicleCategoryDistributionEntryWriter>> vehicle_category_distribution_entries;

  for (size_t i{0}; i < category_count; ++i)
  {
    auto entry{std::make_shared<VehicleCategoryDistributionEntryImpl>()};
    entry->SetCategory(NET_ASAM_OPENSCENARIO::v1_3::VehicleCategory(static_cast<VehicleCategory::VehicleCategoryEnum>(i - 1)));
    entry->SetWeight(static_cast<double>(i));
    vehicle_category_distribution_entries.emplace_back(std::move(entry));
  }

  return vehicle_category_distribution_entries;
}

std::shared_ptr<ControllerDistributionEntryImpl> GetControllerEntry()
{
  std::vector<std::shared_ptr<IPropertyWriter>> controller_property_vec;

  for (size_t i{0}; i < kControllerPropertiesCount; ++i)
  {
    auto property{std::make_shared<PropertyImpl>()};
    property->SetName(kControllerPropertiesName[i]);
    property->SetValue(kControllerPropertiesValue[i]);
    controller_property_vec.emplace_back(std::move(property));
  }

  auto controller_properties{std::make_shared<PropertiesImpl>()};
  controller_properties->SetProperties(controller_property_vec);

  auto controller{std::make_shared<ControllerImpl>()};
  controller->SetName(kControllerName);
  controller->SetProperties(controller_properties);

  auto controller_entry{std::make_shared<ControllerDistributionEntryImpl>()};
  controller_entry->SetWeight(kControllerWeight);
  controller_entry->SetController(controller);

  return controller_entry;
}

std::shared_ptr<ControllerDistributionEntryImpl> GetCatalogEntry()
{
  std::vector<std::shared_ptr<IPropertyWriter>> catalog_property_vec;

  for (size_t i{0}; i < kControllerPropertiesCount; ++i)
  {
    auto property{std::make_shared<PropertyImpl>()};
    property->SetName(kControllerPropertiesName[i]);
    property->SetValue(kControllerPropertiesValue[i]);
    catalog_property_vec.emplace_back(std::move(property));
  }

  auto catalog_properties{std::make_shared<PropertiesImpl>()};
  catalog_properties->SetProperties(catalog_property_vec);

  auto catalog_controller{std::make_shared<ControllerImpl>()};
  catalog_controller->SetName(kCatalogControllerName);
  catalog_controller->SetProperties(catalog_properties);

  auto catalog_reference{std::make_shared<CatalogReferenceImpl>()};
  catalog_reference->SetRef(catalog_controller);

  auto catalog_entry{std::make_shared<ControllerDistributionEntryImpl>()};
  catalog_entry->SetWeight(kCatalogControllerWeight);
  catalog_entry->SetCatalogReference(catalog_reference);

  return catalog_entry;
}

std::vector<std::shared_ptr<IControllerDistributionEntryWriter>> GetControllerDistributionEntries()
{
  std::vector<std::shared_ptr<IControllerDistributionEntryWriter>> controller_distribution_entries;

  auto controller_entry{GetControllerEntry()};
  controller_distribution_entries.emplace_back(std::move(controller_entry));

  auto catalog_entry{GetCatalogEntry()};
  controller_distribution_entries.emplace_back(std::move(catalog_entry));

  return controller_distribution_entries;
}

std::shared_ptr<TrafficDefinitionImpl> GetTrafficDefinition()
{
  auto vehicle_category_distribution_entries{GetVehicleCategoryDistributionEntries()};
  auto vehicle_category_distribution{std::make_shared<VehicleCategoryDistributionImpl>()};
  vehicle_category_distribution->SetVehicleCategoryDistributionEntries(vehicle_category_distribution_entries);

  auto controller_distribution_entries{GetControllerDistributionEntries()};
  auto controller_distribution{std::make_shared<ControllerDistributionImpl>()};
  controller_distribution->SetControllerDistributionEntries(controller_distribution_entries);

  auto traffic_definition{std::make_shared<TrafficDefinitionImpl>()};
  traffic_definition->SetName(kTrafficDefinitionName);
  traffic_definition->SetVehicleCategoryDistribution(vehicle_category_distribution);
  traffic_definition->SetControllerDistribution(controller_distribution);

  return traffic_definition;
}

std::shared_ptr<TrafficDefinitionImpl> GetTrafficDefinitionWithoutVehicleDistribution()
{
  std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IVehicleCategoryDistributionEntryWriter>> vehicle_category_distribution_entries{};
  auto vehicle_category_distribution{std::make_shared<VehicleCategoryDistributionImpl>()};
  vehicle_category_distribution->SetVehicleCategoryDistributionEntries(vehicle_category_distribution_entries);

  auto controller_distribution_entries{GetControllerDistributionEntries()};
  auto controller_distribution{std::make_shared<ControllerDistributionImpl>()};
  controller_distribution->SetControllerDistributionEntries(controller_distribution_entries);

  auto traffic_definition{std::make_shared<TrafficDefinitionImpl>()};
  traffic_definition->SetName(kTrafficDefinitionName);
  traffic_definition->SetVehicleCategoryDistribution(vehicle_category_distribution);
  traffic_definition->SetControllerDistribution(controller_distribution);

  return traffic_definition;
}

std::shared_ptr<TrafficDefinitionImpl> GetTrafficDefinitionWithoutControllerDistribution()
{
  auto vehicle_category_distribution{std::make_shared<VehicleCategoryDistributionImpl>()};
  auto vehicle_category_distribution_entries{GetVehicleCategoryDistributionEntries()};
  vehicle_category_distribution->SetVehicleCategoryDistributionEntries(vehicle_category_distribution_entries);

  std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IControllerDistributionEntryWriter>> controller_distribution_entries{};
  auto controller_distribution{std::make_shared<ControllerDistributionImpl>()};
  controller_distribution->SetControllerDistributionEntries(controller_distribution_entries);

  auto traffic_definition{std::make_shared<TrafficDefinitionImpl>()};
  traffic_definition->SetName(kTrafficDefinitionName);
  traffic_definition->SetVehicleCategoryDistribution(vehicle_category_distribution);
  traffic_definition->SetControllerDistribution(controller_distribution);

  return traffic_definition;
}

}  // namespace detail

TEST(ConvertScenarioTrafficDefinition, GivenTrafficDefinitionImplementation_WhenConverting_ThenGetXoscTrafficDefinition)
{
  auto traffic_definition{detail::GetTrafficDefinition()};

  auto converted{OpenScenarioEngine::v1_3::ConvertScenarioTrafficDefinition(traffic_definition)};

  ASSERT_EQ(detail::kTrafficDefinitionName, converted.name);

  ASSERT_EQ(converted.vehicle_category_distribution_entries.at(0).category, mantle_api::VehicleClass::kInvalid);
  ASSERT_EQ(converted.vehicle_category_distribution_entries.at(1).category, mantle_api::VehicleClass::kBicycle);
  ASSERT_EQ(converted.vehicle_category_distribution_entries.at(2).category, mantle_api::VehicleClass::kBus);
  ASSERT_EQ(converted.vehicle_category_distribution_entries.at(3).category, mantle_api::VehicleClass::kMedium_car);
  ASSERT_EQ(converted.vehicle_category_distribution_entries.at(4).category, mantle_api::VehicleClass::kMotorbike);
  ASSERT_EQ(converted.vehicle_category_distribution_entries.at(5).category, mantle_api::VehicleClass::kSemitrailer);
  ASSERT_EQ(converted.vehicle_category_distribution_entries.at(6).category, mantle_api::VehicleClass::kTrailer);
  ASSERT_EQ(converted.vehicle_category_distribution_entries.at(7).category, mantle_api::VehicleClass::kTrain);
  ASSERT_EQ(converted.vehicle_category_distribution_entries.at(8).category, mantle_api::VehicleClass::kTram);
  ASSERT_EQ(converted.vehicle_category_distribution_entries.at(9).category, mantle_api::VehicleClass::kHeavy_truck);
  ASSERT_EQ(converted.vehicle_category_distribution_entries.at(10).category, mantle_api::VehicleClass::kDelivery_van);

  for (size_t i{0}; i < converted.vehicle_category_distribution_entries.size(); ++i)
  {
    ASSERT_EQ(converted.vehicle_category_distribution_entries.at(i).weight, i);
  }

  ASSERT_EQ(2, converted.controller_distribution_entries.size());

  ASSERT_EQ(detail::kControllerName, converted.controller_distribution_entries.at(0).controller.name);
  ASSERT_EQ(detail::kControllerWeight, converted.controller_distribution_entries.at(0).weight);

  for (size_t i{0}; i < converted.controller_distribution_entries.at(0).controller.parameters.size(); ++i)
  {
    ASSERT_EQ(1, converted.controller_distribution_entries.at(0).controller.parameters.count(detail::kControllerPropertiesName[i]));
    ASSERT_EQ(detail::kControllerPropertiesValue[i], converted.controller_distribution_entries.at(0).controller.parameters[detail::kControllerPropertiesName[i]]);
  }

  ASSERT_EQ(detail::kCatalogControllerName, converted.controller_distribution_entries.at(1).controller.name);
  ASSERT_EQ(detail::kCatalogControllerWeight, converted.controller_distribution_entries.at(1).weight);
  ASSERT_EQ(detail::kControllerPropertiesCount, converted.controller_distribution_entries.at(1).controller.parameters.size());

  for (size_t i{0}; i < converted.controller_distribution_entries.at(1).controller.parameters.size(); ++i)
  {
    ASSERT_EQ(1, converted.controller_distribution_entries.at(1).controller.parameters.count(detail::kControllerPropertiesName[i]));
    ASSERT_EQ(detail::kControllerPropertiesValue[i], converted.controller_distribution_entries.at(1).controller.parameters[detail::kControllerPropertiesName[i]]);
  }
}

TEST(ConvertScenarioTrafficDefinition, GivenTrafficDefinitionWithoutVehicleDistribution_WhenConverting_ExpectThrow)
{
  auto traffic_definition{detail::GetTrafficDefinitionWithoutVehicleDistribution()};
  EXPECT_THROW(OpenScenarioEngine::v1_3::ConvertScenarioTrafficDefinition(traffic_definition), std::runtime_error);
}

TEST(ConvertScenarioTrafficDefinition, GivenTrafficDefinitionWithoutControllerDistribution_WhenConverting_ExpectThrow)
{
  auto traffic_definition{detail::GetTrafficDefinitionWithoutControllerDistribution()};
  EXPECT_THROW(OpenScenarioEngine::v1_3::ConvertScenarioTrafficDefinition(traffic_definition), std::runtime_error);
}
