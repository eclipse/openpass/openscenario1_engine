/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioLightState.h"

namespace OpenScenarioEngine::v1_3
{

namespace detail
{

mantle_api::LightMode ConvertLightMode(NET_ASAM_OPENSCENARIO::v1_3::LightMode lightMode)
{
  const auto literal{lightMode.GetLiteral()};

  switch (lightMode.GetFromLiteral(literal))
  {
    case NET_ASAM_OPENSCENARIO::v1_3::LightMode::LightModeEnum::UNKNOWN:
      return mantle_api::LightMode::kUndefined;
    case NET_ASAM_OPENSCENARIO::v1_3::LightMode::LightModeEnum::FLASHING:
      return mantle_api::LightMode::kFlashing;
    case NET_ASAM_OPENSCENARIO::v1_3::LightMode::LightModeEnum::OFF:
      return mantle_api::LightMode::kOff;
    case NET_ASAM_OPENSCENARIO::v1_3::LightMode::LightModeEnum::ON:
      return mantle_api::LightMode::kOn;
    default:
      return mantle_api::LightMode::kUndefined;
  }
}

}  // namespace detail

mantle_api::LightState ConvertScenarioLightState(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ILightState>& lightState)
{
  return {detail::ConvertLightMode(lightState->GetMode())};
}

}  // namespace OpenScenarioEngine::v1_3
