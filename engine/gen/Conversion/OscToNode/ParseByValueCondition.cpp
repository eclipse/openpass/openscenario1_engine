/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToNode/ParseByValueCondition.h"

#include <memory>

#include "Conversion/OscToNode/ParseParameterCondition.h"
#include "Conversion/OscToNode/ParseSimulationTimeCondition.h"
#include "Conversion/OscToNode/ParseStoryboardElementStateCondition.h"
#include "Conversion/OscToNode/ParseTimeOfDayCondition.h"
#include "Conversion/OscToNode/ParseTrafficSignalCondition.h"
#include "Conversion/OscToNode/ParseTrafficSignalControllerCondition.h"
#include "Conversion/OscToNode/ParseUserDefinedValueCondition.h"
#include "Conversion/OscToNode/ParseVariableCondition.h"

namespace OpenScenarioEngine::v1_3
{
yase::BehaviorNode::Ptr parse(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IByValueCondition> byValueCondition)
{
  if (auto element = byValueCondition->GetParameterCondition(); element)
  {
    return parse(element);
  }
  if (auto element = byValueCondition->GetSimulationTimeCondition(); element)
  {
    return parse(element);
  }
  if (auto element = byValueCondition->GetStoryboardElementStateCondition(); element)
  {
    return parse(element);
  }
  if (auto element = byValueCondition->GetTimeOfDayCondition(); element)
  {
    return parse(element);
  }
  if (auto element = byValueCondition->GetTrafficSignalCondition(); element)
  {
    return parse(element);
  }
  if (auto element = byValueCondition->GetTrafficSignalControllerCondition(); element)
  {
    return parse(element);
  }
  if (auto element = byValueCondition->GetUserDefinedValueCondition(); element)
  {
    return parse(element);
  }
  if (auto element = byValueCondition->GetVariableCondition(); element)
  {
    return parse(element);
  }
  throw std::runtime_error("Corrupted openSCENARIO file: No choice made within std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IByValueCondition>");
}

}  // namespace OpenScenarioEngine::v1_3
