/*******************************************************************************
 * Copyright (c) 2025 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

namespace OpenScenarioEngine::v1_3
{
/// Definition of event priority
///
/// \see https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/generated/content/Priority.html
enum class EventPriority
{
  kOverride,
  kOverwrite,
  kParallel,
  kSkip,
  kUnknown
};
}  // namespace OpenScenarioEngine::v1_3
