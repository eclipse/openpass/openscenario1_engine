/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Node/EntityConditionNode.h"

#include "Conversion/OscToNode/ParseEntityCondition.h"

namespace OpenScenarioEngine::v1_3::Node
{
EntityConditionNode::EntityConditionNode(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IEntityCondition> entityCondition,
    const Entity triggeringEntity)
    : DecoratorNode{"EntityCondition"},
      triggeringEntity_{std::move(triggeringEntity)}
{
  setChild(parse(entityCondition));
}

void EntityConditionNode::lookupAndRegisterData(yase::Blackboard &blackboard)
{
  blackboard.set("TriggeringEntity", triggeringEntity_);
}

yase::NodeStatus EntityConditionNode::tick()
{
    return child().executeTick();
}

}  // namespace OpenScenarioEngine::v1_3::Node
