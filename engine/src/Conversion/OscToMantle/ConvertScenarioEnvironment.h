
/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "Utils/Constants.h"

#include <MantleAPI/Common/time_utils.h>
#include <MantleAPI/EnvironmentalConditions/weather.h>

#include <optional>

namespace OpenScenarioEngine::v1_3
{

struct Environment
{
  std::optional<mantle_api::Time> date_time;
  std::optional<mantle_api::Weather> weather;
};

Environment ConvertScenarioEnvironment(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IEnvironment>& environment,
                                       const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ICatalogReference>& catalogReference);

}  // namespace OpenScenarioEngine::v1_3
