/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <utility>

#include "Storyboard/MotionControlAction/SpeedAction_base.h"

namespace OpenScenarioEngine::v1_3
{
class SpeedAction : public SpeedActionBase
{
public:
  SpeedAction(Values values, Interfaces interfaces)
      : SpeedActionBase{std::move(values), std::move(interfaces)} {};

  virtual ~SpeedAction() = default;

  void SetControlStrategy() override;
  bool HasControlStrategyGoalBeenReached(const std::string &actor) override;
  [[nodiscard]] mantle_api::MovementDomain GetMovementDomain() const override;

private:
  void SetSpline(mantle_api::UniqueId entity_id,
                 units::velocity::meters_per_second_t target_speed,
                 const std::vector<mantle_api::SplineSection<units::velocity::meters_per_second>> &spline_sections);
  void SetLinearVelocitySplineControlStrategy(const std::string &actor);
};

}  // namespace OpenScenarioEngine::v1_3