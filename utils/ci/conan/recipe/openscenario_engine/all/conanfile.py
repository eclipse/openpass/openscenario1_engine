################################################################################
# Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# Install file for building open scenario engine with Conan
################################################################################
import os

from conan import ConanFile
from conan.tools.cmake import CMake, CMakeToolchain
from conan.tools.files import copy, rm, export_conandata_patches, apply_conandata_patches
from conan.tools.scm import Git

required_conan_version = ">=1.53.0"

class OpenScenarioEngineConan(ConanFile):
    name = "openscenario_engine"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False],
               "fPIC": [True, False],
               "MantleAPI_version":["ANY"],
               "openscenario_api_version":["ANY"],
               "units_version":["ANY"],
               "Yase_version":["ANY"]
               }

    default_options = {"shared": False,
                       "fPIC": True,
                       "openscenario_api_version":"v1.3.1",
                       "units_version":"2.3.3"}

    no_copy_source = False
    short_paths = True

    def export_sources(self):
        export_conandata_patches(self)

    def requirements(self):
        self.requires(f"openscenario_api/{self.options.openscenario_api_version}@openpass/testing")
        self.requires(f"units/{self.options.units_version}@openpass/testing")
        self.requires(f"yase/{self.options.Yase_version}@openpass/testing")
        self.requires(f"mantleapi/{self.options.MantleAPI_version}@openpass/testing")

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC
       
    def _get_url_sha(self):
        if self.version in self.conan_data["sources"]:
            url = self.conan_data["sources"][self.version]["url"]
            sha256 = self.conan_data["sources"][self.version]["sha256"]
        else:
            url = self.conan_data["sources"]["default"]["url"]
            sha256 = self.version

        return url, sha256

    def source(self):
        url, sha256 = self._get_url_sha()
        git = Git(self)
        git.clone(url=url, target=self.name)
        git.folder=self.name
        git.checkout(commit=sha256)
        apply_conandata_patches(self)

    def generate(self):
        _cmake_prefix_paths = []
        for _, dependency in self.dependencies.items():
            _cmake_prefix_paths.append(dependency.package_folder)
        _cmake_prefix_paths = ';'.join(str(_cmake_prefix_path) for _cmake_prefix_path in _cmake_prefix_paths)
        tc = CMakeToolchain(self)
        tc.cache_variables["CMAKE_PREFIX_PATH"] = _cmake_prefix_paths
        tc.generate()

    def build(self):
        cmake = CMake(self)
        cmake.configure(build_script_folder=self.name+"/engine")
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()
        copy(self, "*", src=os.path.join(self.source_folder, "engine/cmake"), dst=os.path.join(self.package_folder, "lib/cmake/OpenScenarioEngine/deps"))
        rm(self, "conanbuildinfo.txt", self.package_folder)
        rm(self, "conandata.yml", self.package_folder)
