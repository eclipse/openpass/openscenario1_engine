/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "builders/PositionBuilder.h"

namespace testing::OpenScenarioEngine::v1_3
{
FakePositionBuilder::FakePositionBuilder()
    : position_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::PositionImpl>()) {}

FakePositionBuilder& FakePositionBuilder::WithLanePosition(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ILanePositionWriter> lane_position)
{
    position_->SetLanePosition(lane_position);
    return *this;
}

FakePositionBuilder& FakePositionBuilder::WithWorldPosition(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IWorldPositionWriter> world_position)
{
    position_->SetWorldPosition(world_position);
    return *this;
}

FakePositionBuilder& FakePositionBuilder::WithGeoPosition(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IGeoPositionWriter> geo_position)
{
    position_->SetGeoPosition(geo_position);
    return *this;
}

FakePositionBuilder& FakePositionBuilder::WithRelativeLanePosition(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IRelativeLanePositionWriter> relative_lane_position)
{
    position_->SetRelativeLanePosition(relative_lane_position);
    return *this;
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IPositionWriter> FakePositionBuilder::Build()
{
    return position_;
}

FakeLanePositionBuilder::FakeLanePositionBuilder(std::string road_id, std::string lane_id, double offset, double s)
    : lane_position_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::LanePositionImpl>())
{
    lane_position_->SetRoadId(road_id);
    lane_position_->SetLaneId(lane_id);
    lane_position_->SetOffset(offset);
    lane_position_->SetS(s);
}

FakeLanePositionBuilder& FakeLanePositionBuilder::WithOrientation(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IOrientationWriter> orientation)
{
    lane_position_->SetOrientation(orientation);
    return *this;
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ILanePositionWriter> FakeLanePositionBuilder::Build()
{
    return lane_position_;
}

FakeWorldPositionBuilder::FakeWorldPositionBuilder(double x, double y)
    : world_position_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::WorldPositionImpl>())
{
    world_position_->SetX(x);
    world_position_->SetY(y);
}

FakeWorldPositionBuilder::FakeWorldPositionBuilder(double x, double y, double z, double h, double p, double r)
    : world_position_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::WorldPositionImpl>())
{
    world_position_->SetX(x);
    world_position_->SetY(y);
    world_position_->SetZ(z);
    world_position_->SetH(h);
    world_position_->SetP(p);
    world_position_->SetR(r);
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IWorldPositionWriter> FakeWorldPositionBuilder::Build()
{
    return world_position_;
}

FakeGeoPositionBuilder::FakeGeoPositionBuilder(double latitude, double longitude)
    : geo_position_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::GeoPositionImpl>())
{
    geo_position_->SetLatitude(latitude);
    geo_position_->SetLongitude(longitude);
}

FakeGeoPositionBuilder& FakeGeoPositionBuilder::WithOrientation(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IOrientationWriter> orientation)
{
    geo_position_->SetOrientation(orientation);
    return *this;
}

FakeRelativeLanePositionBuilder::FakeRelativeLanePositionBuilder(int relative_lane, double offset)
    : relative_lane_position_(std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::RelativeLanePositionImpl>())
{
    relative_lane_position_->SetDLane(relative_lane);
    relative_lane_position_->SetOffset(offset);
    auto fake_ref = std::make_shared<NET_ASAM_OPENSCENARIO::INamedReference<NET_ASAM_OPENSCENARIO::v1_3::IEntity>>();
    relative_lane_position_->SetEntityRef(fake_ref);
}

FakeRelativeLanePositionBuilder& FakeRelativeLanePositionBuilder::WithDs(double ds)
{
    relative_lane_position_->SetDs(ds);
    return *this;
}

FakeRelativeLanePositionBuilder& FakeRelativeLanePositionBuilder::WithDsLane(double ds_lane)
{
    relative_lane_position_->SetDsLane(ds_lane);
    return *this;
}

FakeRelativeLanePositionBuilder& FakeRelativeLanePositionBuilder::WithOrientation(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IOrientationWriter> orientation)
{
    relative_lane_position_->SetOrientation(orientation);
    return *this;
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IRelativeLanePositionWriter> FakeRelativeLanePositionBuilder::Build()
{
    return relative_lane_position_;
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IGeoPositionWriter> FakeGeoPositionBuilder::Build()
{
    return geo_position_;
}

}  // namespace testing::OpenScenarioEngine::v1_3
