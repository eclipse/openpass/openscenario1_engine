/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>
#include <openScenarioLib/generated/v1_3/impl/ApiClassImplV1_3.h>

#include "Conversion/OscToMantle/ConvertScenarioPosition.h"

using namespace units::literals;

class ConvertScenarioPositionTest : public ::testing::Test
{
protected:
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IPosition> GetWorldPosition()
  {
    auto world_pos = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::WorldPositionImpl>();
    world_pos->SetX(1);
    world_pos->SetY(2);
    world_pos->SetZ(3);
    world_pos->SetH(4);
    world_pos->SetP(5);
    world_pos->SetR(6);
    auto pos = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::PositionImpl>();
    pos->SetWorldPosition(world_pos);
    return pos;
  }

  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IPosition> GetGeoPosition(
      std::optional<mantle_api::Orientation3<units::angle::radian_t>> opt_orientation = std::nullopt,
      NET_ASAM_OPENSCENARIO::v1_3::ReferenceContext type =
          NET_ASAM_OPENSCENARIO::v1_3::ReferenceContext::ABSOLUTE,
      bool set_degree = false,
      bool set_rad = true) const
  {
    auto geo_pos = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::GeoPositionImpl>();
    if (set_degree)
    {
      geo_pos->SetLatitudeDeg(42.123);
      geo_pos->SetLongitudeDeg(11.65874);
      EXPECT_CALL(dynamic_cast<mantle_api::MockConverter&>(*(mockEnvironment->GetConverter())),
                  Convert(mantle_api::Position{lat_lon_position_deg}))
          .Times(1)
          .WillRepeatedly(testing::Return(expected_position_));
    }
    if (set_rad)
    {
      geo_pos->SetLatitude(1.5);
      geo_pos->SetLongitude(0.5);
      if (!set_degree)
      {
        EXPECT_CALL(dynamic_cast<mantle_api::MockConverter&>(*(mockEnvironment->GetConverter())),
                    Convert(mantle_api::Position{lat_lon_position_}))
            .Times(1)
            .WillRepeatedly(testing::Return(expected_position_));
      }
    }

    if (opt_orientation.has_value())
    {
      auto orientation_writer = GetOrientationWriter(opt_orientation.value(), type);
      geo_pos->SetOrientation(orientation_writer);
    }

    auto pos = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::PositionImpl>();
    pos->SetGeoPosition(geo_pos);

    return pos;
  }

  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IPosition> GetLanePosition(
      std::optional<mantle_api::Orientation3<units::angle::radian_t>> opt_orientation = std::nullopt,
      NET_ASAM_OPENSCENARIO::v1_3::ReferenceContext type =
          NET_ASAM_OPENSCENARIO::v1_3::ReferenceContext::ABSOLUTE) const
  {
    EXPECT_CALL(dynamic_cast<mantle_api::MockConverter&>(*(mockEnvironment->GetConverter())),
                Convert(testing::_))
        .Times(1)
        .WillRepeatedly(testing::Return(expected_position_));

    auto lane_pos = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::LanePositionImpl>();
    lane_pos->SetRoadId("road1");
    lane_pos->SetLaneId("-2");
    lane_pos->SetOffset(4.0);
    lane_pos->SetS(3.0);

    if (opt_orientation.has_value())
    {
      auto orientation_writer = GetOrientationWriter(opt_orientation.value(), type);
      lane_pos->SetOrientation(orientation_writer);
    }

    auto pos = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::PositionImpl>();
    pos->SetLanePosition(lane_pos);
    return pos;
  }

  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IPosition> GetRoadPosition(
      std::optional<mantle_api::Orientation3<units::angle::radian_t>> opt_orientation = std::nullopt,
      NET_ASAM_OPENSCENARIO::v1_3::ReferenceContext type =
          NET_ASAM_OPENSCENARIO::v1_3::ReferenceContext::ABSOLUTE) const
  {
    EXPECT_CALL(dynamic_cast<mantle_api::MockConverter&>(*(mockEnvironment->GetConverter())),
                Convert(testing::_))
        .Times(1)
        .WillRepeatedly(testing::Return(expected_position_));

    auto road_pos = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::RoadPositionImpl>();
    road_pos->SetRoadId("road1");
    road_pos->SetT(4.0);
    road_pos->SetS(3.0);

    if (opt_orientation.has_value())
    {
      auto orientation_writer = GetOrientationWriter(opt_orientation.value(), type);
      road_pos->SetOrientation(orientation_writer);
    }

    auto pos = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::PositionImpl>();
    pos->SetRoadPosition(road_pos);
    return pos;
  }

  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IPosition> GetRelativeLanePosition(
      double ds = 0.0,
      std::optional<mantle_api::Orientation3<units::angle::radian_t>> opt_orientation = std::nullopt,
      NET_ASAM_OPENSCENARIO::v1_3::ReferenceContext type =
          NET_ASAM_OPENSCENARIO::v1_3::ReferenceContext::ABSOLUTE) const
  {
    auto relative_lane_pos = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::RelativeLanePositionImpl>();

    relative_lane_pos->SetDLane(relative_lane_);
    relative_lane_pos->SetOffset(offset_);
    auto fake_ref = std::make_shared<NET_ASAM_OPENSCENARIO::INamedReference<NET_ASAM_OPENSCENARIO::v1_3::IEntity>>();
    relative_lane_pos->SetEntityRef(fake_ref);

    if (ds > 0)
    {
      relative_lane_pos->SetDs(ds);
    }
    else
    {
      relative_lane_pos->SetDsLane(distance_);
    }
    if (opt_orientation.has_value())
    {
      auto orientation_writer = GetOrientationWriter(opt_orientation.value(), type);
      relative_lane_pos->SetOrientation(orientation_writer);
    }
    auto pos = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::PositionImpl>();
    pos->SetRelativeLanePosition(relative_lane_pos);
    return pos;
  }

  mantle_api::LatLonPosition lat_lon_position_{1.5_rad, 0.5_rad};
  mantle_api::LatLonPosition lat_lon_position_deg{42.123_deg, 11.65874_deg};
  mantle_api::OpenDriveLanePosition open_drive_lane_position_{"road1", -2, 3.0_m, 4.0_m};
  mantle_api::Vec3<units::length::meter_t> expected_position_{1_m, 2_m, 3_m};
  int relative_lane_{1};
  double distance_{10.0};
  double offset_{0.0};

  std::shared_ptr<mantle_api::MockEnvironment> mockEnvironment{std::make_shared<mantle_api::MockEnvironment>()};

private:
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IOrientationWriter> GetOrientationWriter(
      mantle_api::Orientation3<units::angle::radian_t> orientation,
      NET_ASAM_OPENSCENARIO::v1_3::ReferenceContext type) const
  {
    auto orientation_writer = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::OrientationImpl>();
    orientation_writer->SetH(orientation.yaw());
    orientation_writer->SetP(orientation.pitch());
    orientation_writer->SetR(orientation.roll());
    orientation_writer->SetType(type);

    return orientation_writer;
  }
};

TEST_F(ConvertScenarioPositionTest,
       GivenWorldPositionWithOrientation_WhenConverting_ThenPositionConvertedAndOrientationSet)
{
  auto pos = GetWorldPosition();
  const auto pose = OpenScenarioEngine::v1_3::ConvertScenarioPosition(mockEnvironment, pos);

  ASSERT_EQ(expected_position_, pose->position);
  ASSERT_EQ(mantle_api::Orientation3<units::angle::radian_t>(4_rad, 5_rad, 6_rad), pose->orientation);
}

TEST_F(ConvertScenarioPositionTest,
       GivenGeoPositionInRadWithNoOrientation_WhenConverting_ThenPositionConvertedAndOrientationZero)
{
  auto pos = GetGeoPosition();
  const auto pose = OpenScenarioEngine::v1_3::ConvertScenarioPosition(mockEnvironment, pos);

  ASSERT_EQ(expected_position_, pose->position);
  ASSERT_EQ(mantle_api::Orientation3<units::angle::radian_t>(0_rad, 0_rad, 0_rad), pose->orientation);
}

TEST_F(ConvertScenarioPositionTest,
       GivenGeoPositionInDegree_WhenConverting_ThenPositionConverted)
{
  auto pos = GetGeoPosition(std::nullopt, NET_ASAM_OPENSCENARIO::v1_3::ReferenceContext::ABSOLUTE, true, false);
  const auto pose = OpenScenarioEngine::v1_3::ConvertScenarioPosition(mockEnvironment, pos);

  ASSERT_EQ(expected_position_, pose->position);
}

TEST_F(ConvertScenarioPositionTest,
       GivenGeoPositionInRadAndDegree_WhenConverting_ThenDegreePositionConverted)
{
  auto pos = GetGeoPosition(std::nullopt, NET_ASAM_OPENSCENARIO::v1_3::ReferenceContext::ABSOLUTE, true, true);
  const auto pose = OpenScenarioEngine::v1_3::ConvertScenarioPosition(mockEnvironment, pos);

  ASSERT_EQ(expected_position_, pose->position);
}

TEST_F(ConvertScenarioPositionTest,
       GivenGeoPositionWithoutLatLon_WhenConverting_ThenThrow)
{
  auto pos = GetGeoPosition(std::nullopt, NET_ASAM_OPENSCENARIO::v1_3::ReferenceContext::ABSOLUTE, false, false);
  EXPECT_ANY_THROW(OpenScenarioEngine::v1_3::ConvertScenarioPosition(mockEnvironment, pos));
}

TEST_F(ConvertScenarioPositionTest,
       GivenGeoPositionWithAbsoluteOrientation_WhenConverting_ThenOrientationIsReturnedUnchanged)
{
  mantle_api::Orientation3<units::angle::radian_t> expected_orientation{1.2_rad, 2.3_rad, 5.0_rad};

  auto pos = GetGeoPosition(expected_orientation, NET_ASAM_OPENSCENARIO::v1_3::ReferenceContext::ABSOLUTE);
  const auto pose = OpenScenarioEngine::v1_3::ConvertScenarioPosition(mockEnvironment, pos);

  ASSERT_EQ(expected_orientation, pose->orientation);
}

TEST_F(ConvertScenarioPositionTest,
       GivenGeoPositionWithRelativeOrientation_WhenConverting_ThenOrientationIsRelativeToLaneOrientation)
{
  mantle_api::Orientation3<units::angle::radian_t> lane_orientation{10_rad, 11_rad, 12_rad};
  mantle_api::Orientation3<units::angle::radian_t> expected_orientation{1.2_rad, 2.3_rad, 5.0_rad};
  EXPECT_CALL(dynamic_cast<const mantle_api::MockQueryService&>(mockEnvironment->GetQueryService()),
              GetLaneOrientation(expected_position_))
      .Times(1)
      .WillRepeatedly(testing::Return(lane_orientation));

  auto pos = GetGeoPosition(expected_orientation, NET_ASAM_OPENSCENARIO::v1_3::ReferenceContext::RELATIVE);
  const auto pose = OpenScenarioEngine::v1_3::ConvertScenarioPosition(mockEnvironment, pos);

  ASSERT_EQ(expected_orientation + lane_orientation, pose->orientation);
}

TEST_F(ConvertScenarioPositionTest,
       GivenGeoPositionOnRoad_WhenConverting_ThenAltitudeOnRoadSurface)
{
  auto pos = GetGeoPosition();
  EXPECT_CALL(dynamic_cast<const mantle_api::MockQueryService&>(mockEnvironment->GetQueryService()),
              IsPositionOnLane(expected_position_))
      .Times(1)
      .WillRepeatedly(testing::Return(true));
  EXPECT_CALL(dynamic_cast<const mantle_api::MockQueryService&>(mockEnvironment->GetQueryService()),
              GetLaneHeightAtPosition(expected_position_))
      .Times(1)
      .WillRepeatedly(testing::Return(5_m));

  const auto pose = OpenScenarioEngine::v1_3::ConvertScenarioPosition(mockEnvironment, pos);

  EXPECT_EQ(mantle_api::Vec3<units::length::meter_t>(1_m, 2_m, 5_m), pose->position);
}

TEST_F(ConvertScenarioPositionTest,
       GivenGeoPositionNotOnRoad_WhenConverting_ThenDefaultAltitude)
{
  auto pos = GetGeoPosition();
  EXPECT_CALL(dynamic_cast<const mantle_api::MockQueryService&>(mockEnvironment->GetQueryService()),
              IsPositionOnLane(expected_position_))
      .Times(1)
      .WillRepeatedly(testing::Return(false));
  EXPECT_CALL(dynamic_cast<const mantle_api::MockQueryService&>(mockEnvironment->GetQueryService()),
              GetLaneHeightAtPosition(expected_position_))
      .Times(0);

  const auto pose = OpenScenarioEngine::v1_3::ConvertScenarioPosition(mockEnvironment, pos);

  EXPECT_EQ(expected_position_, pose->position);
}

TEST_F(ConvertScenarioPositionTest,
       GivenRelativeLanePositionWithNoOrientation_WhenConverting_ThenPositionConvertedAndOrientationZero)
{
  auto pos = GetRelativeLanePosition();
  const auto pose = OpenScenarioEngine::v1_3::ConvertScenarioPosition(mockEnvironment, pos);

  ASSERT_EQ(mantle_api::Vec3<units::length::meter_t>(0_m, 0_m, 0_m), pose->position);
  ASSERT_EQ(mantle_api::Orientation3<units::angle::radian_t>(0_rad, 0_rad, 0_rad), pose->orientation);
}

TEST_F(ConvertScenarioPositionTest,
       GivenRelativeLanePositionWithAbsoluteOrientation_WhenConverting_ThenOrientationIsReturnedUnchanged)
{
  mantle_api::Orientation3<units::angle::radian_t> expected_orientation{1.2_rad, 2.3_rad, 5.0_rad};
  EXPECT_CALL(dynamic_cast<const mantle_api::MockQueryService&>(mockEnvironment->GetQueryService()),
              GetLaneOrientation(testing::_))
      .Times(0);

  auto pos =
      GetRelativeLanePosition(0, expected_orientation, NET_ASAM_OPENSCENARIO::v1_3::ReferenceContext::ABSOLUTE);
  const auto pose = OpenScenarioEngine::v1_3::ConvertScenarioPosition(mockEnvironment, pos);

  ASSERT_EQ(expected_orientation, pose->orientation);
}

TEST_F(ConvertScenarioPositionTest,
       GivenRelativeLanePositionWithDSValueAndAbsoluteOrientation_WhenConverting_ThenReturnEmptyPose)
{
  mantle_api::Orientation3<units::angle::radian_t> expected_orientation{0_rad, 0_rad, 0_rad};
  EXPECT_CALL(dynamic_cast<const mantle_api::MockQueryService&>(mockEnvironment->GetQueryService()),
              GetLaneOrientation(testing::_))
      .Times(0);

  auto pos =
      GetRelativeLanePosition(10, expected_orientation, NET_ASAM_OPENSCENARIO::v1_3::ReferenceContext::ABSOLUTE);
  const auto pose = OpenScenarioEngine::v1_3::ConvertScenarioPosition(mockEnvironment, pos);

  ASSERT_EQ(expected_orientation, pose->orientation);
  ASSERT_EQ(mantle_api::Vec3<units::length::meter_t>(0_m, 0_m, 0_m), pose->position);
}

TEST_F(ConvertScenarioPositionTest,
       GivenRelativeLanePositionWithRelativeOrientation_WhenConverting_ThenOrientationIsIsRelativeToLaneOrientation)
{
  mantle_api::Orientation3<units::angle::radian_t> lane_orientation{10_rad, 11_rad, 12_rad};
  mantle_api::Orientation3<units::angle::radian_t> expected_orientation{1.2_rad, 2.3_rad, 5.0_rad};
  EXPECT_CALL(dynamic_cast<const mantle_api::MockQueryService&>(mockEnvironment->GetQueryService()),
              GetLaneOrientation(testing::_))
      .Times(1)
      .WillRepeatedly(testing::Return(lane_orientation));

  auto pos =
      GetRelativeLanePosition(0, expected_orientation, NET_ASAM_OPENSCENARIO::v1_3::ReferenceContext::RELATIVE);
  const auto pose = OpenScenarioEngine::v1_3::ConvertScenarioPosition(mockEnvironment, pos);

  ASSERT_EQ(lane_orientation + expected_orientation, pose->orientation);
}

TEST_F(ConvertScenarioPositionTest,
       GivenRelativeLanePositionWithDSValueAndRelativeOrientation_WhenConverting_ThenReturnEmptyPose)
{
  mantle_api::Orientation3<units::angle::radian_t> lane_orientation{10_rad, 11_rad, 12_rad};
  mantle_api::Orientation3<units::angle::radian_t> expected_orientation{0_rad, 0_rad, 0_rad};
  EXPECT_CALL(dynamic_cast<const mantle_api::MockQueryService&>(mockEnvironment->GetQueryService()),
              GetLaneOrientation(testing::_))
      .Times(0);

  auto pos =
      GetRelativeLanePosition(10, expected_orientation, NET_ASAM_OPENSCENARIO::v1_3::ReferenceContext::RELATIVE);
  const auto pose = OpenScenarioEngine::v1_3::ConvertScenarioPosition(mockEnvironment, pos);

  ASSERT_EQ(expected_orientation, pose->orientation);
  ASSERT_EQ(mantle_api::Vec3<units::length::meter_t>(0_m, 0_m, 0_m), pose->position);
}

TEST_F(ConvertScenarioPositionTest,
       GivenLanePositionWithNoOrientation_WhenConverting_ThenPositionConvertedAndOrientationZero)
{
  auto pos = GetLanePosition();
  const auto pose = OpenScenarioEngine::v1_3::ConvertScenarioPosition(mockEnvironment, pos);

  ASSERT_EQ(expected_position_, pose->position);
  ASSERT_EQ(mantle_api::Orientation3<units::angle::radian_t>(0_rad, 0_rad, 0_rad), pose->orientation);
}

TEST_F(ConvertScenarioPositionTest,
       GivenLanePositionWithAbsoluteOrientation_WhenConverting_ThenOrientationIsReturnedUnchanged)
{
  mantle_api::Orientation3<units::angle::radian_t> expected_orientation{1.2_rad, 2.3_rad, 5.0_rad};

  auto pos = GetLanePosition(expected_orientation, NET_ASAM_OPENSCENARIO::v1_3::ReferenceContext::ABSOLUTE);
  const auto pose = OpenScenarioEngine::v1_3::ConvertScenarioPosition(mockEnvironment, pos);

  ASSERT_EQ(expected_orientation, pose->orientation);
}

TEST_F(ConvertScenarioPositionTest,
       GivenLanePositionWithRelativeOrientation_WhenConverting_ThenOrientationIsRelativeToLaneOrientation)
{
  mantle_api::Orientation3<units::angle::radian_t> lane_orientation{10_rad, 11_rad, 12_rad};
  mantle_api::Orientation3<units::angle::radian_t> expected_orientation{1.2_rad, 2.3_rad, 5.0_rad};
  EXPECT_CALL(dynamic_cast<mantle_api::MockConverter&>(*(mockEnvironment->GetConverter())),
              GetLaneOrientation(testing::_))
      .Times(1)
      .WillRepeatedly(testing::Return(lane_orientation));

  auto pos = GetLanePosition(expected_orientation, NET_ASAM_OPENSCENARIO::v1_3::ReferenceContext::RELATIVE);
  const auto pose = OpenScenarioEngine::v1_3::ConvertScenarioPosition(mockEnvironment, pos);

  ASSERT_EQ(expected_orientation + lane_orientation, pose->orientation);
}

TEST_F(ConvertScenarioPositionTest,
       GivenRoadPositionWithAbsoluteOrientation_WhenConverting_ThenOrientationIsReturnedUnchanged)
{
  mantle_api::Orientation3<units::angle::radian_t> expected_orientation{1.2_rad, 2.3_rad, 5.0_rad};

  auto pos = GetRoadPosition(expected_orientation, NET_ASAM_OPENSCENARIO::v1_3::ReferenceContext::ABSOLUTE);
  const auto pose = OpenScenarioEngine::v1_3::ConvertScenarioPosition(mockEnvironment, pos);

  ASSERT_EQ(expected_orientation, pose->orientation);
}

TEST_F(ConvertScenarioPositionTest,
       GivenRoadPositionWithRelativeOrientation_WhenConverting_ThenOrientationIsRelativeToRoadOrientation)
{
  mantle_api::Orientation3<units::angle::radian_t> road_orientation{10_rad, 11_rad, 12_rad};
  mantle_api::Orientation3<units::angle::radian_t> expected_orientation{1.2_rad, 2.3_rad, 5.0_rad};
  EXPECT_CALL(dynamic_cast<mantle_api::MockConverter&>(*(mockEnvironment->GetConverter())),
              GetRoadOrientation(testing::_))
      .Times(1)
      .WillRepeatedly(testing::Return(road_orientation));

  auto pos = GetRoadPosition(expected_orientation, NET_ASAM_OPENSCENARIO::v1_3::ReferenceContext::RELATIVE);
  const auto pose = OpenScenarioEngine::v1_3::ConvertScenarioPosition(mockEnvironment, pos);

  ASSERT_EQ(expected_orientation + road_orientation, pose->orientation);
}
