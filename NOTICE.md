# Notices for Eclipse openpass

This content is produced and maintained by the Eclipse openpass project.

 * Project home: https://projects.eclipse.org/projects/automotive.openpass

## Copyright

All content is the property of the respective authors or their employers.
For more information regarding authorship of content, please consult the
listed source code repository logs.

## Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
https://www.eclipse.org/legal/epl-2.0/.

SPDX-License-Identifier: EPL-2.0

## Source Code

The project maintains the following source code repositories:

 * https://gitlab.eclipse.org/eclipse/openpass/openscenario1-engine

## Third-party Content

Google Protobuf (3.20.0)

 * License: New BSD License
 * Source: https://github.com/protocolbuffers/protobuf

Google Test (1.13.0)

 * License: New BSD License
 * Source: https://github.com/google/googletest

ca-certificates
 * License: MPL 2.0

libboost-filesystem-dev
 * License: Boost Software License

libgmock-dev
 * License: BSD License

libqt5xmlpatterns5-dev
 * License: LGPL 3.0

openjdk-17-jre
* License: GPL 2.0 with Classpath Exception

python3
 * License: Python Software Foundation License

python3-distutils
 * License: Python Software Foundation License

python3-pip
 * License: MIT

qtbase5-dev
 * License: LGPL 3.0

uuid-dev
 * License: BSD License

Bazel
 * License: Apache-2.0

Bazel Rules
 * License: Apache-2.0

Yase
 * License: EPL 2.0

units_nhh
 * License: MIT

MantleAPI
 * License: EPL 2.0

Antlr4Runtime
 * License: BSD License

pthread
 * License: LGPL 2.1

OpenSCENARIO API (Parser)
 * License: Apache-2.0

openpass/stochastics-library
 * License: EPL 2.0

Boost
 * License: Boost Software License 1.0

## Cryptography

Content may contain encryption software. The country in which you are currently
may have restrictions on the import, possession, and use, and/or re-export to
another country, of encryption software. BEFORE using any encryption software,
please check the country's laws, regulations and policies concerning the import,
possession, or use, and re-export of encryption software, to see if this is
permitted.
