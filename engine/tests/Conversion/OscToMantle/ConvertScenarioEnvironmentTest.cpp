/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gtest/gtest.h>
#include <openScenarioLib/generated/v1_3/impl/ApiClassImplV1_3.h>
#include <openScenarioLib/src/common/SimpleMessageLogger.h>
#include <openScenarioLib/src/loader/FileResourceLocator.h>
#include <openScenarioLib/src/v1_3/loader/XmlScenarioImportLoaderFactoryV1_3.h>

#include "Conversion/OscToMantle/ConvertScenarioEnvironment.h"
#include "TestUtils.h"

class ScenarioDataLoaderTest : public testing::OpenScenarioEngine::v1_3::EngineSubModuleTestBase
{
};

using EnvironmentGivenTypes = std::tuple<double,  // Fog
                                         double,  // Precipitation
                                         double,  // Illuminance
                                         double,  // Temperature
                                         double,  // Pressure
                                         double,  // Sun azimuth
                                         double,  // Sun elevation
                                         NET_ASAM_OPENSCENARIO::DateTime>;

using EnvironmentExpectedTypes = std::tuple<OpenScenarioEngine::v1_3::Environment>;

using EnvironmentGivenExpectedTypes = decltype(std::tuple_cat(std::declval<EnvironmentGivenTypes>(), std::declval<EnvironmentExpectedTypes>()));

using units::constants::detail::PI_VAL;

class ConvertEnvironmentTest : public ::testing::TestWithParam<EnvironmentGivenExpectedTypes>
{
protected:
  OpenScenarioEngine::v1_3::Environment ConvertEnvironment(EnvironmentGivenTypes given_values)
  {
    const auto fog = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::FogImpl>();
    fog->SetVisualRange(std::get<0>(given_values));

    const auto precipitation = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::PrecipitationImpl>();
    precipitation->SetPrecipitationIntensity(std::get<1>(given_values));

    const auto sun = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::SunImpl>();
    sun->SetIlluminance(std::get<2>(given_values));
    sun->SetAzimuth(std::get<5>(given_values));
    sun->SetElevation(std::get<6>(given_values));

    const auto weather = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::WeatherImpl>();
    weather->SetFog(fog);
    weather->SetPrecipitation(precipitation);
    weather->SetSun(sun);
    weather->SetTemperature(std::get<3>(given_values));
    weather->SetAtmosphericPressure(std::get<4>(given_values));

    const auto environment = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::EnvironmentImpl>();
    environment->SetWeather(weather);

    const auto time_of_day = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::TimeOfDayImpl>();
    time_of_day->SetDateTime(std::get<7>(given_values));
    environment->SetTimeOfDay(time_of_day);

    return OpenScenarioEngine::v1_3::ConvertScenarioEnvironment(environment, nullptr);
  }
};

template <typename T>
T PreviousNearest(const T& num)
{
  return std::nextafter(num, -std::numeric_limits<T>::infinity());
}

template <class ParamType, typename GivenType, typename SetValueFunc>
std::vector<ParamType> GenerateParameters(ParamType param,
                                          const std::vector<GivenType> given_expected,
                                          SetValueFunc set_value)
{
  std::vector<ParamType> params;

  for (const auto& val : given_expected)
  {
    set_value(param, val);
    params.push_back(param);
  }

  return params;
}

auto default_param = std::make_tuple(PreviousNearest(0.0),  // Fog
                                     PreviousNearest(0.0),  // Precipitation
                                     PreviousNearest(0.0),  // Illumanation
                                     PreviousNearest(0.0),  // Temperature
                                     PreviousNearest(0.0),  // Pressure
                                     PreviousNearest(0.0),  // Sun azimuth
                                     PreviousNearest(0.0),  // Sun elevation
                                     NET_ASAM_OPENSCENARIO::DateTime{0.0, 0, 0, 1, 0, 70, 0, 0},
                                     OpenScenarioEngine::v1_3::Environment{units::time::millisecond_t{0.0},
                                                                           mantle_api::Weather{mantle_api::Fog::kOther,
                                                                                               mantle_api::Precipitation::kOther,
                                                                                               mantle_api::Illumination::kOther,
                                                                                               units::concentration::percent_t{0.0},  // not used
                                                                                               units::temperature::kelvin_t{0.0},
                                                                                               units::pressure::pascal_t{0.0},
                                                                                               mantle_api::Sun{units::angle::radian_t{0.0},
                                                                                                               units::angle::radian_t{0.0},
                                                                                                               units::illuminance::lux_t{0.0}}}
                                                                           });

INSTANTIATE_TEST_SUITE_P(
    FogTests,
    ConvertEnvironmentTest,
    ::testing::ValuesIn(
        GenerateParameters(
            default_param,
            std::vector{
                std::make_tuple(0.0d, mantle_api::Fog::kOther),
                std::make_tuple(50.0d, mantle_api::Fog::kDense),
                std::make_tuple(200.0d, mantle_api::Fog::kThick),
                std::make_tuple(1000.0d, mantle_api::Fog::kLight),
                std::make_tuple(2000.0d, mantle_api::Fog::kMist),
                std::make_tuple(4000.0d, mantle_api::Fog::kPoorVisibility),
                std::make_tuple(10000.0d, mantle_api::Fog::kModerateVisibility),
                std::make_tuple(40000.0d, mantle_api::Fog::kGoodVisibility),
                std::make_tuple(std::numeric_limits<double>::max(), mantle_api::Fog::kExcellentVisibility)},
            [](EnvironmentGivenExpectedTypes& param, const std::tuple<double, mantle_api::Fog>& fog) {
              std::get<0>(param) = PreviousNearest(std::get<0>(fog));
              std::get<8>(param).weather.value().fog = std::get<1>(fog);
            })));

INSTANTIATE_TEST_SUITE_P(
    PrecipitationTests,
    ConvertEnvironmentTest,
    ::testing::ValuesIn(
        GenerateParameters(
            default_param,
            std::vector{
                std::make_tuple(0.0d, mantle_api::Precipitation::kOther),
                std::make_tuple(0.1d, mantle_api::Precipitation::kNone),
                std::make_tuple(0.5d, mantle_api::Precipitation::kVeryLight),
                std::make_tuple(1.9d, mantle_api::Precipitation::kLight),
                std::make_tuple(8.1d, mantle_api::Precipitation::kModerate),
                std::make_tuple(34.0d, mantle_api::Precipitation::kHeavy),
                std::make_tuple(149.0d, mantle_api::Precipitation::kVeryHeavy),
                std::make_tuple(std::numeric_limits<double>::max(), mantle_api::Precipitation::kExtreme)},
            [](EnvironmentGivenExpectedTypes& param, const std::tuple<double, mantle_api::Precipitation>& precipitation) {
              std::get<1>(param) = PreviousNearest(std::get<0>(precipitation));
              std::get<8>(param).weather.value().precipitation = std::get<1>(precipitation);
            })));

INSTANTIATE_TEST_SUITE_P(
    AzimuthTests,
    ConvertEnvironmentTest,
    ::testing::ValuesIn(
        GenerateParameters(
            default_param,
            std::vector{
                std::make_tuple(0.0d,    units::angle::radian_t{0.0}),
                std::make_tuple(0.001d,  units::angle::radian_t{2*PI_VAL-0.001}),
                std::make_tuple(0.01d,   units::angle::radian_t{2*PI_VAL-0.01}),
                std::make_tuple(1.57d,   units::angle::radian_t{2*PI_VAL-1.57}),
                std::make_tuple(3.14d,   units::angle::radian_t{2*PI_VAL-3.14}),
                std::make_tuple(6.28d,   units::angle::radian_t{2*PI_VAL-6.28}),
                std::make_tuple(-0.001d, units::angle::radian_t{0.001}),
                std::make_tuple(-0.01d,  units::angle::radian_t{0.01}),
                std::make_tuple(-1.57d,  units::angle::radian_t{1.57}),
                std::make_tuple(-3.14d,  units::angle::radian_t{3.14}),
                std::make_tuple(-6.28d,  units::angle::radian_t{6.28}),
                std::make_tuple(2*PI_VAL+0.001d,  units::angle::radian_t{2*PI_VAL-0.001}),
                std::make_tuple(2*PI_VAL+0.01d,   units::angle::radian_t{2*PI_VAL-0.01}),
                std::make_tuple(2*PI_VAL+1.57d,   units::angle::radian_t{2*PI_VAL-1.57}),
                std::make_tuple(2*PI_VAL+3.14d,   units::angle::radian_t{2*PI_VAL-3.14}),
                std::make_tuple(2*PI_VAL+6.28d,   units::angle::radian_t{2*PI_VAL-6.28})
            },
            [](EnvironmentGivenExpectedTypes& param, const std::tuple<double, units::angle::radian_t>& azimuth) {
                std::get<5>(param) = std::get<0>(azimuth);
                std::get<8>(param).weather.value().sun.azimuth = std::get<1>(azimuth);
            })));

INSTANTIATE_TEST_SUITE_P(
    ElevationTests,
    ConvertEnvironmentTest,
    ::testing::ValuesIn(
        GenerateParameters(
            default_param,
            std::vector{
                std::make_tuple(0.0d,   units::angle::radian_t{0.0}),
                std::make_tuple(0.001d, units::angle::radian_t{0.001}),
                std::make_tuple(0.01d,  units::angle::radian_t{0.01}),
                std::make_tuple(1.57d,  units::angle::radian_t{1.57}),
                std::make_tuple(3.14d,  units::angle::radian_t{3.14}),
                std::make_tuple(6.28d,  units::angle::radian_t{6.28})
            },
            [](EnvironmentGivenExpectedTypes& param, const std::tuple<double, units::angle::radian_t>& elevation) {
                std::get<6>(param) = std::get<0>(elevation);
                std::get<8>(param).weather.value().sun.elevation = std::get<1>(elevation);
            })));

using TemperatureGivenExpected = std::tuple<double, units::temperature::kelvin_t>;

INSTANTIATE_TEST_SUITE_P(
    TemperatureTests,
    ConvertEnvironmentTest,
    ::testing::ValuesIn(
        GenerateParameters(
            default_param,
            std::vector{
                std::make_tuple(
                    293.0d,
                    units::temperature::kelvin_t{293}  // 20°C
                    ),
                std::make_tuple(
                    272.0d,
                    units::temperature::kelvin_t{272}  // -1°C
                    )},
            [](EnvironmentGivenExpectedTypes& param, const TemperatureGivenExpected& temperature) {
              std::get<3>(param) = std::get<0>(temperature);
              std::get<8>(param).weather.value().temperature = std::get<1>(temperature);
            })));

using PressureGivenExpected = std::tuple<double, units::pressure::pascal_t>;

INSTANTIATE_TEST_SUITE_P(
    PressureTests,
    ConvertEnvironmentTest,
    ::testing::ValuesIn(
        GenerateParameters(
            default_param,
            std::vector{
                std::make_tuple(
                    10000.0d,
                    units::pressure::pascal_t{10000}),
                std::make_tuple(
                    9000.0d,
                    units::pressure::pascal_t{9000})},
            [](EnvironmentGivenExpectedTypes& param, const PressureGivenExpected& atmospheric_pressure) {
              std::get<4>(param) = std::get<0>(atmospheric_pressure);
              std::get<8>(param).weather.value().atmospheric_pressure = std::get<1>(atmospheric_pressure);
            })));

using Date = NET_ASAM_OPENSCENARIO::DateTime;

INSTANTIATE_TEST_SUITE_P(
    DateTests,
    ConvertEnvironmentTest,
    ::testing::ValuesIn(
        GenerateParameters(
            default_param,
            std::vector{
                std::make_tuple(
                    Date{
                        0,    // seconds
                        10,   // minutes
                        11,   // hour
                        26,   // month day
                        3,    // month (counting from 0, so 3 is April)
                        124,  // years from 1900
                        2,    // gmt hours
                        0     // gmt minutes
                    },
                    mantle_api::Time{1714122600.0l * 1000.0l})},
            [](EnvironmentGivenExpectedTypes& param, const std::tuple<Date, mantle_api::Time>& time) {
              std::get<7>(param) = std::get<0>(time);
              std::get<8>(param).date_time.value() = std::get<1>(time);
            })));

TEST_P(ConvertEnvironmentTest, GivenOSCEnvironmentValues_WhenConvertingToMantle_ThenCheckExpectedValues)
{
  OpenScenarioEngine::v1_3::Environment environment;

  EnvironmentGivenTypes given_values;

  std::tie(std::get<0>(given_values),  // Fog
           std::get<1>(given_values),  // Precipitation
           std::get<2>(given_values),  // Illumination
           std::get<3>(given_values),  // Temperature
           std::get<4>(given_values),  // Pressure
           std::get<5>(given_values),  // Sun azimuth
           std::get<6>(given_values),  // Sun elevation
           std::get<7>(given_values),  // Date
           environment) = GetParam();

  const auto converted_environment = ConvertEnvironment(given_values);

  EXPECT_EQ(converted_environment.date_time.value(), environment.date_time.value());
  EXPECT_EQ(converted_environment.weather.value().atmospheric_pressure, environment.weather.value().atmospheric_pressure);
  EXPECT_EQ(converted_environment.weather.value().temperature, environment.weather.value().temperature);
  EXPECT_EQ(converted_environment.weather.value().fog, environment.weather.value().fog);
  EXPECT_EQ(converted_environment.weather.value().illumination, environment.weather.value().illumination);
  EXPECT_EQ(converted_environment.weather.value().precipitation, environment.weather.value().precipitation);
  EXPECT_FLOAT_EQ(converted_environment.weather.value().sun.azimuth.value(), environment.weather.value().sun.azimuth.value());
  EXPECT_EQ(converted_environment.weather.value().sun.elevation, environment.weather.value().sun.elevation);
  EXPECT_EQ(converted_environment.weather.value().sun.intensity, environment.weather.value().sun.intensity);
}

TEST_F(ScenarioDataLoaderTest, GivenOSCCatalog_WhenConvertingToMantle_ThenCheckExpectedValues)
{
  OpenScenarioEngine::v1_3::Environment expected_environment{units::time::millisecond_t{1714122600.0l * 1000.0l},
                                                             mantle_api::Weather{mantle_api::Fog::kDense,
                                                                                 mantle_api::Precipitation::kNone,
                                                                                 mantle_api::Illumination::kOther,
                                                                                 units::concentration::percent_t{0.0},  // not used
                                                                                 units::temperature::kelvin_t{293},
                                                                                 units::pressure::pascal_t{101350},
                                                                                 mantle_api::Sun{units::angle::radian_t{2*PI_VAL-1.56},
                                                                                                 units::angle::radian_t{1.57},
                                                                                                 units::illuminance::lux_t{0.009}}}};

  const auto resolved_scenario_file_path =
      std::filesystem::path{test_info_->file()}
          .parent_path()  // Removes filename
          .parent_path()  // Removes OscToMantle folder
          .parent_path()  // Removes Conversion folder
          .string() +
      "/data/Scenarios/OSC_1_2_test_EnvironmentCatalog.xosc";

  LoadScenario(resolved_scenario_file_path);

  if (!(scenario_ptr_ && scenario_ptr_->GetOpenScenarioCategory() &&
        scenario_ptr_->GetOpenScenarioCategory()->GetScenarioDefinition()))
  {
    throw std::runtime_error("Scenario file not found or file does not contain scenario definition.");
  }

  const auto global_actions = scenario_ptr_->GetOpenScenarioCategory()
                                  ->GetScenarioDefinition()
                                  ->GetStoryboard()
                                  ->GetInit()
                                  ->GetActions()
                                  ->GetGlobalActions();

  const auto global_action_it = std::find_if(global_actions.begin(),
                                             global_actions.end(),
                                             [](const auto& global_action) {
                                               return global_action->GetEnvironmentAction()
                                                          ->GetCatalogReference()
                                                          ->GetCatalogName() == "EnvironmentCatalog";
                                             });

  ASSERT_NE((*global_action_it)->GetEnvironmentAction(), nullptr);
  const auto catalog_reference = (*global_action_it)->GetEnvironmentAction()->GetCatalogReference();

  ASSERT_NE(catalog_reference, nullptr);

  const auto environment = OpenScenarioEngine::v1_3::ConvertScenarioEnvironment(nullptr, catalog_reference);

  EXPECT_EQ(environment.date_time.value(), expected_environment.date_time.value());
  EXPECT_EQ(environment.weather.value().atmospheric_pressure, expected_environment.weather.value().atmospheric_pressure);
  EXPECT_EQ(environment.weather.value().temperature, expected_environment.weather.value().temperature);
  EXPECT_EQ(environment.weather.value().fog, expected_environment.weather.value().fog);
  EXPECT_EQ(environment.weather.value().illumination, expected_environment.weather.value().illumination);
  EXPECT_EQ(environment.weather.value().precipitation, expected_environment.weather.value().precipitation);
  EXPECT_FLOAT_EQ(environment.weather.value().sun.azimuth.value(), expected_environment.weather.value().sun.azimuth.value());
  EXPECT_EQ(environment.weather.value().sun.elevation, expected_environment.weather.value().sun.elevation);
  EXPECT_EQ(environment.weather.value().sun.intensity, expected_environment.weather.value().sun.intensity);
}
