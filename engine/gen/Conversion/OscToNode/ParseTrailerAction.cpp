/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToNode/ParseTrailerAction.h"

#include <memory>

#include "Conversion/OscToNode/ParseConnectTrailerAction.h"
#include "Conversion/OscToNode/ParseDisconnectTrailerAction.h"

namespace OpenScenarioEngine::v1_3
{
yase::BehaviorNode::Ptr parse(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITrailerAction> trailerAction)
{
  if (auto element = trailerAction->GetConnectTrailerAction(); element)
  {
    return parse(element);
  }
  if (auto element = trailerAction->GetDisconnectTrailerAction(); element)
  {
    return parse(element);
  }
  throw std::runtime_error("Corrupted openSCENARIO file: No choice made within std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITrailerAction>");
}

}  // namespace OpenScenarioEngine::v1_3
