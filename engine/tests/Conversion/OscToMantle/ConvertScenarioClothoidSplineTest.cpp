/********************************************************************************
 * Copyright (c) 2025 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Common/clothoid_spline.h>
#include <MantleAPI/Common/pose.h>
#include <gtest/gtest.h>
#include <openScenarioLib/generated/v1_3/api/writer/ApiClassWriterInterfacesV1_3.h>
#include <openScenarioLib/generated/v1_3/impl/ApiClassImplV1_3.h>

#include <functional>
#include <memory>
#include <optional>

#include "Conversion/OscToMantle/ConvertScenarioClothoidSpline.h"
#include "TestUtils/ClothoidSplineUtils.h"

namespace
{

using units::literals::operator""_m;
using units::literals::operator""_rad;
using units::literals::operator""_curv;

using ClothoidSplineSegmentWriter = NET_ASAM_OPENSCENARIO::v1_3::IClothoidSplineSegmentWriter;
using ClothoidSplineSegmentWriterSharedPtr = std::shared_ptr<ClothoidSplineSegmentWriter>;
class ConvertClothoidSplineTestFixture : public ::testing::Test
{
protected:
  ConvertClothoidSplineTestFixture()
  {
    expected_converted_clothoid_spline_.segments.push_back({-6.8_curv,
                                                            9.4_curv,
                                                            2.3_rad,
                                                            1.5_m,
                                                            mantle_api::Pose{{6.1_m, -1.5_m, 3.9_m}, {1.4_rad, 2.0_rad, -1.4_rad}}});

    clothoid_spline_ = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::ClothoidSplineImpl>();
  }
  void SetUpClothoid()
  {
    std::vector<ClothoidSplineSegmentWriterSharedPtr> segments;

    for (auto& segment : expected_converted_clothoid_spline_.segments)
    {
      segments.push_back(testing::OpenScenarioEngine::v1_3::CreateClothoidSegment(segment));
    }

    clothoid_spline_->SetSegments(segments);
  }

  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IClothoidSplineWriter> clothoid_spline_;
  mantle_api::ClothoidSpline expected_converted_clothoid_spline_;
};

TEST_F(ConvertClothoidSplineTestFixture, GivenNotInitializedClothoidSpline_ThenException)
{
  clothoid_spline_.reset();
  EXPECT_THROW(OpenScenarioEngine::v1_3::ConvertClothoidSpline(clothoid_spline_), std::invalid_argument);
}

TEST_F(ConvertClothoidSplineTestFixture, GivenNotInitializedClothoidSplineSegment_ThenException)
{
  ClothoidSplineSegmentWriterSharedPtr segment;
  std::vector<ClothoidSplineSegmentWriterSharedPtr> segments;

  segments.push_back(segment);
  clothoid_spline_->SetSegments(segments);

  EXPECT_THROW(OpenScenarioEngine::v1_3::ConvertClothoidSpline(clothoid_spline_), std::invalid_argument);
}

TEST_F(ConvertClothoidSplineTestFixture, GivenClothoidSplineSegment_WhenWorldPositionNotSet_ThenException)
{
  auto osc_position_start = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::PositionImpl>();
  auto geo_position = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::GeoPositionImpl>();
  osc_position_start->SetGeoPosition(geo_position);

  SetUpClothoid();

  ClothoidSplineSegmentWriterSharedPtr segment = std::dynamic_pointer_cast<ClothoidSplineSegmentWriter>(clothoid_spline_->GetSegmentsAtIndex(0));
  segment->SetPositionStart(osc_position_start);

  EXPECT_THROW(OpenScenarioEngine::v1_3::ConvertClothoidSpline(clothoid_spline_), std::runtime_error);
}

using ResetParam = std::function<void(ClothoidSplineSegmentWriterSharedPtr&, mantle_api::ClothoidSplineSegment&)>;

void ResetHOffset(ClothoidSplineSegmentWriterSharedPtr& segment, mantle_api::ClothoidSplineSegment& expected_converted_clothoid_spline_segment)
{
  expected_converted_clothoid_spline_segment.h_offset = 0.0_rad;
  segment->ResetHOffset();
}

void ResetPoseZ(ClothoidSplineSegmentWriterSharedPtr& segment, mantle_api::ClothoidSplineSegment& expected_converted_clothoid_spline_segment)
{
  expected_converted_clothoid_spline_segment.position_start->position.z = 0.0_m;
  std::dynamic_pointer_cast<NET_ASAM_OPENSCENARIO::v1_3::IWorldPositionWriter>(segment->GetPositionStart()->GetWorldPosition())
      ->ResetZ();
}

void ResetPoseH(ClothoidSplineSegmentWriterSharedPtr& segment, mantle_api::ClothoidSplineSegment& expected_converted_clothoid_spline_segment)
{
  expected_converted_clothoid_spline_segment.position_start->orientation.yaw = 0.0_rad;
  std::dynamic_pointer_cast<NET_ASAM_OPENSCENARIO::v1_3::IWorldPositionWriter>(segment->GetPositionStart()->GetWorldPosition())
      ->ResetH();
}

void ResetPoseP(ClothoidSplineSegmentWriterSharedPtr& segment, mantle_api::ClothoidSplineSegment& expected_converted_clothoid_spline_segment)
{
  expected_converted_clothoid_spline_segment.position_start->orientation.pitch = 0.0_rad;
  std::dynamic_pointer_cast<NET_ASAM_OPENSCENARIO::v1_3::IWorldPositionWriter>(segment->GetPositionStart()->GetWorldPosition())
      ->ResetP();
}

void ResetPoseR(ClothoidSplineSegmentWriterSharedPtr& segment, mantle_api::ClothoidSplineSegment& expected_converted_clothoid_spline_segment)
{
  expected_converted_clothoid_spline_segment.position_start->orientation.roll = 0.0_rad;
  std::dynamic_pointer_cast<NET_ASAM_OPENSCENARIO::v1_3::IWorldPositionWriter>(segment->GetPositionStart()->GetWorldPosition())
      ->ResetR();
}
class ConvertClothoidSplineTestParamFixture : public ::testing::WithParamInterface<ResetParam>, public ConvertClothoidSplineTestFixture
{
};

INSTANTIATE_TEST_SUITE_P(ConvertClothoidSplineTestInstParamFixture, ConvertClothoidSplineTestParamFixture, ::testing::Values(ResetHOffset, ResetPoseZ, ResetPoseH, ResetPoseP, ResetPoseR));

TEST_P(ConvertClothoidSplineTestParamFixture, GivenClothoidSplineSegment_WhenNotRequiredParametersNotSet_ThenConvertedClothoidSplineSegmentHasNotRequiredParametersEqualToZero)
{
  SetUpClothoid();

  ClothoidSplineSegmentWriterSharedPtr segment = std::dynamic_pointer_cast<ClothoidSplineSegmentWriter>(clothoid_spline_->GetSegmentsAtIndex(0));

  GetParam()(segment, expected_converted_clothoid_spline_.segments.front());

  mantle_api::ClothoidSpline actual_converted_clothoid_spline = OpenScenarioEngine::v1_3::ConvertClothoidSpline(clothoid_spline_);

  EXPECT_EQ(expected_converted_clothoid_spline_.segments, actual_converted_clothoid_spline.segments);
}

TEST_F(ConvertClothoidSplineTestFixture, GivenClothoidSpline_ThenConvertedClotoidSplineHasEqualParameters)
{
  expected_converted_clothoid_spline_.segments.push_back({2.0_curv,
                                                          1.5_curv,
                                                          1.5_rad,
                                                          1.8_m,
                                                          std::nullopt});
  SetUpClothoid();

  mantle_api::ClothoidSpline actual_converted_clothoid_spline = OpenScenarioEngine::v1_3::ConvertClothoidSpline(clothoid_spline_);

  EXPECT_EQ(expected_converted_clothoid_spline_, actual_converted_clothoid_spline);
}

}  // namespace
