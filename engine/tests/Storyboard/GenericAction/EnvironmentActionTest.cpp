/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gmock/gmock-matchers.h>
#include <gtest/gtest.h>

#include "Storyboard/GenericAction/EnvironmentAction_impl.h"

TEST(EnvironmentActionTest, GivenEnvironment_WhenSteppingEnvironmentAction_ThenExpectMantleEnvironmentSetterCalled)
{
  OpenScenarioEngine::v1_3::Environment environment = {units::time::second_t{}, mantle_api::Weather{}};

  auto mockEnvironment = std::make_shared<mantle_api::MockEnvironment>();

  EXPECT_CALL(*mockEnvironment,
              SetDateTime(testing::_)).Times(1);

  EXPECT_CALL(*mockEnvironment,
              SetWeather(testing::_)).Times(1);

  OpenScenarioEngine::v1_3::EnvironmentAction environment_action{{environment},
                                                                 {mockEnvironment}};
  environment_action.Step();
}

TEST(EnvironmentActionTest, GivenNullEnvironment_WhenSteppingEnvironmentAction_ThenExpectMantleEnvironmentSetterNotCalled)
{
  auto mockEnvironment = std::make_shared<mantle_api::MockEnvironment>();

  EXPECT_CALL(*mockEnvironment,
              SetDateTime(testing::_)).Times(0);

  EXPECT_CALL(*mockEnvironment,
              SetWeather(testing::_)).Times(0);

  OpenScenarioEngine::v1_3::EnvironmentAction environment_action{{},
                                                                 {mockEnvironment}};
  environment_action.Step();
}

