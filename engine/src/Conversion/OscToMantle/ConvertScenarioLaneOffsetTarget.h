
/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <openScenarioLib/generated/v1_3/api/ApiClassInterfacesV1_3.h>
#include <units.h>

#include <memory>
#include <string>

#include "Utils/Logger.h"

namespace OpenScenarioEngine::v1_3
{
using LaneOffsetTarget = units::length::meter_t;

inline LaneOffsetTarget ConvertScenarioLaneOffsetTarget(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ILaneOffsetTarget>& laneOffsetTarget)
{
  if (laneOffsetTarget->GetAbsoluteTargetLaneOffset())
  {
    return units::length::meter_t(laneOffsetTarget->GetAbsoluteTargetLaneOffset()->GetValue());
  }
  else
  {
    Logger::Error("RelativeTargetLaneOffset is not yet implemented (returning \"0\" offset by default)");
    return {};
  }
  /// TODO: RelativeTargetLaneOffset case needs to be implemented as it is currently not supported
}

}  // namespace OpenScenarioEngine::v1_3
