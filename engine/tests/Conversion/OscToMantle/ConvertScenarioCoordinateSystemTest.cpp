/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gtest/gtest.h>

#include "Conversion/OscToMantle/ConvertScenarioCoordinateSystem.h"

TEST(CoordinateSystemTest, GivenUnkownCoordinates_WhenConvert_ThenConvertedToUnknown)
{
  auto coordinateSystem = OpenScenarioEngine::v1_3::ConvertScenarioCoordinateSystem(NET_ASAM_OPENSCENARIO::v1_3::CoordinateSystem::CoordinateSystemEnum::UNKNOWN);

  ASSERT_EQ(OpenScenarioEngine::v1_3::CoordinateSystem::kUnknown, coordinateSystem);
}

TEST(CoordinateSystemTest, GivenEntityCoordinates_WhenConvert_ThenConvertedToEntity)
{
  auto coordinateSystem = OpenScenarioEngine::v1_3::ConvertScenarioCoordinateSystem(NET_ASAM_OPENSCENARIO::v1_3::CoordinateSystem::CoordinateSystemEnum::ENTITY);

  ASSERT_EQ(OpenScenarioEngine::v1_3::CoordinateSystem::kEntity, coordinateSystem);
}

TEST(CoordinateSystemTest, GivenLaneCoordinates_WhenConvert_ThenConvertedToLane)
{
  auto coordinateSystem = OpenScenarioEngine::v1_3::ConvertScenarioCoordinateSystem(NET_ASAM_OPENSCENARIO::v1_3::CoordinateSystem::CoordinateSystemEnum::LANE);

  ASSERT_EQ(OpenScenarioEngine::v1_3::CoordinateSystem::kLane, coordinateSystem);
}

TEST(CoordinateSystemTest, GivenRoadCoordinates_WhenConvert_ThenConvertedToRoad)
{
  auto coordinateSystem = OpenScenarioEngine::v1_3::ConvertScenarioCoordinateSystem(NET_ASAM_OPENSCENARIO::v1_3::CoordinateSystem::CoordinateSystemEnum::ROAD);

  ASSERT_EQ(OpenScenarioEngine::v1_3::CoordinateSystem::kRoad, coordinateSystem);
}

TEST(CoordinateSystemTest, GivenTrajectoryCoordinates_WhenConvert_ThenConvertedToTrajectory)
{
  auto coordinateSystem = OpenScenarioEngine::v1_3::ConvertScenarioCoordinateSystem(NET_ASAM_OPENSCENARIO::v1_3::CoordinateSystem::CoordinateSystemEnum::TRAJECTORY);

  ASSERT_EQ(OpenScenarioEngine::v1_3::CoordinateSystem::kTrajectory, coordinateSystem);
}

TEST(CoordinateSystemTest, GivenWorldCoordinates_WhenConvert_ThenConvertedToWorld)
{
  auto coordinateSystem = OpenScenarioEngine::v1_3::ConvertScenarioCoordinateSystem(NET_ASAM_OPENSCENARIO::v1_3::CoordinateSystem::CoordinateSystemEnum::WORLD);

  ASSERT_EQ(OpenScenarioEngine::v1_3::CoordinateSystem::kWorld, coordinateSystem);
}
