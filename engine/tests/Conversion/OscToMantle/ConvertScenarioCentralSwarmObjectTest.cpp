/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gtest/gtest.h>
#include <openScenarioLib/generated/v1_3/impl/ApiClassImplV1_3.h>
#include "Conversion/OscToMantle/ConvertScenarioCentralSwarmObject.h"
#include <openScenarioLib/src/common/INamedReference.h>

namespace detail
{
const std::string kEntityName{"entity"};

class NamedEntity : public NET_ASAM_OPENSCENARIO::INamedReference<NET_ASAM_OPENSCENARIO::v1_3::IEntity>
{
    public:
        std::string GetNameRef() override
        {
            return kEntityName;
        }
};

} // namespace detail

TEST(ConvertScenarioCentralSwarmObject, GivenNamedCentralSwarmObject_ThenConvertIntoEntityRefString)
{
    auto entity{std::make_shared<detail::NamedEntity>()};

    auto central_swarm_object{std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::CentralSwarmObjectImpl>()};
    central_swarm_object->SetEntityRef(entity);

    auto converted{OpenScenarioEngine::v1_3::ConvertScenarioCentralSwarmObject(central_swarm_object)};

    ASSERT_EQ(detail::kEntityName, converted);
}
