/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gtest/gtest.h>

#include "Conversion/OscToMantle/ConvertScenarioLateralDisplacement.h"

TEST(LateralDisplacementTest,
     GivenOpenScenarioLateralDisplacementAny_WhenConvertScenarioLateralDisplacement_ThenMantleApiLateralDisplacementDirectionAnyReturned)
{
  auto direction = OpenScenarioEngine::v1_3::ConvertScenarioLateralDisplacement(NET_ASAM_OPENSCENARIO::v1_3::LateralDisplacement::ANY);
  ASSERT_EQ(direction, mantle_api::LateralDisplacementDirection::kAny);
}

TEST(LateralDisplacementTest,
     GivenOpenScenarioLateralDisplacementLeft_WhenConvertScenarioLateralDisplacement_ThenMantleApiLateralDisplacementDirectionLeftReturned)
{
  auto direction = OpenScenarioEngine::v1_3::ConvertScenarioLateralDisplacement(NET_ASAM_OPENSCENARIO::v1_3::LateralDisplacement::LEFT_TO_REFERENCED_ENTITY);
  ASSERT_EQ(direction, mantle_api::LateralDisplacementDirection::kLeft);
}

TEST(LateralDisplacementTest,
     GivenOpenScenarioLateralDisplacementRight_WhenConvertScenarioLateralDisplacement_ThenMantleApiLateralDisplacementDirectionRightReturned)
{
  auto direction = OpenScenarioEngine::v1_3::ConvertScenarioLateralDisplacement(NET_ASAM_OPENSCENARIO::v1_3::LateralDisplacement::RIGHT_TO_REFERENCED_ENTITY);
  ASSERT_EQ(direction, mantle_api::LateralDisplacementDirection::kRight);
}

TEST(LateralDisplacementTest,
     GivenOpenScenarioLateralDisplacementUnknown_WhenConvertScenarioLateralDisplacement_ThenExceptionThrown)
{
  EXPECT_THROW(OpenScenarioEngine::v1_3::ConvertScenarioLateralDisplacement(NET_ASAM_OPENSCENARIO::v1_3::LateralDisplacement::UNKNOWN), std::runtime_error);
}