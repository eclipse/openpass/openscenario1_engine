/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/action_node.h>
#include <openScenarioLib/generated/v1_3/api/ApiClassInterfacesV1_3.h>

#include <cassert>
#include <utility>

#include "Storyboard/GenericAction/LightStateAction_impl.h"
#include "Utils/EntityBroker.h"

namespace OpenScenarioEngine::v1_3::Node
{
class LightStateAction : public yase::ActionNode
{
public:
  LightStateAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ILightStateAction> lightStateAction)
      : yase::ActionNode{"LightStateAction"},
        lightStateAction_{lightStateAction}
  {
  }

  void onInit() override{};

private:
  yase::NodeStatus tick() override
  {
    assert(impl_);
    const auto is_finished = impl_->Step();
    return is_finished ? yase::NodeStatus::kSuccess : yase::NodeStatus::kRunning;
  };

  void lookupAndRegisterData(yase::Blackboard& blackboard) final
  {
    std::shared_ptr<mantle_api::IEnvironment> environment = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");
    const EntityBroker::Ptr entityBroker = blackboard.get<EntityBroker::Ptr>("EntityBroker");

    impl_ = std::make_unique<OpenScenarioEngine::v1_3::LightStateAction>(
        OpenScenarioEngine::v1_3::LightStateAction::Values{
            entityBroker->GetEntities(),
            lightStateAction_->GetTransitionTime(),
            ConvertScenarioLightType(lightStateAction_->GetLightType()),
            ConvertScenarioLightState(lightStateAction_->GetLightState())},
        OpenScenarioEngine::v1_3::LightStateAction::Interfaces{
            environment});
  }

  std::unique_ptr<OpenScenarioEngine::v1_3::LightStateAction> impl_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ILightStateAction> lightStateAction_;
};

}  // namespace OpenScenarioEngine::v1_3::Node
