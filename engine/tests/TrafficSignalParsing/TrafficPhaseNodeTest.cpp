/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <agnostic_behavior_tree/composite/parallel_node.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <units.h>

#include "Conversion/OscToNode/ParseTrafficSignals.h"
#include "Node/RepeatingSequenceNode.h"
#include "Node/TrafficSignalPhaseNode.h"
#include "Utils/TrafficSignalBuilder.h"

using testing::_;
using testing::Return;

using namespace units::literals;
using namespace std::string_literals;
using namespace OpenScenarioEngine::v1_3;

class TestingRoot : public yase::ParallelNode
{
public:
  TestingRoot(
      std::shared_ptr<mantle_api::IEnvironment> environment)
      : environment_{environment} {}

private:
  void lookupAndRegisterData(yase::Blackboard& blackboard) override
  {
    blackboard.set("Environment", environment_);
  }

  std::shared_ptr<mantle_api::IEnvironment> environment_;
};

std::shared_ptr<Node::TrafficSignalPhaseNode> CREATE_PHASE_WITHIN_10_SEC_PERIOD(
    mantle_api::Time trigger_time,
    mantle_api::Time duration)
{
  return std::make_shared<Node::TrafficSignalPhaseNode>(
      "TEST_PHASE",
      10_s,
      trigger_time,
      duration,
      std::vector<std::pair<std::string, std::string>>{{"TEST_SIGNAL_NAME"s, "TEST_SIGNAL_STATE"s}});
}

TEST(TrafficSignalPhaseNode, ExecutedFirstWithinPeriod_TriggersAction)
{
  auto environment_ = std::make_shared<mantle_api::MockEnvironment>();
  TestingRoot root(environment_);
  ON_CALL(*environment_, GetSimulationTime()).WillByDefault(Return(1_s));
  EXPECT_CALL(*environment_, SetTrafficSignalState(_, _));

  auto traffic_signal_phase = CREATE_PHASE_WITHIN_10_SEC_PERIOD(1_s, 2_s);
  root.addChild(traffic_signal_phase);
  root.distributeData();
  root.executeTick();
}

TEST(TrafficSignalPhaseNode, ExecutedTwiceWithinPeriod_TriggersNoSecondAction)
{
  auto environment_ = std::make_shared<mantle_api::MockEnvironment>();
  TestingRoot root(environment_);
  EXPECT_CALL(*environment_, GetSimulationTime()).WillOnce(Return(1_s));
  EXPECT_CALL(*environment_, SetTrafficSignalState(_, _)).Times(1);

  auto traffic_signal_phase = CREATE_PHASE_WITHIN_10_SEC_PERIOD(1_s, 2_s);
  root.addChild(traffic_signal_phase);
  root.distributeData();
  root.executeTick();
  root.executeTick();
}

TEST(TrafficSignalPhaseNode, ExecuteAtZero_TriggersAction)
{
  auto environment_ = std::make_shared<mantle_api::MockEnvironment>();
  TestingRoot root(environment_);
  auto traffic_signal_phase = CREATE_PHASE_WITHIN_10_SEC_PERIOD(0_s, 2_s);
  root.addChild(traffic_signal_phase);
  root.distributeData();

  ON_CALL(*environment_, GetSimulationTime()).WillByDefault(Return(0_s));
  EXPECT_CALL(*environment_, SetTrafficSignalState(_, _)).Times(1);
  root.executeTick();
}

TEST(TrafficSignalPhaseNode, ExecutedOutsideOfPeriod_ReportsRunning)
{
  auto environment_ = std::make_shared<mantle_api::MockEnvironment>();
  TestingRoot root(environment_);
  auto traffic_signal_phase = CREATE_PHASE_WITHIN_10_SEC_PERIOD(1_s, 2_s);
  root.addChild(traffic_signal_phase);
  root.distributeData();

  ON_CALL(*environment_, GetSimulationTime()).WillByDefault(Return(0_s));
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);
}

TEST(TrafficSignalPhaseNode, ExecutedWithinPeriod_ReportsSuccess)
{
  auto environment_ = std::make_shared<mantle_api::MockEnvironment>();
  TestingRoot root(environment_);
  auto traffic_signal_phase = CREATE_PHASE_WITHIN_10_SEC_PERIOD(1_s, 2_s);
  root.addChild(traffic_signal_phase);
  root.distributeData();

  ON_CALL(*environment_, GetSimulationTime()).WillByDefault(Return(1_s));
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kSuccess);
}

///////////////////////////////////////////////////////////////////////////////

TEST(TrafficSignalBuilder, GivenControllerWithThreePhases_CreatesSequenceWithThreeElements)
{
  TrafficSignalBuilder builder;
  builder.Add({"Controller",
               {{"green", 10_s, {{"lamp0"s, "on"s}, {"lamp1"s, "off"s}, {"lamp2"s, "off"s}}},
                {"yellow", 10_s, {{"lamp0"s, "off"s}, {"lamp1"s, "on"s}, {"lamp2"s, "off"s}}},
                {"red", 10_s, {{"lamp0"s, "off"s}, {"lamp1"s, "off"s}, {"lamp2"s, "on"s}}}},
               std::nullopt});
  auto node = builder.Create();

  auto& traffic_signals = dynamic_cast<yase::ParallelNode&>(node->child());
  ASSERT_THAT(traffic_signals.childrenCount(), 1);

  auto& controller_node = dynamic_cast<Node::RepeatingSequenceNode&>(traffic_signals.child(0u));
  ASSERT_THAT(controller_node.childrenCount(), 3);
}

TEST(TrafficSignalBuilder, GivenTwoIndependentControllers_CreatesTwoSequnces)
{
  TrafficSignalBuilder builder;
  builder.Add({"Controller1", {}, std::nullopt});
  builder.Add({"Controller2", {}, std::nullopt});
  auto node = builder.Create();

  auto& traffic_signals = dynamic_cast<yase::ParallelNode&>(node->child());
  ASSERT_THAT(traffic_signals.childrenCount(), 2);

  EXPECT_NO_THROW([[maybe_unused]] auto& _0 = dynamic_cast<Node::RepeatingSequenceNode&>(traffic_signals.child(0u)));
  EXPECT_NO_THROW([[maybe_unused]] auto& _1 = dynamic_cast<Node::RepeatingSequenceNode&>(traffic_signals.child(1u)));
}
