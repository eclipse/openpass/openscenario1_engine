/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>

#include "Conversion/OscToMantle/ConvertScenarioCoordinateSystem.h"
#include "Conversion/OscToMantle/ConvertScenarioDynamicConstraints.h"
#include "Conversion/OscToMantle/ConvertScenarioEntity.h"
#include "Conversion/OscToMantle/ConvertScenarioLateralDisplacement.h"
#include "Utils/EntityBroker.h"

namespace OpenScenarioEngine::v1_3
{
class LateralDistanceActionBase
{
public:
  struct Values
  {
    Entities entities;
    bool continuous;
    double distance;
    bool freespace;
    mantle_api::LateralDisplacementDirection lateralDisplacement;
    DynamicConstraints dynamicConstraints;
    Entity entityRef;
    CoordinateSystem coordinateSystem;
  };
  struct Interfaces
  {
    std::shared_ptr<mantle_api::IEnvironment> environment;
  };

  LateralDistanceActionBase(Values values, Interfaces interfaces)
      : values_{std::move(values)},
        mantle_{std::move(interfaces)} {};

  virtual ~LateralDistanceActionBase() = default;
  virtual void SetControlStrategy() = 0;
  [[nodiscard]] virtual bool HasControlStrategyGoalBeenReached(const std::string& actor) = 0;
  [[nodiscard]] virtual mantle_api::MovementDomain GetMovementDomain() const = 0;

protected:
  Values values_;
  Interfaces mantle_;
};

}  // namespace OpenScenarioEngine::v1_3
