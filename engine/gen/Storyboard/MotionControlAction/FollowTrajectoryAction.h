/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/action_node.h>

#include <cassert>
#include <utility>

#include "Storyboard/MotionControlAction/FollowTrajectoryAction_impl.h"
#include "Storyboard/MotionControlAction/MotionControlAction.h"
#include "Utils/EntityBroker.h"

namespace OpenScenarioEngine::v1_3::Node
{
class FollowTrajectoryAction : public yase::ActionNode
{
public:
  FollowTrajectoryAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IFollowTrajectoryAction> followTrajectoryAction)
      : yase::ActionNode{"FollowTrajectoryAction"},
        followTrajectoryAction_{followTrajectoryAction}
  {
  }

  void onInit() override{};

private:
  yase::NodeStatus tick() override
  {
    assert(impl_);
    const auto is_finished = impl_->Step();
    return is_finished ? yase::NodeStatus::kSuccess : yase::NodeStatus::kRunning;
  };

  void lookupAndRegisterData(yase::Blackboard& blackboard) final
  {
    auto environment = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");
    const auto entityBroker = blackboard.get<EntityBroker::Ptr>("EntityBroker");

    impl_ = std::make_unique<OpenScenarioEngine::v1_3::MotionControlAction<OpenScenarioEngine::v1_3::FollowTrajectoryAction>>(
        OpenScenarioEngine::v1_3::FollowTrajectoryAction::Values{
            entityBroker->GetEntities(),
            followTrajectoryAction_->GetInitialDistanceOffset(),
            ConvertScenarioTrajectory(followTrajectoryAction_->GetTrajectory()),
            ConvertScenarioTimeReference(followTrajectoryAction_->GetTimeReference()),
            ConvertScenarioTrajectoryFollowingMode(followTrajectoryAction_->GetTrajectoryFollowingMode()),
            ConvertScenarioTrajectoryRef(environment, followTrajectoryAction_->GetTrajectoryRef(), followTrajectoryAction_->GetCatalogReference())},
        OpenScenarioEngine::v1_3::FollowTrajectoryAction::Interfaces{
            environment});
  }

  std::unique_ptr<OpenScenarioEngine::v1_3::MotionControlAction<OpenScenarioEngine::v1_3::FollowTrajectoryAction>> impl_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IFollowTrajectoryAction> followTrajectoryAction_;
};

}  // namespace OpenScenarioEngine::v1_3::Node
