/********************************************************************************
 * Copyright (c) 2025 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "TestUtils/ClothoidSplineUtils.h"

#include <MantleAPI/Common/pose.h>
#include <openScenarioLib/generated/v1_3/impl/ApiClassImplV1_3.h>

namespace testing::OpenScenarioEngine::v1_3::detail
{
// Create WorldPosition
std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IWorldPositionWriter> CreateWorldPosition(const mantle_api::Pose& world_position)
{
  auto position = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::WorldPositionImpl>();
  position->SetX(world_position.position.x.value());
  position->SetY(world_position.position.y.value());
  position->SetZ(world_position.position.z.value());
  position->SetH(world_position.orientation.yaw.value());
  position->SetP(world_position.orientation.pitch.value());
  position->SetR(world_position.orientation.roll.value());
  return position;
}
}  // namespace testing::OpenScenarioEngine::v1_3::detail

namespace testing::OpenScenarioEngine::v1_3
{

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IClothoidSplineSegmentWriter> CreateClothoidSegment(const mantle_api::ClothoidSplineSegment& clothoid_spline_segment)
{
  auto segment = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::ClothoidSplineSegmentImpl>();

  segment->SetCurvatureEnd(clothoid_spline_segment.curvature_end.value());
  segment->SetCurvatureStart(clothoid_spline_segment.curvature_start.value());
  segment->SetHOffset(clothoid_spline_segment.h_offset.value());
  segment->SetLength(clothoid_spline_segment.length.value());
  if (clothoid_spline_segment.position_start.has_value())
  {
    auto world_position = detail::CreateWorldPosition(clothoid_spline_segment.position_start.value());
    auto osc_position_start = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::PositionImpl>();
    osc_position_start->SetWorldPosition(world_position);
    segment->SetPositionStart(osc_position_start);
  }
  return segment;
}
}  // namespace testing::OpenScenarioEngine::v1_3
