/*******************************************************************************
 * Copyright (c) 2025 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioEventPriority.h"

#include "Utils/EventPriority.h"

namespace OpenScenarioEngine::v1_3
{
EventPriority ConvertScenarioEventPriority(const NET_ASAM_OPENSCENARIO::v1_3::Priority& priority)
{
  if (priority == NET_ASAM_OPENSCENARIO::v1_3::Priority::OVERRIDE)
  {
    return EventPriority::kOverride;
  }
  if (priority == NET_ASAM_OPENSCENARIO::v1_3::Priority::OVERWRITE)
  {
    return EventPriority::kOverwrite;
  }
  if (priority == NET_ASAM_OPENSCENARIO::v1_3::Priority::PARALLEL)
  {
    return EventPriority::kParallel;
  }
  if (priority == NET_ASAM_OPENSCENARIO::v1_3::Priority::SKIP)
  {
    return EventPriority::kSkip;
  }
  if (priority == NET_ASAM_OPENSCENARIO::v1_3::Priority::UNKNOWN)
  {
    return EventPriority::kUnknown;
  }

  throw std::runtime_error("ConvertScenarioEventPriority: Unsupported EventPriority");
}

}  // namespace OpenScenarioEngine::v1_3
