/*******************************************************************************
 * Copyright (c) 2025 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <openScenarioLib/generated/v1_3/api/ApiClassInterfacesV1_3.h>

namespace OpenScenarioEngine::v1_3
{
enum class EventPriority;

EventPriority ConvertScenarioEventPriority(const NET_ASAM_OPENSCENARIO::v1_3::Priority& priority);

}  // namespace OpenScenarioEngine::v1_3
