/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <agnostic_behavior_tree/composite_node.h>

namespace OpenScenarioEngine::v1_3::Node
{
class AllTriggeringEntityNode : public yase::CompositeNode
{
public:
  explicit AllTriggeringEntityNode(const std::string &name = "Unnamed")
      : AllTriggeringEntityNode(name, nullptr){};

  explicit AllTriggeringEntityNode(yase::Extension::UPtr extension_ptr)
      : AllTriggeringEntityNode("Unnamed", std::move(extension_ptr)){};

  AllTriggeringEntityNode(const std::string &name, yase::Extension::UPtr extension_ptr)
      : CompositeNode(std::string("Sequence::").append(name), std::move(extension_ptr)){};

  ~AllTriggeringEntityNode() override = default;

  void onInit() override
  {
    m_last_running_child = 0;
    this->child(m_last_running_child).onInit();
  }

private:
  yase::NodeStatus tick() final
  {
    // Loop over all children
    for (size_t index = m_last_running_child; index < childrenCount(); index++)
    {
      auto &child = this->child(index);
      const yase::NodeStatus child_status = child.executeTick();

      switch (child_status)
      {
        case yase::NodeStatus::kIdle:
        {
          throw std::runtime_error("Child was not initilized.");
        }
        case yase::NodeStatus::kRunning:
        {
          return yase::NodeStatus::kRunning;
        }
        case yase::NodeStatus::kFailure:
        {
          throw std::runtime_error("Conditions are not allowed to fail.");
        }
        case yase::NodeStatus::kSuccess:
        {
          // Terminate current child and init subsequent one
          child.onTerminate();
          m_last_running_child++;
          if (m_last_running_child < childrenCount())
          {
            this->child(m_last_running_child).onInit();
          }
          continue;
        }
      }

      throw std::invalid_argument("Undefined child_status.");
    }

    return yase::NodeStatus::kSuccess;
  }

  size_t m_last_running_child{0};
};

}  // namespace OpenScenarioEngine::v1_3::Node
