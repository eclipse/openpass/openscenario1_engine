/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Node/ByEntityConditionNode.h"

#include "Conversion/OscToMantle/ConvertScenarioEntity.h"
#include "Node/AllTriggeringEntityNode.h"
#include "Node/AnyTriggeringEntityNode.h"
#include "Node/EntityConditionNode.h"

namespace OpenScenarioEngine::v1_3::Node
{
namespace detail
{
[[nodiscard]] static yase::CompositeNode::Ptr SelectByRule(NET_ASAM_OPENSCENARIO::v1_3::TriggeringEntitiesRule rule)
{
  if (rule == NET_ASAM_OPENSCENARIO::v1_3::TriggeringEntitiesRule::ALL)
    return std::make_shared<AllTriggeringEntityNode>("TriggeringEntitiesRule=\"All\"");
  if (rule == NET_ASAM_OPENSCENARIO::v1_3::TriggeringEntitiesRule::ANY)
    return std::make_shared<AnyTriggeringEntityNode>("TriggeringEntitiesRule=\"Any\"");
  throw std::runtime_error("Invalid TriggeringEntitesRule");
}

}  // namespace detail

ByEntityConditionNode::ByEntityConditionNode(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IByEntityCondition> byEntityCondition)
    : DecoratorNode{"ByEntityConditionNode"}
{
  const auto triggeringEntities = byEntityCondition->GetTriggeringEntities();
  const auto entityCondition = byEntityCondition->GetEntityCondition();

  auto selector = detail::SelectByRule(triggeringEntities->GetTriggeringEntitiesRule());
  for (auto entityRef : triggeringEntities->GetEntityRefs())
  {
    selector->addChild(std::make_shared<EntityConditionNode>(entityCondition, ConvertScenarioEntity(entityRef)));
  }
  setChild(selector);
}

yase::NodeStatus ByEntityConditionNode::tick()
{
  return child().executeTick();
}

}  // namespace OpenScenarioEngine::v1_3::Node
