################################################################################
# Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

################################################################################
# Install file for building mantle api with Conan
################################################################################

from conan import ConanFile
from conan.tools.cmake import CMake, CMakeToolchain
from conan.tools.files import rm, copy
from conan.tools.scm import Git
import os

required_conan_version = ">=1.53"

class MantleAPIConan(ConanFile):
    name = "mantleapi"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False],
               "fPIC": [True, False],
               "MantleAPI_TEST":[True, False]}

    default_options = {"shared": False,
                       "fPIC": True,
                       "MantleAPI_TEST":False}

    exports_sources = "*"
    no_copy_source = False
    short_paths = True

    def generate(self):
        tc = CMakeToolchain(self)
        tc.cache_variables["MantleAPI_TEST"] = self.options.MantleAPI_TEST
        tc.generate()

    def _get_url_sha(self):
        if self.version in self.conan_data["sources"]:
            url = self.conan_data["sources"][self.version]["url"]
            sha256 = self.conan_data["sources"][self.version]["sha256"]
        else:
            url = self.conan_data["sources"]["default"]["url"]
            sha256 = self.version

        return url, sha256

    def source(self):
        url, sha256 = self._get_url_sha()
        git = Git(self)
        git.clone(url=url, target=self.name)
        git.folder=self.name
        git.checkout(commit=sha256)

    def build(self):
        cmake = CMake(self)
        cmake.configure(build_script_folder=self.name)
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()
        copy(self, pattern="*", src=os.path.join(self.name, "test/MantleAPI"), dst=os.path.join(self.package_folder, "test/MantleAPI"))
        rm(self, "conanbuildinfo.txt", self.package_folder)
        rm(self, "conandata.yml", self.package_folder)
