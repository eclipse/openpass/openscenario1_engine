/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *               2021 Max Paul Bauer - Robert Bosch GmbH
 *               2025 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "Node/TriggerableCompositeNode.h"
#include "Utils/EventPriority.h"
#include "Utils/IEventPrioritizer.h"

namespace OpenScenarioEngine::v1_3::Node
{

/// @brief Runs a list of actions when the start trigger is successful. It can be given a priority to interact with other nodes in the same manevuer.
class EventNode : public TriggerableCompositeNode
{
public:
  /// @brief Construct a new event node
  ///
  /// @param name Name of the event node
  /// @param actions Actions node to run
  /// @param start_trigger Start trigger for the node
  /// @param priority The priority of the event
  EventNode(const std::string& name,
            yase::BehaviorNode::Ptr actions,
            yase::BehaviorNode::Ptr start_trigger = nullptr,
            EventPriority priority = EventPriority::kUnknown);

  void lookupAndRegisterData(yase::Blackboard& blackboard) override;

  yase::NodeStatus tick() override;

private:
  EventPriority event_priority_;
  yase::BehaviorNode::Ptr start_trigger_;
  std::shared_ptr<IEventPrioritizer> event_prioritizer_;
};

}  // namespace OpenScenarioEngine::v1_3::Node
