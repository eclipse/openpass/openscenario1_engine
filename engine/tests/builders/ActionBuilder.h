/*******************************************************************************
 * Copyright (c) 2021-2024, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022-2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once
#include <openScenarioLib/generated/v1_3/impl/ApiClassImplV1_3.h>

#include <memory>

namespace testing::OpenScenarioEngine::v1_3
{

class FakeActionBuilder
{
  public:
    FakeActionBuilder();
    FakeActionBuilder& WithPrivateAction(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IPrivateActionWriter> private_action);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IActionWriter> Build();

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ActionImpl> action_{};
};

class FakePrivateActionBuilder
{
  public:
    FakePrivateActionBuilder();
    FakePrivateActionBuilder& WithTeleportAction(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITeleportActionWriter> teleport_action);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IPrivateActionWriter> Build();

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::PrivateActionImpl> private_action_{};
};

class FakeTeleportActionBuilder
{
  public:
    FakeTeleportActionBuilder();
    FakeTeleportActionBuilder& WithPosition(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IPositionWriter> position);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITeleportActionWriter> Build();

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::TeleportActionImpl> teleport_action_{};
};

class FakeLaneChangeActionBuilder
{
public:
    FakeLaneChangeActionBuilder();
    FakeLaneChangeActionBuilder& WithDynamics(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITransitionDynamicsWriter> transition_dynamics);
    FakeLaneChangeActionBuilder& WithLaneChangeTarget(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ILaneChangeTargetWriter> lane_change_action_dynamics);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ILaneChangeActionWriter> Build();

  private:
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::LaneChangeActionImpl> lane_change_action_{};
};

class FakeLaneChangeTargetBuilder
{
public:
    FakeLaneChangeTargetBuilder();
    FakeLaneChangeTargetBuilder& WithRelativeTargetLane(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IRelativeTargetLaneWriter> relative_target_lane);
    FakeLaneChangeTargetBuilder& WithAbsoluteTargetLane(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IAbsoluteTargetLaneWriter> absolute_target_lane);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ILaneChangeTargetWriter> Build();

  private:
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::LaneChangeTargetImpl> lane_change_target_{};
};

class FakeLightStateActionBuilder
{
public:
    FakeLightStateActionBuilder();
    FakeLightStateActionBuilder& WithLightType(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ILightTypeWriter> light_type);
    FakeLightStateActionBuilder& WithLightState(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ILightStateWriter> light_state);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ILightStateActionWriter> Build();

  private:
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::LightStateActionImpl> light_state_action_{};
};

class FakeRelativeTargetLaneBuilder
{
public:
    FakeRelativeTargetLaneBuilder();
    FakeRelativeTargetLaneBuilder& WithEntityRef(const std::string entity_name);
    FakeRelativeTargetLaneBuilder& WithValue(const int value);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IRelativeTargetLaneWriter> Build();

  private:
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::RelativeTargetLaneImpl> relative_target_lane_{};
};

class FakeAbsoluteTargetLaneBuilder
{
public:
    FakeAbsoluteTargetLaneBuilder();
    FakeAbsoluteTargetLaneBuilder& WithValue(const std::string value);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IAbsoluteTargetLaneWriter> Build();

  private:
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::AbsoluteTargetLaneImpl> absolute_target_lane_{};
};

class FakeSpeedActionBuilder
{
  public:
    FakeSpeedActionBuilder();
    FakeSpeedActionBuilder& WithSpeedActionTarget(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ISpeedActionTargetWriter> speed_action_target);
    FakeSpeedActionBuilder& WithTransitionDynamics(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITransitionDynamicsWriter> transition_dynamics);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ISpeedActionWriter> Build();

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::SpeedActionImpl> speed_action_{};
};

class FakeSpeedActionTargetBuilder
{
  public:
    FakeSpeedActionTargetBuilder();
    FakeSpeedActionTargetBuilder& WithAbsoluteTargetSpeed(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IAbsoluteTargetSpeedWriter> absolute_target_speed);
    FakeSpeedActionTargetBuilder& WithRelativeTargetSpeed(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IRelativeTargetSpeedWriter> relative_target_speed);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ISpeedActionTargetWriter> Build();

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::SpeedActionTargetImpl> speed_action_target_{};
};

class FakeAbsoluteTargetSpeedBuilder
{
  public:
    FakeAbsoluteTargetSpeedBuilder(double value);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IAbsoluteTargetSpeedWriter> Build();

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::AbsoluteTargetSpeedImpl> absolute_target_speed_{};
};

class FakeRelativeTargetSpeedBuilder
{
  public:
    FakeRelativeTargetSpeedBuilder(
        double value,
        std::string entity_name,
        const NET_ASAM_OPENSCENARIO::v1_3::SpeedTargetValueType::SpeedTargetValueTypeEnum speed_type);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IRelativeTargetSpeedWriter> Build();

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::RelativeTargetSpeedImpl> relative_target_speed_{};
};

class FakeTransitionDynamicsBuilder
{
  public:
    FakeTransitionDynamicsBuilder();
    FakeTransitionDynamicsBuilder(
        const NET_ASAM_OPENSCENARIO::v1_3::DynamicsShape::DynamicsShapeEnum dynamics_shape_enum,
        const NET_ASAM_OPENSCENARIO::v1_3::DynamicsDimension::DynamicsDimensionEnum dynamics_dimension_enum,
        double value);
    FakeTransitionDynamicsBuilder& WithDynamicsShape(NET_ASAM_OPENSCENARIO::v1_3::DynamicsShape dynamics_shape);
    FakeTransitionDynamicsBuilder& WithDynamicsDimension(
        NET_ASAM_OPENSCENARIO::v1_3::DynamicsDimension dynamics_dimension);
    FakeTransitionDynamicsBuilder& WithValue(double value);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITransitionDynamicsWriter> Build();

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::TransitionDynamicsImpl> transition_dynamics_{};
};

class FakeTrafficSignalStateActionBuilder
{
  public:
    FakeTrafficSignalStateActionBuilder();
    FakeTrafficSignalStateActionBuilder& WithState(const std::string& state);
    FakeTrafficSignalStateActionBuilder& WithName(const std::string& name);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITrafficSignalStateActionWriter> Build();

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::TrafficSignalStateActionImpl> traffic_signal_state_action_{};
};

class FakeFollowTrajectoryActionBuilder
{
  public:
    FakeFollowTrajectoryActionBuilder();
    FakeFollowTrajectoryActionBuilder& WithTimeReference(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITimeReferenceWriter> time_reference);
    FakeFollowTrajectoryActionBuilder& WithTrajectory(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITrajectoryWriter> trajectory);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IFollowTrajectoryActionWriter> Build();

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::FollowTrajectoryActionImpl> follow_trajectory_action_{};
};

class FakeAssignRouteActionBuilder
{
  public:
    FakeAssignRouteActionBuilder();

    FakeAssignRouteActionBuilder& WithRoute(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IRouteWriter> route_writer);

    FakeAssignRouteActionBuilder& WithCatalogReference(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ICatalogReferenceWriter> catalog_reference_writer);

    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IAssignRouteActionWriter> Build();

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::AssignRouteActionImpl> assign_route_action_impl_{};
};

class FakeTimeReferenceBuilder
{
  public:
    FakeTimeReferenceBuilder();
    FakeTimeReferenceBuilder& WithTiming(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITimingWriter> timing);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITimeReferenceWriter> Build();

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::TimeReferenceImpl> time_reference_{};
};

class FakeTrajectoryBuilder
{
  public:
    FakeTrajectoryBuilder();
    FakeTrajectoryBuilder(
      const std::string& name,
      bool closed);
    FakeTrajectoryBuilder& WithShape(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IShapeWriter> shape);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITrajectoryWriter> Build();

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::TrajectoryImpl> trajectory_{};
};

class FakeShapeBuilder
{
  public:
    FakeShapeBuilder();
    FakeShapeBuilder& WithPolyline(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IPolylineWriter> polyline);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IShapeWriter> Build();

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ShapeImpl> shape_{};
};

class FakePolylineBuilder
{
  public:
    FakePolylineBuilder();
    FakePolylineBuilder& WithVertices(
        std::vector<std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IVertexWriter>> vertices);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IPolylineWriter> Build();

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::PolylineImpl> polyline_{};
};

class FakeVertexBuilder
{
  public:
    FakeVertexBuilder();
    FakeVertexBuilder(
      double time);
    FakeVertexBuilder& WithPosition(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IPositionWriter> position);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IVertexWriter> Build();

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::VertexImpl> vertex_{};
};

class FakeLongitudinalDistanceActionBuilder
{
  public:
    FakeLongitudinalDistanceActionBuilder();
    FakeLongitudinalDistanceActionBuilder& WithDisplacement(
        const NET_ASAM_OPENSCENARIO::v1_3::LongitudinalDisplacement::LongitudinalDisplacement::
            LongitudinalDisplacementEnum displacement_target);
    FakeLongitudinalDistanceActionBuilder& WithContinuous(bool is_continuous);
    FakeLongitudinalDistanceActionBuilder& WithTimeGap(double time_gap_target);
    FakeLongitudinalDistanceActionBuilder& WithDynamicConstraints(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IDynamicConstraintsWriter> dynamic_constraint_writer);
    FakeLongitudinalDistanceActionBuilder& WithCoordinateSystem(
        const NET_ASAM_OPENSCENARIO::v1_3::CoordinateSystem::CoordinateSystem::CoordinateSystemEnum
            coordinate_system_target);
    FakeLongitudinalDistanceActionBuilder& WithDistance(double distance_target);
    FakeLongitudinalDistanceActionBuilder& WithFreeSpace(bool is_freespace);
    FakeLongitudinalDistanceActionBuilder& WithEntityRef(const std::string entity_name);

    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ILongitudinalDistanceActionWriter> Build();

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::LongitudinalDistanceActionImpl> longitudinal_distance_action_impl_{};
};

class FakeUserDefinedActionBuilder
{
  public:
    FakeUserDefinedActionBuilder();
    FakeUserDefinedActionBuilder& WithCustomCommandAction(
        std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ICustomCommandActionWriter> custom_command_action);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IUserDefinedActionWriter> Build();

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::UserDefinedActionImpl> user_defined_action_{};
};

class FakeCustomCommandActionBuilder
{
  public:
    FakeCustomCommandActionBuilder();
    FakeCustomCommandActionBuilder(const std::string& type, const std::string& command);
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ICustomCommandActionWriter> Build();

  private:
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::CustomCommandActionImpl> custom_command_action_{};
};

}  // namespace testing::OpenScenarioEngine::v1_3
