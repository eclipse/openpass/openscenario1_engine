/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <openScenarioLib/generated/v1_3/api/ApiClassInterfacesV1_3.h>

namespace OpenScenarioEngine::v1_3
{
class EntityCreator
{
public:
  explicit EntityCreator(std::shared_ptr<mantle_api::IEnvironment> environment);

  void CreateEntity(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IScenarioObject> scenario_object);

private:
  void CreateVehicle(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IVehicle> vehicle, const std::string& name);
  void CreatePedestrian(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IPedestrian> pedestrian,
                        const std::string& name);
  void CreateMiscObject(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IMiscObject> misc_object,
                        const std::string& name);
  void CreateCatalogReferenceEntity(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ICatalogReference> catalog_reference,
                                    const std::string& name);

  /// Fills the bounding box properties of an entity
  /// @param[in]  bounding_box The bounding box information to copy from
  /// @param[in]  name         The name of the entity for debug output
  /// @param[out] properties   The properties to copy to
  void FillBoundingBoxProperties(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IBoundingBox> bounding_box,
                                 const std::string& name,
                                 mantle_api::EntityProperties& properties);

  /// Fills the pedestrian properties of a pedestrian entity
  /// @param[in]  pedestrian The pedestrian object to copy from
  /// @param[out] properties The properties to copy to
  void FillEntityPropertiesForPedestrian(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IPedestrian> pedestrian,
                                         mantle_api::EntityProperties& properties);

  /// Fills the generic properties of an entity
  /// @param[in]  entity            The entity to copy from
  /// @param[out] entity_properties The entity properties to copy to
  template <class EntityType>
  void FillGenericProperties(const EntityType& entity, mantle_api::EntityProperties& entity_properties)
  {
    if (entity.GetProperties() == nullptr)
    {
      return;
    }
    for (const auto& property : entity.GetProperties()->GetProperties())
    {
      entity_properties.properties.emplace(property->GetName(), property->GetValue());
    }
  }

  /// Fills the axle properties of a vehicle entity
  /// @param[in]  open_scenario_axle The axle information to copy from
  /// @param[in]  bounding_box       The bounding box information to copy from
  /// @param[in]  name               The name of the entity for debug output
  /// @param[out] axle               The axle properties to copy to
  void FillAxleProperties(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IAxle> open_scenario_axle,
                          std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IBoundingBox> bounding_box,
                          const std::string& name,
                          mantle_api::Axle& axle);

  /// Fills the vertical offset property of a static object entity
  /// @param[in]  misc_object       The misc object to copy from
  /// @param[out] entity_properties The entity properties to copy to
  void SetVerticalOffset(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IMiscObject> misc_object,
                         mantle_api::StaticObjectProperties& entity_properties);

  mantle_api::VehicleClass GetVehicleClass(NET_ASAM_OPENSCENARIO::v1_3::VehicleCategory vehicle_category);
  mantle_api::StaticObjectType GetStaticObjectType(NET_ASAM_OPENSCENARIO::v1_3::MiscObjectCategory misc_object_category);
  mantle_api::EntityType GetEntityTypeFromPedestrianCategory(
      NET_ASAM_OPENSCENARIO::v1_3::PedestrianCategory pedestrian_category);

  std::shared_ptr<mantle_api::IEnvironment> environment_;
};

}  // namespace OpenScenarioEngine::v1_3
