/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/ByEntityCondition/RelativeSpeedCondition_impl.h"

#include "Utils/EntityUtils.h"

namespace OpenScenarioEngine::v1_3
{
bool RelativeSpeedCondition::IsSatisfied() const
{
  const auto& triggeringEntity = EntityUtils::GetEntityByName(mantle.environment, values.triggeringEntity);
  const auto triggeringVelocity = triggeringEntity.GetVelocity().Length();

  const auto& referencedEntity = EntityUtils::GetEntityByName(mantle.environment, values.entityRef);
  const auto referencedVelocity = referencedEntity.GetVelocity().Length();

  const auto relativeSpeed = triggeringVelocity - referencedVelocity;
  return values.rule.IsSatisfied(relativeSpeed.value());
}

}  // namespace OpenScenarioEngine::v1_3