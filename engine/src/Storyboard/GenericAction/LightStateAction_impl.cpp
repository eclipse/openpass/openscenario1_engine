/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/GenericAction/LightStateAction_impl.h"

#include "Utils/EntityUtils.h"
#include "Utils/Logger.h"

namespace OpenScenarioEngine::v1_3
{

bool LightStateAction::Step()
{
  SetControlStrategy();
  return true;
}

void LightStateAction::SetControlStrategy()
{
  control_strategy_->light_type = values.lightType;
  control_strategy_->light_state = values.lightState;
  for (const auto& actor : values.entities)
  {
    auto& entity = EntityUtils::GetEntityByName(mantle.environment, actor);
    mantle.environment->UpdateControlStrategies(
        entity.GetUniqueId(),
        {control_strategy_});
  }
}

}  // namespace OpenScenarioEngine::v1_3
