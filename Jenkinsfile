/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

pipeline {
  agent none
  parameters {
    booleanParam(name: 'BUILD_DOCKER_IMAGE', defaultValue: false, description: 'Force docker image (re-)build')
  }
  options {
    checkoutToSubdirectory('repo')
    timeout(time: 4, unit: 'HOURS')
  }
  environment {
    IMAGE_NAME = "eclipseopenpass/ose"
    REPO_URL = "https://gitlab.eclipse.org/eclipse/openpass/openscenario1_engine.git"
    DOCKERFILE_PATH = "utils/Dockerfile"
  }
  stages {
    stage('Build docker image or set image tag') {
      agent {
            kubernetes {
              inheritFrom 'openpass-agent-pod-' + env.BUILD_NUMBER
              yaml """
apiVersion: v1
kind: Pod
spec:
  containers:
  - name: ose-build
    image: eclipseopenpass/ubuntu:base
    tty: true
    resources:
      limits:
        memory: "16Gi"
        cpu: "4"
      requests:
        memory: "16Gi"
        cpu: "4"
  - name: jnlp
    volumeMounts:
    - name: volume-known-hosts
      mountPath: /home/jenkins/.ssh
  volumes:
  - name: volume-known-hosts
    configMap:
      name: known-hosts
"""
            }
          }
      stages {
        stage('Build docker image') {
          when {
            expression {
              return params.BUILD_DOCKER_IMAGE
            }
          }
          steps {
            script {
              env.IMAGE_TAG = "testing"
            }
            build job: 'Docker-build', parameters: [string(name: 'IMAGE_NAME', value: "${env.IMAGE_NAME}"), 
                                                    string(name: 'IMAGE_TAG', value:'testing'),
                                                    string(name: 'REPO_URL', value: "${env.REPO_URL}"), 
                                                    string(name: 'BRANCH_NAME', value: "${env.GIT_BRANCH}"), 
                                                    string(name: 'DOCKERFILE_PATH', value: "${env.DOCKERFILE_PATH}")],
                                       propagate: true
          }
        }
        stage('Set image tag') {
          when {
            expression {
              return !params.BUILD_DOCKER_IMAGE
            }
          }
          steps {
            script {
              env.IMAGE_TAG = "testing"
            }
          }
        }
      }
    }
    stage('Linux and Windows build') {
      parallel {
        stage('Linux') {
          agent {
            kubernetes {
              inheritFrom 'openscenarioengine_image-agent-pod-' + env.BUILD_NUMBER
              yaml """
apiVersion: v1
kind: Pod
spec:
  containers:
  - name: openscenarioengine-image
    image: "${env.IMAGE_NAME}:${env.IMAGE_TAG}"
    tty: true
    resources:
      limits:
        memory: "16Gi"
        cpu: "4"
      requests:
        memory: "16Gi"
        cpu: "4"
  - name: jnlp
    volumeMounts:
    - name: volume-known-hosts
      mountPath: /home/jenkins/.ssh
  volumes:
  - name: volume-known-hosts
    configMap:
      name: known-hosts
"""
            }
          }
          environment {
            CONAN_HOME = '/OSE/conan'
          }
          stages {
            stage('Linux: Prepare Dependencies') {
              steps {
                  container('openscenarioengine-image') {
                    sh 'bash repo/utils/ci/scripts/10_build_prepare.sh'
                    sh 'bash repo/utils/ci/scripts/15_prepare_thirdParty.sh'
                }
              }
            }
            stage('Linux: Configure build') {
              steps {
                  container('openscenarioengine-image') {
                    sh 'bash repo/utils/ci/scripts/20_configure.sh'
                }
              }
            }

            stage('Linux: Build and install project') {
              steps {
                  container('openscenarioengine-image') {
                    sh 'bash repo/utils/ci/scripts/30_build.sh'
                }
              }
            }
            stage('Linux: Build and run unit tests') {
              steps {
                  container('openscenarioengine-image') {
                    sh 'bash repo/utils/ci/scripts/40_unit_tests.sh'
                }
              }
            }
          }
        }
        stage('Windows') {
          agent {
            label 'windows10'
          }
          environment {
            MSYSTEM = 'MINGW64'
            CHERE_INVOKING = 'yes'
            PYTHON_WINDOWS_EXE = 'C:/op/python/python.exe'
          }
          stages {
            stage('Windows: Prepare dependencies') {
              steps {
                bat 'subst W: %WORKSPACE%'
                dir('W:/') {
                  bat 'C:\\op\\msys64\\usr\\bin\\bash -lc repo/utils/ci/scripts/10_build_prepare.sh'
                  bat 'C:\\op\\msys64\\usr\\bin\\bash -lc repo/utils/ci/scripts/15_prepare_thirdParty.sh'
                }
              }
            }
            stage('Windows: Configure build') {
              steps {
                dir('W:/') {
                  bat 'C:\\op\\msys64\\usr\\bin\\bash -lc repo/utils/ci/scripts/20_configure.sh'
                }
              }
            }
            stage('Windows: Build and install project') {
              steps {
                dir('W:/') {
                  bat 'C:\\op\\msys64\\usr\\bin\\bash -lc repo/utils/ci/scripts/30_build.sh'
                }
              }
            }
            stage('Windows: Build and run unit tests') {
              steps {
                dir('W:/') {
                  bat 'C:\\op\\msys64\\usr\\bin\\bash -lc repo/utils/ci/scripts/40_unit_tests.sh'
                }
              }
            }
          }
          post {
            always {
              bat 'subst W: /d'
            }
          }
        }
      }
    }
  }
}
