/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/GenericAction/AssignRouteAction_impl.h"

#include <stdexcept>

#include "Utils/EntityUtils.h"
#include "Utils/Logger.h"

namespace OpenScenarioEngine::v1_3
{
bool AssignRouteAction::Step()
{
  if (values.route.closed)
  {
    Logger::Error("AssignRouteAction: Closed routes not supported yet");
  }

  for (const auto& actor : values.entities)
  {
    const auto& entity = EntityUtils::GetEntityByName(mantle.environment, actor);
    mantle.environment->AssignRoute(entity.GetUniqueId(), {values.route.waypoints});
  }
  return true;
}

}  // namespace OpenScenarioEngine::v1_3
