################################################################################
# Copyright (c) 2021-2022 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

import jinja2
from jinja2.exceptions import TemplateNotFound
from pathlib import Path


class TemplateMachine:

    def __init__(self, searchpath: str = 'templates', suffix: str = 'j2') -> None:
        self.searchpath = str(searchpath)
        self.suffix = suffix

    def _load_template(self, template_name: str) -> jinja2.Template:
        loader = jinja2.FileSystemLoader(searchpath=self.searchpath)
        environment = jinja2.Environment(
            loader=loader,
            trim_blocks=True,
            lstrip_blocks=True,
            keep_trailing_newline=True)
        return environment.get_template(template_name)

    def create(self, name: str, context, output: Path = None, throw_on_fail=False):
        """
        Creates a file from the given template, associated by its name.

        Keyword arguments:
        name -- template name without suffix '.template' (e.g wrapper.vbs)
        context -- dict as context for the template.
        output - output path, where the file with name will be stored

        """
        output = Path(output if output else name)
        try:
            rendered_string = self._create(name, context)
            Path(output).parent.mkdir(parents=True, exist_ok=True)
            with open(output, 'w') as o:
                o.write(rendered_string)
        except TemplateNotFound as e:
            if throw_on_fail: raise e

    def _create(self, name: str, context):
        """
        Creates a string from the given template, associated by its name.

        Keyword arguments:
        name -- template name without suffix '.template' (e.g wrapper.vbs)
        context -- dict as context for the template.
        output - output path, where the file with name will be stored

        """
        template = self._load_template(f"{name}.{self.suffix}")
        return template.render(**context)
