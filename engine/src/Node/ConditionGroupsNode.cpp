/*******************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Node/ConditionGroupsNode.h"

#include <stdexcept>

namespace OpenScenarioEngine::v1_3::Node
{
using namespace yase;

ConditionGroupsNode::ConditionGroupsNode(const std::string &name)
    : CompositeNode(name) {}

void ConditionGroupsNode::onInit()
{
  for (auto &child : m_children_nodes)
  {
    child->onInit();
  }
}

NodeStatus ConditionGroupsNode::tick()
{
  for (auto &child : m_children_nodes)
  {
    const auto child_status = child->executeTick();
    if (child_status == NodeStatus::kFailure)
    {
      throw std::runtime_error("Conditions are not allowed to fail.");
    }
    if (child_status == NodeStatus::kSuccess)
    {
      return NodeStatus::kSuccess;
    }
  }

  return NodeStatus::kRunning;
}

}  // namespace OpenScenarioEngine::v1_3::Node