/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/MotionControlAction/LateralDistanceAction_impl.h"

#include <MantleAPI/Common/i_geometry_helper.h>
#include <MantleAPI/Common/vector.h>

#include "Utils/EntityUtils.h"
#include "Utils/Logger.h"

namespace OpenScenarioEngine::v1_3
{
bool LateralDistanceAction::IsLeftToCenterline(const mantle_api::Vec3<units::length::meter_t>& position_on_lane_centerline,
                                               const mantle_api::Vec3<units::length::meter_t>& laterally_shifted_position) const
{
  const auto lane_orientation = mantle_.environment->GetQueryService().GetLaneOrientation(position_on_lane_centerline);
  const auto position_transformed = mantle_.environment->GetGeometryHelper()->TransformPositionFromWorldToLocal(
      laterally_shifted_position, position_on_lane_centerline, lane_orientation);
  return position_transformed.y.value() > 0;
}

mantle_api::Vec3<units::length::meter_t> LateralDistanceAction::TranslateBoundingBoxCornerToCenter(
    const mantle_api::IEntity& entity,
    const mantle_api::Vec3<units::length::meter_t>& corner_position,
    LateralLocation lateral_location,
    LongitudinalLocation longitudinal_location) const
{
  const auto bounding_box = entity.GetProperties()->bounding_box;
  const auto longitudinal_shift = bounding_box.dimension.length * 0.5 *
                                  (longitudinal_location == LongitudinalLocation::kFront ? -1.0 : 1.0);
  const auto lateral_shift = bounding_box.dimension.width * 0.5 *
                             (lateral_location == LateralLocation::kLeft ? -1.0 : 1.0);
  const auto vertical_shift = bounding_box.dimension.height * 0.5;

  return mantle_.environment->GetGeometryHelper()->TranslateGlobalPositionLocally(
      corner_position,
      entity.GetOrientation(),
      {longitudinal_shift, lateral_shift, vertical_shift});
}

std::vector<LateralDistanceAction::BoundingBoxPoint> LateralDistanceAction::GetBoundingBoxCornerPoints(
    const mantle_api::IEntity& entity,
    LateralLocation lateral_location) const
{
  const auto bounding_box = entity.GetProperties()->bounding_box;
  const auto half_length = bounding_box.dimension.length * 0.5;
  const auto half_width = bounding_box.dimension.width * 0.5;
  const auto half_height = bounding_box.dimension.height * 0.5;

  if (lateral_location == LateralLocation::kLeft)
  {
    return {
        {
            {{half_length, half_width, -half_height}, LongitudinalLocation::kFront},  // front bottom left
            {{-half_length, half_width, -half_height}, LongitudinalLocation::kBack}   // back bottom left
        }};
  }
  else
  {
    return {
        {
            {{half_length, -half_width, -half_height}, LongitudinalLocation::kFront},  // front bottom right
            {{-half_length, -half_width, -half_height}, LongitudinalLocation::kBack}   // back bottom right
        }};
  }
}

LateralDistanceAction::BoundingBoxPoint LateralDistanceAction::GetBoundingBoxCornerPoint(
    const mantle_api::IEntity& entity,
    LateralLocation lateral_location) const
{
  auto local_to_global = [&](auto& corner_point)
  {
    corner_point.position = mantle_.environment->GetGeometryHelper()->TranslateGlobalPositionLocally(
        entity.GetPosition(),
        entity.GetOrientation(),
        corner_point.position);
  };

  auto global_to_lane = [&](auto& corner_point)
  {
    corner_point.position = mantle_.environment->GetGeometryHelper()->TransformPositionFromWorldToLocal(
        corner_point.position,
        entity.GetPosition(),
        mantle_.environment->GetQueryService().GetLaneOrientation(entity.GetPosition()));
  };

  auto corner_points = GetBoundingBoxCornerPoints(entity, lateral_location);
  std::for_each(begin(corner_points), end(corner_points), local_to_global);
  std::for_each(begin(corner_points), end(corner_points), global_to_lane);

  auto get_corner_point = [&]()
  {
    const auto oriented_similarly = mantle_.environment->GetGeometryHelper()->AreOrientedSimilarly(
        entity.GetOrientation(),
        mantle_.environment->GetQueryService().GetLaneOrientation(entity.GetPosition()));
    auto compare = [](const auto& a, const auto& b)
    {
      return a.position.y < b.position.y;
    };

    return lateral_location == LateralLocation::kLeft
               ? (oriented_similarly
                      ? std::max_element(corner_points.begin(), corner_points.end(), compare)
                      : std::min_element(corner_points.begin(), corner_points.end(), compare))
               : (oriented_similarly
                      ? std::min_element(corner_points.begin(), corner_points.end(), compare)
                      : std::max_element(corner_points.begin(), corner_points.end(), compare));
  };

  auto bounding_box_corner_position = mantle_.environment->GetGeometryHelper()->TranslateGlobalPositionLocally(
      entity.GetPosition(),
      mantle_.environment->GetQueryService().GetLaneOrientation(entity.GetPosition()),
      get_corner_point()->position);

  return {bounding_box_corner_position, get_corner_point()->longitudinal_location};
}

std::optional<units::length::meter_t> LateralDistanceAction::CalculateDesiredDistanceFromCenterline(
    const mantle_api::Pose& ref_entity_pose,
    mantle_api::LateralDisplacementDirection direction)
{
  const auto laterally_shifted_position = mantle_.environment->GetQueryService().GetPosition(
      ref_entity_pose,
      direction,
      units::make_unit<units::length::meter_t>(values_.distance));
  if (!laterally_shifted_position)
  {
    return std::nullopt;
  }

  const auto projected_centerline_position =
      mantle_.environment->GetQueryService().GetProjectedCenterLinePoint(ref_entity_pose.position);
  if (!projected_centerline_position)
  {
    return std::nullopt;
  }

  direction_from_centerline_ = IsLeftToCenterline(projected_centerline_position.value(), laterally_shifted_position.value()) ? mantle_api::LateralDisplacementDirection::kLeft : mantle_api::LateralDisplacementDirection::kRight;

  return (laterally_shifted_position.value() - projected_centerline_position.value()).Length();
}

std::optional<units::length::meter_t> LateralDistanceAction::GetDesiredDistance(
    const mantle_api::IEntity& ref_entity)
{
  if (!values_.freespace)
  {
    auto entity_ref_position = mantle_.environment->GetGeometryHelper()->TranslateGlobalPositionLocally(
        ref_entity.GetPosition(),
        ref_entity.GetOrientation(),
        -ref_entity.GetProperties()->bounding_box.geometric_center);

    const mantle_api::Pose pose{entity_ref_position, ref_entity.GetOrientation()};
    return CalculateDesiredDistanceFromCenterline(pose, values_.lateralDisplacement);
  }
  else
  {
    auto CalculateDistance = [&](auto&& lateral_location, auto&& lateral_displacement_direction)
    {
      auto bounding_box_corner_point = GetBoundingBoxCornerPoint(ref_entity, lateral_location);
      mantle_api::Pose pose{bounding_box_corner_point.position, ref_entity.GetOrientation()};
      return CalculateDesiredDistanceFromCenterline(pose, lateral_displacement_direction);
    };

    if (values_.lateralDisplacement == mantle_api::LateralDisplacementDirection::kAny)
    {
      const auto distance_left = CalculateDistance(LateralLocation::kLeft, mantle_api::LateralDisplacementDirection::kLeft);
      return distance_left ? distance_left : CalculateDistance(LateralLocation::kRight, mantle_api::LateralDisplacementDirection::kRight);
    }
    else
    {
      LateralLocation lateral_location = values_.lateralDisplacement == mantle_api::LateralDisplacementDirection::kLeft ? LateralLocation::kLeft : LateralLocation::kRight;
      return CalculateDistance(lateral_location, values_.lateralDisplacement);
    }
  }
}

std::optional<mantle_api::Vec3<units::length::meter_t>> LateralDistanceAction::CalculateTargetPosition(
    const mantle_api::IEntity& actor_entity,
    LateralLocation lateral_location_on_actor_bounding_box) const
{
  const auto bounding_box_point = GetBoundingBoxCornerPoint(actor_entity, lateral_location_on_actor_bounding_box);
  auto pose_projected_to_ref_lane = mantle_.environment->GetQueryService().GetProjectedPoseAtLane(
      bounding_box_point.position, ref_entity_lane_id_);
  std::optional<mantle_api::Vec3<units::length::meter_t>> target_position{};

  if (pose_projected_to_ref_lane)
  {
    target_position = mantle_.environment->GetQueryService().GetPosition(
        pose_projected_to_ref_lane.value(),
        direction_from_centerline_,
        desired_distance_);
  }
  if (!target_position)
  {
    return std::nullopt;
  }

  return TranslateBoundingBoxCornerToCenter(actor_entity,
                                            target_position.value(),
                                            lateral_location_on_actor_bounding_box,
                                            bounding_box_point.longitudinal_location);
}

std::optional<mantle_api::Vec3<units::length::meter_t>> LateralDistanceAction::GetTargetPositionFromReferencePoint(
    const mantle_api::IEntity& actor_entity) const
{
  auto actor_ref_position = mantle_.environment->GetGeometryHelper()->TranslateGlobalPositionLocally(
      actor_entity.GetPosition(),
      actor_entity.GetOrientation(),
      -actor_entity.GetProperties()->bounding_box.geometric_center);

  auto actor_ref_pose_projected_to_ref_entity_centerline = mantle_.environment->GetQueryService().GetProjectedPoseAtLane(
      actor_ref_position, ref_entity_lane_id_);
  std::optional<mantle_api::Vec3<units::length::meter_t>> target_position{};
  if (actor_ref_pose_projected_to_ref_entity_centerline)
  {
    target_position = mantle_.environment->GetQueryService().GetPosition(
        actor_ref_pose_projected_to_ref_entity_centerline.value(),
        direction_from_centerline_,
        desired_distance_);
  }
  if (!target_position)
  {
    return std::nullopt;
  }

  return mantle_.environment->GetGeometryHelper()->TranslateGlobalPositionLocally(
      target_position.value(),
      actor_entity.GetOrientation(),
      actor_entity.GetProperties()->bounding_box.geometric_center);
}

std::optional<mantle_api::Vec3<units::length::meter_t>> LateralDistanceAction::GetTargetPositionFromBoundingBoxCornerPoint(
    const mantle_api::IEntity& actor_entity,
    const mantle_api::Orientation3<units::angle::radian_t>& ref_entity_lane_orientation) const
{
  auto GetLateralLocation = [&]()
  {
    const auto oriented_similarly =
        mantle_.environment->GetGeometryHelper()->AreOrientedSimilarly(actor_entity.GetOrientation(),
                                                                       ref_entity_lane_orientation);

    return direction_from_centerline_ == mantle_api::LateralDisplacementDirection::kLeft
               ? (oriented_similarly ? LateralLocation::kRight : LateralLocation::kLeft)
               : (oriented_similarly ? LateralLocation::kLeft : LateralLocation::kRight);
  };
  return CalculateTargetPosition(actor_entity, GetLateralLocation());
}

std::optional<mantle_api::Vec3<units::length::meter_t>> LateralDistanceAction::GetTargetPosition(
    const mantle_api::IEntity& actor_entity,
    const mantle_api::Orientation3<units::angle::radian_t>& ref_entity_lane_orientation) const
{
  return values_.freespace ? GetTargetPositionFromBoundingBoxCornerPoint(actor_entity, ref_entity_lane_orientation) : GetTargetPositionFromReferencePoint(actor_entity);
}

void LateralDistanceAction::SetControlStrategy()
{
  if (values_.continuous)
  {
    Logger::Error(
        "LateralDistanceAction: Continuous action is not supported. "
        "Currently, action ends when target distance is reached.");
    return;
  }

  if (values_.coordinateSystem != CoordinateSystem::kLane)
  {
    Logger::Error(
        "LateralDistanceAction: Unsupported coordinate system. "
        "Currently, only \"lane\" can be interpreted.");
    return;
  }

  if (values_.dynamicConstraints.has_value())
  {
    Logger::Error(
        "LateralDistanceAction: Found dynamic constraints. "
        "This feature is currently not implemented.");
    return;
  }

  if (values_.distance < 0)
  {
    throw std::runtime_error(
        "LateralDistanceAction: Distance should be greater than or equal to zero."
        " Please adjust the scenario.");
  }

  const auto& ref_entity = EntityUtils::GetEntityByName(mantle_.environment, values_.entityRef);
  auto ref_entity_lane_ids = mantle_.environment->GetQueryService().GetLaneIdsAtPosition(ref_entity.GetPosition());
  if (ref_entity_lane_ids.empty())
  {
    Logger::Warning(
        "LateralDistanceAction: Reference entity is not on a valid lane. "
        "Given entities have not been updated for LateralDistanceAction.");
    return;
  }
  ref_entity_lane_id_ = static_cast<mantle_api::LaneId>(ref_entity_lane_ids.front());

  auto desired_distance = GetDesiredDistance(ref_entity);
  if (!desired_distance)
  {
    Logger::Warning(
        "LateralDistanceAction: Desired lateral distance cannot be calculated. "
        "Given entities have not been updated for LateralDistanceAction.");
    return;
  }
  desired_distance_ = desired_distance.value();

  for (const auto& entity : values_.entities)
  {
    auto& actor_entity = EntityUtils::GetEntityByName(mantle_.environment, entity);
    const auto& ref_entity_lane_orientation = mantle_.environment->GetQueryService().GetLaneOrientation(ref_entity.GetPosition());
    auto target_position = GetTargetPosition(actor_entity, ref_entity_lane_orientation);
    if (!target_position)
    {
      Logger::Warning("LateralDistanceAction: The pose for the entity with name \"" + entity +
                      "\" could not be resolved. It has not been updated for LateralDistanceAction.");
      return;
    }
    actor_entity.SetPosition(target_position.value());
  }
}

bool LateralDistanceAction::HasControlStrategyGoalBeenReached(const std::string& actor)
{
  if (auto entity = mantle_.environment->GetEntityRepository().Get(actor))
  {
    return (!values_.dynamicConstraints.has_value() && !values_.continuous);
  }
  return false;
}

mantle_api::MovementDomain LateralDistanceAction::GetMovementDomain() const
{
  return mantle_api::MovementDomain::kLateral;
}

}  // namespace OpenScenarioEngine::v1_3
