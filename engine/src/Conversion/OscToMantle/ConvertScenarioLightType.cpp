/********************************************************************************
 * Copyright (c) 2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioLightType.h"

namespace OpenScenarioEngine::v1_3
{

namespace detail
{

mantle_api::VehicleLightType ConvertVehicleLightType(NET_ASAM_OPENSCENARIO::v1_3::VehicleLightType vehicleLightType)
{
  const auto literal{vehicleLightType.GetLiteral()};

  switch (vehicleLightType.GetFromLiteral(literal))
  {
    case NET_ASAM_OPENSCENARIO::v1_3::VehicleLightType::VehicleLightTypeEnum::UNKNOWN:
      return mantle_api::VehicleLightType::kUndefined;
    case NET_ASAM_OPENSCENARIO::v1_3::VehicleLightType::VehicleLightTypeEnum::BRAKE_LIGHTS:
      return mantle_api::VehicleLightType::kBrakeLights;
    case NET_ASAM_OPENSCENARIO::v1_3::VehicleLightType::VehicleLightTypeEnum::DAYTIME_RUNNING_LIGHTS:
      return mantle_api::VehicleLightType::kDaytimeRunningLights;
    case NET_ASAM_OPENSCENARIO::v1_3::VehicleLightType::VehicleLightTypeEnum::FOG_LIGHTS:
      return mantle_api::VehicleLightType::kFogLights;
    case NET_ASAM_OPENSCENARIO::v1_3::VehicleLightType::VehicleLightTypeEnum::FOG_LIGHTS_FRONT:
      return mantle_api::VehicleLightType::kFogLightsFront;
    case NET_ASAM_OPENSCENARIO::v1_3::VehicleLightType::VehicleLightTypeEnum::FOG_LIGHTS_REAR:
      return mantle_api::VehicleLightType::kFogLightsRear;
    case NET_ASAM_OPENSCENARIO::v1_3::VehicleLightType::VehicleLightTypeEnum::HIGH_BEAM:
      return mantle_api::VehicleLightType::kHighBeam;
    case NET_ASAM_OPENSCENARIO::v1_3::VehicleLightType::VehicleLightTypeEnum::INDICATOR_LEFT:
      return mantle_api::VehicleLightType::kIndicatorLeft;
    case NET_ASAM_OPENSCENARIO::v1_3::VehicleLightType::VehicleLightTypeEnum::INDICATOR_RIGHT:
      return mantle_api::VehicleLightType::kIndicatorRight;
    case NET_ASAM_OPENSCENARIO::v1_3::VehicleLightType::VehicleLightTypeEnum::LICENSE_PLATE_ILLUMINATION:
      return mantle_api::VehicleLightType::kLicensePlateIllumination;
    case NET_ASAM_OPENSCENARIO::v1_3::VehicleLightType::VehicleLightTypeEnum::LOW_BEAM:
      return mantle_api::VehicleLightType::kLowBeam;
    case NET_ASAM_OPENSCENARIO::v1_3::VehicleLightType::VehicleLightTypeEnum::REVERSING_LIGHTS:
      return mantle_api::VehicleLightType::kReversingLights;
    case NET_ASAM_OPENSCENARIO::v1_3::VehicleLightType::VehicleLightTypeEnum::SPECIAL_PURPOSE_LIGHTS:
      return mantle_api::VehicleLightType::kSpecialPurposeLights;
    case NET_ASAM_OPENSCENARIO::v1_3::VehicleLightType::VehicleLightTypeEnum::WARNING_LIGHTS:
      return mantle_api::VehicleLightType::kWarningLights;
    default:
      return mantle_api::VehicleLightType::kUndefined;
  }
}

}  // namespace detail

mantle_api::LightType ConvertScenarioLightType(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ILightType>& lightType)
{
  if (lightType->GetVehicleLight())
  {
    return detail::ConvertVehicleLightType(lightType->GetVehicleLight()->GetVehicleLightType());
  }
  return lightType->GetUserDefinedLight()->GetUserDefinedLightType();
}

}  // namespace OpenScenarioEngine::v1_3
