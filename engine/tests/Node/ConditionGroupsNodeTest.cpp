/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <memory>

#include "Node/ConditionGroupsNode.h"
#include "Node/Testing/FakeActionNode.h"

using testing::OpenScenarioEngine::v1_3::FakeActionNode;
using testing::Return;

TEST(ConditionGroupsNode, OneChildSucceeds_ReturnsSuccess)
{
  auto child0 = FakeActionNode::CREATE_PTR();
  ON_CALL(*child0, tick()).WillByDefault(Return(yase::NodeStatus::kRunning));

  auto child1 = FakeActionNode::CREATE_PTR();
  ON_CALL(*child1, tick()).WillByDefault(Return(yase::NodeStatus::kSuccess));

  auto conditions_under_test = std::make_shared<OpenScenarioEngine::v1_3::Node::ConditionGroupsNode>("conditionGroups under test");
  conditions_under_test->addChild(child0);
  conditions_under_test->addChild(child1);

  conditions_under_test->onInit();
  ASSERT_THAT(conditions_under_test->executeTick(), yase::NodeStatus::kSuccess);
}

TEST(ConditionGroupsNode, AllChildrenRunning_ReturnsRunning)
{
  auto child0 = FakeActionNode::CREATE_PTR();
  ON_CALL(*child0, tick()).WillByDefault(Return(yase::NodeStatus::kRunning));

  auto child1 = FakeActionNode::CREATE_PTR();
  ON_CALL(*child1, tick()).WillByDefault(Return(yase::NodeStatus::kRunning));

  auto conditions_under_test = std::make_shared<OpenScenarioEngine::v1_3::Node::ConditionGroupsNode>("conditionGroups under test");
  conditions_under_test->addChild(child0);
  conditions_under_test->addChild(child1);

  conditions_under_test->onInit();
  ASSERT_THAT(conditions_under_test->executeTick(), yase::NodeStatus::kRunning);
}

TEST(ConditionGroupsNode, AllChildSucceed_ReturnsSuccess)
{
  auto child0 = FakeActionNode::CREATE_PTR();
  ON_CALL(*child0, tick()).WillByDefault(Return(yase::NodeStatus::kSuccess));

  auto child1 = FakeActionNode::CREATE_PTR();
  ON_CALL(*child1, tick()).WillByDefault(Return(yase::NodeStatus::kSuccess));

  auto conditions_under_test = std::make_shared<OpenScenarioEngine::v1_3::Node::ConditionGroupsNode>("conditionGroups under test");
  conditions_under_test->addChild(child0);
  conditions_under_test->addChild(child1);

  conditions_under_test->onInit();
  ASSERT_THAT(conditions_under_test->executeTick(), yase::NodeStatus::kSuccess);
}