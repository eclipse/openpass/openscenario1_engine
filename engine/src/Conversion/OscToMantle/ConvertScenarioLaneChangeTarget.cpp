
/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioLaneChangeTarget.h"

#include <stdexcept>
#include <utility>

#include "Conversion/OscToMantle/ConvertScenarioAbsoluteTargetLane.h"
#include "Conversion/OscToMantle/ConvertScenarioRelativeTargetLane.h"

namespace OpenScenarioEngine::v1_3
{
mantle_api::LaneId ConvertScenarioLaneChangeTarget(
    const std::shared_ptr<mantle_api::IEnvironment>& environment,
    const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ILaneChangeTarget>& laneChangeTarget)
{
  if (const auto& relativeTargetLane = laneChangeTarget->GetRelativeTargetLane(); relativeTargetLane)
  {
    return ConvertScenarioRelativeTargetLane(environment, relativeTargetLane);
  }
  if (const auto& absoluteTargetLane = laneChangeTarget->GetAbsoluteTargetLane(); absoluteTargetLane)
  {
    return ConvertScenarioAbsoluteTargetLane(absoluteTargetLane);
  }
  throw std::runtime_error("ConvertScenarioLaneChangeTarget: Either RelativeTargetLane or AbsoluteTargetLane must be specified.");
}
}  // namespace OpenScenarioEngine::v1_3
