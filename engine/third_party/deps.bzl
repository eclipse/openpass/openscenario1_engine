load("@//third_party/boost:boost.bzl", "boost_deps")
load("@//third_party/mantle_api:mantle_api.bzl", "mantle_api")
load("@//third_party/open_scenario_parser:open_scenario_parser.bzl", "open_scenario_parser")
load("@//third_party/stochastics_library:stochastics_library.bzl", "stochastics_library")
load("@//third_party/units:units.bzl", "units_nhh")
load("@//third_party/yase:yase.bzl", "yase")



def osc_engine_deps():
    """Load dependencies"""
    boost_deps()
    mantle_api()
    open_scenario_parser()
    stochastics_library()
    units_nhh()
    yase()

