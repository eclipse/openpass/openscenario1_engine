/*******************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#pragma once

#include <MantleAPI/Common/i_logger.h>
#include <MantleAPI/Common/log_utils.h>
#include <gmock/gmock.h>

namespace mantle_api
{
inline void PrintTo(mantle_api::LogLevel log_level, ::std::ostream* os)
{
  *os << log_level;
}
}  // namespace mantle_api

namespace testing::OpenScenarioEngine::v1_3
{
class MockLogger : public mantle_api::ILogger
{
public:
  MOCK_METHOD(mantle_api::LogLevel, GetCurrentLogLevel, (), (const, noexcept, override));
  MOCK_METHOD(void, Log, (mantle_api::LogLevel level, std::string_view message), (noexcept, override));
};

}  // namespace testing::OpenScenarioEngine::v1_3