
/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioController.h"

#include <openScenarioLib/generated/v1_3/catalog/CatalogHelperV1_3.h>

#include <memory>
#include <utility>

namespace OpenScenarioEngine::v1_3
{
namespace detail
{
mantle_api::ExternalControllerConfig ConvertExternalControllerConfig(
    const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IController>& controller)
{
  mantle_api::ExternalControllerConfig external_config;
  external_config.name = controller->GetName();

  for (const auto& property : controller->GetProperties()->GetProperties())
  {
    external_config.parameters.emplace(property->GetName(), property->GetValue());
  }

  return external_config;
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IController> GetController(
    std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IController> controller)
{
  return controller;
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IController> ConvertCatalogReferenceToController(
    const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ICatalogReference>& catalogReference)
{
  auto catalogElement = catalogReference->GetRef();
  return NET_ASAM_OPENSCENARIO::v1_3::CatalogHelper::AsController(catalogElement);
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IController> ResolveChoice(
    const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IController>& controller,
    const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ICatalogReference>& catalogReference)
{
  if (controller)
  {
    return GetController(controller);
  }
  if (catalogReference)
  {
    return ConvertCatalogReferenceToController(catalogReference);
  }
  throw std::runtime_error("ConvertScenarioController: No controller defined or referenced. Please adjust the scenario.");
}
}  // namespace detail

Controller ConvertScenarioController(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IController>& controller,
                                     const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ICatalogReference>& catalogReference)
{
  return detail::ConvertExternalControllerConfig(
      detail::ResolveChoice(controller, catalogReference));
}

ControllerRef ConvertScenarioControllerRef(const std::shared_ptr<NET_ASAM_OPENSCENARIO::INamedReference<NET_ASAM_OPENSCENARIO::v1_3::IController>>& controllerRef)
{
  return controllerRef ? std::make_optional(controllerRef->GetNameRef()) : std::nullopt;
}

}  // namespace OpenScenarioEngine::v1_3