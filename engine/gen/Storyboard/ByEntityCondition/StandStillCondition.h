/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/action_node.h>

#include <cassert>
#include <utility>

#include "Storyboard/ByEntityCondition/StandStillCondition_impl.h"
#include "Utils/EntityBroker.h"

namespace OpenScenarioEngine::v1_3::Node
{
class StandStillCondition : public yase::ActionNode
{
public:
  StandStillCondition(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IStandStillCondition> standStillCondition)
      : yase::ActionNode{"StandStillCondition"},
        standStillCondition_{standStillCondition}
  {
  }

  void onInit() override{};

private:
  yase::NodeStatus tick() override
  {
    assert(impl_);

    const auto is_satisfied = impl_->IsSatisfied();

    if (is_satisfied)
    {
      if (entityBroker_)
      {
        entityBroker_->add_triggering(triggeringEntity_);
      }
      return yase::NodeStatus::kSuccess;
    }

    return yase::NodeStatus::kRunning;
  };

  void lookupAndRegisterData(yase::Blackboard& blackboard) final
  {
    triggeringEntity_ = blackboard.get<std::string>("TriggeringEntity");
    if (blackboard.exists("EntityBroker"))
    {
      entityBroker_ = blackboard.get<EntityBroker::Ptr>("EntityBroker");
    }

    auto environment = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");

    impl_ = std::make_unique<OpenScenarioEngine::v1_3::StandStillCondition>(
        OpenScenarioEngine::v1_3::StandStillCondition::Values{
            triggeringEntity_,
            standStillCondition_->GetDuration()},
        OpenScenarioEngine::v1_3::StandStillCondition::Interfaces{
            environment});
  }

  std::unique_ptr<OpenScenarioEngine::v1_3::StandStillCondition> impl_{nullptr};
  std::string triggeringEntity_;
  EntityBroker::Ptr entityBroker_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IStandStillCondition> standStillCondition_;
};

}  // namespace OpenScenarioEngine::v1_3::Node
