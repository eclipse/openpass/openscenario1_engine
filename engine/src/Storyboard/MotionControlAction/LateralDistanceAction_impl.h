/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <utility>

#include "Storyboard/MotionControlAction/LateralDistanceAction_base.h"

namespace OpenScenarioEngine::v1_3
{
class LateralDistanceAction : public LateralDistanceActionBase
{
private:
  enum class LateralLocation
  {
    kLeft,
    kRight
  };

  enum class LongitudinalLocation
  {
    kFront,
    kBack
  };

  struct BoundingBoxPoint
  {
    mantle_api::Vec3<units::length::meter_t> position{};
    LongitudinalLocation longitudinal_location{};
  };

  /// @brief Determines if a laterally shifted position is to the left or right of the
  ///        position on center line in lane coordinate system.
  /// @param position_on_centerline Position on lane center line
  /// @param laterally_shifted_position Position either on the left or right of lane center line
  /// @return true if laterally shifted position is to the left side of position on lane centerline,
  ///         false otherwise
  bool IsLeftToCenterline(const mantle_api::Vec3<units::length::meter_t>& position_on_lane_centerline,
                          const mantle_api::Vec3<units::length::meter_t>& laterally_shifted_position) const;

  /// @brief Moves the bounding box points so that the former center becomes the new origin.
  /// @param entity Given entity. Used to get bounding box properties.
  /// @param corner_position Corner position of the bounding box to be translated.
  /// @param lateral_location Lateral location on the bounding box. Right or left.
  /// @param longitudinal_location Longitudinal location on the bounding box. Front or back.
  /// @return Returns bounding box center position translated from given corner position.
  mantle_api::Vec3<units::length::meter_t> TranslateBoundingBoxCornerToCenter(
      const mantle_api::IEntity& entity,
      const mantle_api::Vec3<units::length::meter_t>& corner_position,
      LateralLocation lateral_location,
      LongitudinalLocation longitudinal_location) const;

  /// @brief Returns list of BoundingBoxPoints on given side of the entity.
  ///        Each BoundingBoxPoint contains the position and corner(front/back) information.
  /// @param entity Given entity. Used to get bounding box properties.
  /// @param lateral_location Lateral location on the bounding box. Right or left.
  /// @return Returns the list of BoundingBoxPoints on the given side of the entity.
  /// @note Only bottom corner points of the entity are considered.
  std::vector<BoundingBoxPoint> GetBoundingBoxCornerPoints(const mantle_api::IEntity& entity,
                                                           LateralLocation lateral_location) const;

  /// @brief Finds rightmost or leftmost BoundingBoxPoint in lane coordinate system
  ///        on the given side of the entity. Each BoundingBoxPoint contains the
  ///        position and corner(front/back) information.
  /// @param entity Given entity. Used to get bounding box properties.
  /// @param lateral_location Lateral location on the bounding box. Right or left.
  /// @return Returns right most or left most BoundingBoxPoint on the given side
  ///         of the entity.
  /// @note Only bottom corner points of the entity are considered.
  BoundingBoxPoint GetBoundingBoxCornerPoint(const mantle_api::IEntity& entity,
                                             LateralLocation lateral_location) const;

  /// @brief Calculates lateral distance between projected center line position of the given pose
  ///        and laterally shifted position from the same pose in given direction.
  /// @param ref_entity_pose Given pose of reference entity.
  /// @param direction Given lateral displacement direction.
  /// @return Returns the distance between projected center line position of the given pose
  ///         and laterally shifted position from the same pose in given direction.
  ///         No output, if projected center line position or laterally shifted position are not
  ///         on valid road.
  std::optional<units::length::meter_t> CalculateDesiredDistanceFromCenterline(
      const mantle_api::Pose& ref_entity_pose,
      mantle_api::LateralDisplacementDirection direction);

  /// @brief Get desired distance for LateralDistanceAction.
  ///        In case of freespace=false, calculates desired distance using reference position
  ///        of the reference entity. In case of freespace=true, calculates it using right most or left most
  ///        bounding box corner point based on the lateral displacement direction defined in
  ///        LateralDistanceAction.
  /// @param ref_entity Reference entity.
  /// @return Returns the desired distance.
  ///         No value, if the distance cannot be calculated.
  std::optional<units::length::meter_t> GetDesiredDistance(const mantle_api::IEntity& ref_entity);

  /// @brief Calculates target position for the entity. It projects the a position
  ///        on the centerline of the reference entity and calculates target position by laterally
  ///        shifting the projected position using given direction and distance.
  /// @param actor_entity Actor entity. Entity which will be laterally shifted to target position
  ///                     on success.
  /// @param lateral_location_on_actor_bounding_box Lateral location on the bounding box of actor entity.
  ///                                               Right or left.
  /// @return Returns the target position for the entity. No output if, the projected position
  //          or laterally shifted position are not on valid road.
  std::optional<mantle_api::Vec3<units::length::meter_t>> CalculateTargetPosition(
      const mantle_api::IEntity& actor_entity,
      LateralLocation lateral_location_on_actor_bounding_box) const;

  /// @brief Calculates target position for actor entity from its reference point.
  /// @param actor_entity Actor entity. Entity which will be laterally shifted to target position
  ///                     on success.
  /// @return Returns the target position for actor entity. No output if, the target position
  ///         cannot be calculated or the position is not on a valid road.
  std::optional<mantle_api::Vec3<units::length::meter_t>> GetTargetPositionFromReferencePoint(
      const mantle_api::IEntity& actor_entity) const;

  /// @brief Calculates target position for actor entity from one of its bounding box corner points.
  /// @param actor_entity Actor entity. Entity which will be laterally shifted to target position
  ///                     on success.
  /// @param ref_entity_lane_orientation Reference entity's lane orientation. Target position is calculated in the
  ///                    direction of reference entity's lane. This parameter is also used to check
  ///                    whether actor entity is oriented in reference entity's lane direction or not.
  /// @return Returns the target position for actor entity. No output if, the target position
  ///         cannot be calculated or the position is not on a valid road.
  std::optional<mantle_api::Vec3<units::length::meter_t>> GetTargetPositionFromBoundingBoxCornerPoint(
      const mantle_api::IEntity& actor_entity,
      const mantle_api::Orientation3<units::angle::radian_t>& ref_entity_lane_orientation) const;

  /// @brief Calculates target position for the actor entity.
  ///        In case of freespace=false, reference position of the entity is used to calculate
  ///        the target position. In case of freespace=true, right most or left most bounding box
  ///        corner point is used.
  /// @param actor_entity Actor entity. Entity which will be laterally shifted to target position
  ///                     on success.
  /// @param ref_entity_lane_orientation Reference entity's lane orientation. Target position is calculated in the
  ///                    direction of reference entity's lane. This parameter is also used to check
  ///                    whether actor entity is oriented in reference entity's lane direction or not.
  /// @return Returns the target position for the entity. No output if, the target position
  ///         cannot be calculated or the position is not on a valid road.
  std::optional<mantle_api::Vec3<units::length::meter_t>> GetTargetPosition(
      const mantle_api::IEntity& actor_entity,
      const mantle_api::Orientation3<units::angle::radian_t>& ref_entity_lane_orientation) const;

  units::length::meter_t desired_distance_{};
  mantle_api::LaneId ref_entity_lane_id_{};
  mantle_api::LateralDisplacementDirection direction_from_centerline_{};

public:
  LateralDistanceAction(Values values, Interfaces interfaces)
      : LateralDistanceActionBase{std::move(values), std::move(interfaces)} {};

  virtual ~LateralDistanceAction() = default;
  void SetControlStrategy() override;
  bool HasControlStrategyGoalBeenReached(const std::string& actor) override;
  [[nodiscard]] mantle_api::MovementDomain GetMovementDomain() const override;
};

}  // namespace OpenScenarioEngine::v1_3
