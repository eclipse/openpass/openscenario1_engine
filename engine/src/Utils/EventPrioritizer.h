/*******************************************************************************
 * Copyright (c) 2025 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <openScenarioLib/generated/v1_3/api/EnumerationsV1_3.h>

#include <set>
#include <unordered_map>

#include "Utils/EventPriority.h"
#include "Utils/IEventPrioritizer.h"

namespace OpenScenarioEngine::v1_3
{

class EventPrioritizer : public IEventPrioritizer
{
public:
  void RegisterEvent(const Event& name, EventPriority priority) override;
  void EventStarted(const Event& name) override;
  [[nodiscard]] bool ShouldSkipChild(const Event& name) const override;
  [[nodiscard]] bool ShouldStopChild(const Event& name) const override;
  void UpdateOverriddenEvents() override;

private:
  void mark_events_overridden(const Event& overriding_event);

  std::unordered_map<Event, EventPriority> event_priorities_;
  std::set<Event> started_events_;
  std::set<Event> events_to_override_;
  Event overriding_event_;
};

}  // namespace OpenScenarioEngine::v1_3
