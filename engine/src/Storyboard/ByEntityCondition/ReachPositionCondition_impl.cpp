/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/ByEntityCondition/ReachPositionCondition_impl.h"

#include <units.h>

#include "Utils/Logger.h"

namespace OpenScenarioEngine::v1_3
{
bool ReachPositionCondition::IsSatisfied() const
{
  const auto position = values.GetPosition();
  if (!position)
  {
    Logger::Warning("ReachPositionCondition: ReachPositionCondition cannot be satisfied (position undefined).");
    return false;
  }

  const auto& entity = mantle.environment->GetEntityRepository().Get(values.triggeringEntity);
  if (!entity)
  {
    Logger::Warning("ReachPositionCondition: ReachPositionCondition cannot be satisfied (entity undefined).");
    return false;
  }

  auto current_position = entity->get().GetPosition();
  return (position->position - current_position).Length().value() <= values.tolerance;
}

}  // namespace OpenScenarioEngine::v1_3