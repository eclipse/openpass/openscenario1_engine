/********************************************************************************
 * Copyright (c) 2001-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/GenericAction/EnvironmentAction_impl.h"

#include "Utils/Logger.h"

namespace OpenScenarioEngine::v1_3
{
bool EnvironmentAction::Step()
{
  if (values.environment.weather.has_value())
  {
    mantle.environment->SetWeather(values.environment.weather.value());
  }
  if (values.environment.date_time.has_value())
  {
    mantle.environment->SetDateTime(values.environment.date_time.value());
  }

  return true;
}

}  // namespace OpenScenarioEngine::v1_3
