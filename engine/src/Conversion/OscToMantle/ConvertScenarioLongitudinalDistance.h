/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <openScenarioLib/generated/v1_3/api/ApiClassInterfacesV1_3.h>

#include <memory>
#include <string>

namespace OpenScenarioEngine::v1_3
{
using LongitudinalDistance = double;

LongitudinalDistance ConvertScenarioLongitudinalDistance(const std::shared_ptr<mantle_api::IEnvironment>& environment,
                                                         bool isSetDistance,
                                                         double longitudinalDistance,
                                                         bool isSetTimeGap,
                                                         double longitudinalTimeGap,
                                                         const std::shared_ptr<NET_ASAM_OPENSCENARIO::INamedReference<NET_ASAM_OPENSCENARIO::v1_3::IEntity>>& entityRef);
}  // namespace OpenScenarioEngine::v1_3