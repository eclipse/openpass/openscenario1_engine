/*******************************************************************************
 * Copyright (c) 2025 Ansys, Inc.
 * Copyright (c) 2025 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <agnostic_behavior_tree/decorator/data_declaration_node.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <memory>

#include "Node/EventNode.h"
#include "Node/Testing/FakeActionNode.h"
#include "TestUtils/MockEventPrioritizer.h"

using OpenScenarioEngine::v1_3::EventPriority;
using OpenScenarioEngine::v1_3::IEventPrioritizer;
using OpenScenarioEngine::v1_3::Node::EventNode;
using testing::Return;
using testing::OpenScenarioEngine::v1_3::FakeActionNode;
using testing::OpenScenarioEngine::v1_3::MockEventPrioritizer;

class TreeWithPrioritizer
{
public:
  TreeWithPrioritizer()
      : event_prioritizer_{std::make_shared<MockEventPrioritizer>()}
  {
    auto declaration = std::make_unique<
        yase::TypeDeclarer<
            std::shared_ptr<
                IEventPrioritizer>>>("EventPrioritizer", event_prioritizer_);
    root_ = std::make_shared<yase::DataDeclarationNode>("RootWithEventPrioritizer", std::move(declaration));
  }

  yase::DecoratorNode& get_root()
  {
    return *root_;
  }

  MockEventPrioritizer& get_event_prioritizer()
  {
    return *event_prioritizer_;
  }

private:
  std::shared_ptr<MockEventPrioritizer> event_prioritizer_;
  yase::DataDeclarationNode::Ptr root_;
};

class EventNodeTest : public testing::Test
{
protected:
  EventNodeTest()
      : root{tree.get_root()}, event_prioritizer{tree.get_event_prioritizer()}
  {
    ON_CALL(event_prioritizer, RegisterEvent).WillByDefault(Return());
    ON_CALL(*mock_action_node, tick()).WillByDefault(Return(yase::NodeStatus::kSuccess));
    ON_CALL(*mock_start_trigger, tick()).WillByDefault(Return(yase::NodeStatus::kSuccess));
  }

  auto CreateEventWithPriority(EventPriority priority)
  {
    return std::make_shared<EventNode>("Event", mock_action_node, mock_start_trigger, priority);
  }

  TreeWithPrioritizer tree;
  yase::DecoratorNode& root;
  MockEventPrioritizer& event_prioritizer;
  std::shared_ptr<testing::NiceMock<FakeActionNode>> mock_action_node = FakeActionNode::CREATE_PTR();
  std::shared_ptr<testing::NiceMock<FakeActionNode>> mock_start_trigger = FakeActionNode::CREATE_PTR();
};

TEST_F(EventNodeTest, GivenUnstartedNodeWithParallelPriority_WhenParentTicks_ThenEventTicksAndIsRunning)
{
  auto event_under_test = CreateEventWithPriority(EventPriority::kParallel);
  root.setChild(event_under_test);
  root.distributeData();

  EXPECT_CALL(event_prioritizer, ShouldStopChild).Times(1).WillOnce(Return(false));
  EXPECT_CALL(event_prioritizer, ShouldSkipChild).Times(1).WillOnce(Return(false));
  EXPECT_CALL(event_prioritizer, EventStarted).Times(1).WillOnce(Return());
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);
}

TEST_F(EventNodeTest, GivenUnstartedNodeWithOverridePriority_WhenParentTicks_ThenEventTicksAndIsRunning)
{
  auto event_under_test = CreateEventWithPriority(EventPriority::kOverride);
  root.setChild(event_under_test);
  root.distributeData();

  EXPECT_CALL(event_prioritizer, ShouldStopChild).Times(1).WillOnce(Return(false));
  EXPECT_CALL(event_prioritizer, ShouldSkipChild).Times(1).WillOnce(Return(false));
  EXPECT_CALL(event_prioritizer, EventStarted).Times(1).WillOnce(Return());
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);
}

TEST_F(EventNodeTest, GivenUnstartedNodeWithOverwritePriority_WhenParentTicks_ThenEventTicksAndIsRunning)
{
  auto event_under_test = CreateEventWithPriority(EventPriority::kOverwrite);
  root.setChild(event_under_test);
  root.distributeData();

  EXPECT_CALL(event_prioritizer, ShouldStopChild).Times(1).WillOnce(Return(false));
  EXPECT_CALL(event_prioritizer, ShouldSkipChild).Times(1).WillOnce(Return(false));
  EXPECT_CALL(event_prioritizer, EventStarted).Times(1).WillOnce(Return());
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kRunning);
}

TEST_F(EventNodeTest, GivenUnstartedNodeWithSkipPriority_WhenParentTicks_ThenEventDoesNotStartAndSucceeds)
{
  auto event_under_test = CreateEventWithPriority(EventPriority::kSkip);
  root.setChild(event_under_test);
  root.distributeData();

  EXPECT_CALL(event_prioritizer, ShouldStopChild).Times(1).WillOnce(Return(false));
  EXPECT_CALL(event_prioritizer, ShouldSkipChild).Times(1).WillOnce(Return(true));
  EXPECT_CALL(event_prioritizer, EventStarted).Times(0);
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kSuccess);
}

TEST_F(EventNodeTest, GivenStartedNodeWithNonSkipPriority_WhenShouldStopChildAndParentTicks_ThenEventStopsAndSucceeds)
{
  auto event_under_test = CreateEventWithPriority(EventPriority::kParallel);
  root.setChild(event_under_test);
  root.distributeData();

  EXPECT_CALL(event_prioritizer, ShouldStopChild).Times(1).WillOnce(Return(true));
  EXPECT_CALL(event_prioritizer, ShouldSkipChild).Times(0);
  EXPECT_CALL(event_prioritizer, EventStarted).Times(0);
  ASSERT_THAT(root.executeTick(), yase::NodeStatus::kSuccess);
}

TEST_F(EventNodeTest, GivenEventWithoutStartTrigger_WhenTick_ThenEventStarted)
{
  auto event_under_test = std::make_shared<EventNode>("Event", mock_action_node, nullptr);
  root.setChild(event_under_test);
  root.distributeData();

  EXPECT_CALL(event_prioritizer, ShouldStopChild).Times(1).WillOnce(Return(false));
  EXPECT_CALL(event_prioritizer, ShouldSkipChild).Times(1).WillOnce(Return(false));
  EXPECT_CALL(event_prioritizer, EventStarted).Times(1).WillOnce(Return());
  root.executeTick();
}
