
/********************************************************************************
 * Copyright (c) 2021-2025 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToMantle/ConvertScenarioTrajectoryRef.h"

#include <MantleAPI/Common/clothoid_spline.h>

#include <memory>
#include <unordered_map>
#include <utility>

#include "Conversion/OscToMantle/ConvertScenarioClothoidSpline.h"
#include "Conversion/OscToMantle/ConvertScenarioPosition.h"
#include "Utils/Logger.h"

namespace OpenScenarioEngine::v1_3
{
namespace detail
{
std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITrajectory> GetTrajectory(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITrajectoryRef>& trajectoryRef)
{
  return trajectoryRef->GetTrajectory();
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITrajectory> ConvertCatalogReferenceToTrajectory(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ICatalogReference>& catalogReference)
{
  auto catalogElement = catalogReference->GetRef();
  return NET_ASAM_OPENSCENARIO::v1_3::CatalogHelper::AsTrajectory(catalogElement);
}

std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITrajectory> OpenScenarioToMantle(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITrajectoryRef>& trajectoryRef,
                                                                               const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ICatalogReference>& catalogReference)
{
  if (trajectoryRef)
  {
    return GetTrajectory(trajectoryRef);
  }
  if (catalogReference)
  {
    return ConvertCatalogReferenceToTrajectory(catalogReference);
  }
  throw std::runtime_error("ConvertScenarioTrajectoryRef: Either trajectorReference or catalogReference must be set. Note: Deprecated element Trajectory is not supported.");
}

mantle_api::TrajectoryReferencePoint ParseTrajectoryReferencePoint(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITrajectory>& follow_trajectory_action_trajectory)
{
  mantle_api::TrajectoryReferencePoint reference_point{mantle_api::TrajectoryReferencePoint::kUndefined};

  for (const auto& parameter : follow_trajectory_action_trajectory->GetParameterDeclarations())
  {
    if (parameter->GetName() == "reference")
    {
      const std::unordered_map<std::string, mantle_api::TrajectoryReferencePoint> string_to_reference_point{
          {"rear_axle", mantle_api::TrajectoryReferencePoint::kRearAxle},
          {"front_axle", mantle_api::TrajectoryReferencePoint::kFrontAxle},
          {"center_of_mass", mantle_api::TrajectoryReferencePoint::kCenterOfMass},
          {"bounding_box_center", mantle_api::TrajectoryReferencePoint::kBoundingBoxCenter}};
      const auto value_it = string_to_reference_point.find(parameter->GetValue());
      if (value_it == string_to_reference_point.end())
      {
        throw std::runtime_error("ConvertScenarioTrajectoryRef: Invalid value of Trajectory Parameter \"reference\"");
      }
      else
      {
        reference_point = value_it->second;
      }
    }
  }
  return reference_point;
}

mantle_api::Trajectory ConvertClothoidShape(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITrajectory>& follow_trajectory_action_trajectory)
{
  mantle_api::Trajectory trajectory{follow_trajectory_action_trajectory->GetName()};

  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IShape> follow_trajectory_action_trajectory_shape = follow_trajectory_action_trajectory->GetShape();
  const auto clothoid_spline = follow_trajectory_action_trajectory_shape->GetClothoidSpline();
  const auto converted_clothoid_spline = ConvertClothoidSpline(clothoid_spline);

  trajectory.type = converted_clothoid_spline;
  trajectory.reference = ParseTrajectoryReferencePoint(follow_trajectory_action_trajectory);

  return trajectory;
}

mantle_api::Trajectory ConvertPolyline(const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITrajectory>& follow_trajectory_action_trajectory, const std::shared_ptr<mantle_api::IEnvironment>& environment)
{
  mantle_api::Trajectory trajectory{follow_trajectory_action_trajectory->GetName()};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IShape> follow_trajectory_action_trajectory_shape = follow_trajectory_action_trajectory->GetShape();
  mantle_api::PolyLine poly_line;

  for (const auto& poly_line_vertex : follow_trajectory_action_trajectory_shape->GetPolyline()->GetVertices())
  {
    mantle_api::PolyLinePoint poly_line_point;

    auto poly_line_vertex_position = poly_line_vertex->GetPosition();
    auto pose = ConvertScenarioPosition(environment, poly_line_vertex_position);
    poly_line_point.pose = pose.value();

    poly_line_point.time = poly_line_vertex->IsSetTime() ? std::make_optional<units::time::second_t>(poly_line_vertex->GetTime()) : std::nullopt;

    poly_line.push_back(poly_line_point);
  }

  trajectory.type = poly_line;
  return trajectory;
}

}  // namespace detail

TrajectoryRef ConvertScenarioTrajectoryRef(const std::shared_ptr<mantle_api::IEnvironment>& environment,
                                           const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITrajectoryRef>& trajectoryRef,
                                           const std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ICatalogReference>& catalogReference)
{
  const auto trajectory = detail::OpenScenarioToMantle(trajectoryRef, catalogReference);
  const auto shape = trajectory->GetShape();

  if (shape->IsSetPolyline())
  {
    return detail::ConvertPolyline(trajectory, environment);
  }

  if (shape->IsSetClothoidSpline())
  {
    return detail::ConvertClothoidShape(trajectory);
  }

  Logger::Error("ConvertScenarioTrajectoryRef: Unsupported trajectory shape for trajectory '" + trajectory->GetName() + "'. Only Polyline and ClothoidSpline are supported!");
  return std::nullopt;
}

}  // namespace OpenScenarioEngine::v1_3
