/********************************************************************************
 * Copyright (c) 2021-2025 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>
#include <agnostic_behavior_tree/action_node.h>
#include <openScenarioLib/generated/v1_3/api/ApiClassInterfacesV1_3.h>

#include <cassert>
#include <limits>
#include <utility>

#include "Storyboard/GenericAction/TrafficSinkAction_impl.h"

namespace OpenScenarioEngine::v1_3::Node
{
class TrafficSinkAction : public yase::ActionNode
{
public:
  TrafficSinkAction(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITrafficSinkAction> trafficSinkAction)
      : yase::ActionNode{"TrafficSinkAction"},
        trafficSinkAction_{trafficSinkAction}
  {
  }

  void onInit() override {};

private:
  yase::NodeStatus tick() override
  {
    assert(impl_);
    const auto is_finished = impl_->Step();
    return is_finished ? yase::NodeStatus::kSuccess : yase::NodeStatus::kRunning;
  };

  void lookupAndRegisterData(yase::Blackboard& blackboard) final
  {
    std::shared_ptr<mantle_api::IEnvironment> environment = blackboard.get<std::shared_ptr<mantle_api::IEnvironment>>("Environment");
    auto controllerService = blackboard.get<ControllerServicePtr>("ControllerService");

    impl_ = std::make_unique<OpenScenarioEngine::v1_3::TrafficSinkAction>(
        OpenScenarioEngine::v1_3::TrafficSinkAction::Values{
            trafficSinkAction_->GetRadius(),
            trafficSinkAction_->IsSetRate() ? trafficSinkAction_->GetRate() : std::numeric_limits<double>::infinity(),
            [=]()
            { return ConvertScenarioPosition(environment, trafficSinkAction_->GetPosition()); },
            trafficSinkAction_->IsSetTrafficDefinition()
                ? std::make_optional(ConvertScenarioTrafficDefinition(trafficSinkAction_->GetTrafficDefinition()))
                : std::nullopt},
        OpenScenarioEngine::v1_3::TrafficSinkAction::Interfaces{
            environment},
        OpenScenarioEngine::v1_3::TrafficSinkAction::OseServices{
            controllerService});
  }

  std::unique_ptr<OpenScenarioEngine::v1_3::TrafficSinkAction> impl_{nullptr};
  std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::ITrafficSinkAction> trafficSinkAction_;
};

}  // namespace OpenScenarioEngine::v1_3::Node
