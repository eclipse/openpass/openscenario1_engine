load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

def open_scenario_parser():
    maybe(
        http_archive,
        name = "open_scenario_parser",
        build_file = Label("//:third_party/open_scenario_parser/open_scenario_parser.BUILD"),
        url = "https://github.com/RA-Consulting-GmbH/openscenario.api.test/releases/download/v1.4.0/OpenSCENARIO_API_LinuxSharedRelease_2024.11.18.tgz",
        sha256 = "7a4cdb82ccaaeed5fccf12efd94cf4f9c9d3ac0fc7d7feedd4c0babe2404f853",
        strip_prefix = "OpenSCENARIO_API_LinuxSharedRelease",
    )
