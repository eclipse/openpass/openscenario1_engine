load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")
load("@bazel_tools//tools/build_defs/repo:utils.bzl", "maybe")

_TAG = "v0.0.1"

def yase():
    maybe(
        http_archive,
        name = "yase",
        url = "https://gitlab.eclipse.org/eclipse/openpass/yase/-/archive/{tag}/yase-{tag}.tar.gz".format(tag = _TAG),
        sha256 = "243d710172dff4c4e7ab95e5bd68a6d8335cf8a3fdd1efa87d118250e3b85ee3",
        strip_prefix = "yase-{tag}".format(tag = _TAG),
    )
