################################################################################
# Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################

import logging
import json
import pathlib
import re
import sys
import inspect


from pathlib import Path
from subprocess import call
from typing import Dict

from metainfo import metainfo
from templatemachine import TemplateMachine
from utils import parse_arguments, setup_logging

from model import Converter, UmlClass, ChildType, is_leaf, parse_mappings, Dependency, GeneratorUtils as gUtil
from caseconverter import pascalcase

logger = logging.getLogger()
CLASS_SELECTOR = None
FORCE_GENERATION = False
CLANG_FORMAT = None

def checked(name):
    return f'{name}_' if name in ['private'] else name

class Model():
    def __init__(self, model, dependencies, converters):
        self.nodes = {name: UmlClass(model, name) for name in model}
        self.is_leaf = {name: is_leaf(self.nodes[name], converters) for name in self.nodes}
        self.mappings = {name: parse_mappings(self.nodes[name], converters, dependencies) for name in self.nodes}
        self.tree = self._generate_tree('OpenScenario')

    def get_graph(self, anchor, endNode, trim_to_anchor=False):
        return self._filter_tree(self.tree, anchor, endNode, trim_to_anchor)

    def _generate_tree(self, parent, root = None):
        if not root: root = { parent: dict() }

        node = self.nodes[parent] if parent in self.nodes else None
        logger.debug(f'{inspect.currentframe().f_code.co_name}: {node.name if node else "nothing" }')

        if node:
            if node.name == "Trailer": return root

            for child in node.children(ChildType.MODEL):
                if child.raw_type != 'TrajectoryRef':
                    if self.is_leaf[child.raw_type]:
                        root[parent][child.raw_type] = "don't know" #child.raw_type if not child.raw_type_isMapped else child.raw_type_isMapped
                    else:
                        root[parent][child.raw_type] = self._generate_tree(child.raw_type, dict())
                else:
                    logger.debug(f'{inspect.currentframe().f_code.co_name}: Skipping {list(root)[0]}::{child.type}')

        return root

    def _traverse_model(self, tree, startNode, endNode, anchor):
        try:
            for node in tree[startNode].keys():
                if node == anchor:
                    anchor = None
                if node != endNode:
                    find_node = self._traverse_model(tree[startNode], node, endNode, anchor)
                    if find_node:
                        return [node] + find_node if node not in find_node else find_node
                else:
                    if not anchor: return [node]
            return None
        except:
            return None

    def _filter_tree(self, tree, anchor, endNode, trim_to_anchor = True):
        result = self._traverse_model(tree, 'OpenScenario', endNode, anchor)
        if result and anchor in result:
            if trim_to_anchor:
                return result[result.index('Storyboard'):]
            else: return ['OpenScenario'] + result
        return []


class MetaInfo():
    def __init__(self, meta_info):
        self.interfaces = metainfo['interfaces']
        self.blackboard = meta_info['blackboard']
        self.converter = meta_info['converter']
        self.annotations = meta_info['annotations']
        self.dependencies = meta_info['dependencies']

class YaseUmlClass:
    def __init__(self, umlClass, metainfo: MetaInfo, model: Model):
        self._model = model
        self._umlClass = umlClass
        self.name = umlClass.name
        self.this = umlClass.this
        self.interface = self.resolve_interface(metainfo.interfaces)
        self._deps = list()
        self._parsers = dict()
        self._blackboard = self._resolve_blackboard(metainfo.blackboard)
        self.dependencies = set()
        self.values = self._gather_values()

        self._gather_converter_dependencies()
        self.is_leaf = model.is_leaf[umlClass.name]

    def resolve_interface(self, interfaces):
        for interface, nodes in interfaces.items():
            if self.name in nodes: return interface

    def add_dependencies(self, dependencies):
        for d in dependencies:
            self.dependencies.add(d)
            self._blackboard['get'].add(d)

    def _resolve_blackboard(self, _blackboard):
        blackboard = {'set': set(), 'get': set()}
        for class_name, name, type, source in _blackboard['set']:
            if class_name == self.umlClass.name:
                blackboard['set'].add(
                    Member(name, type, self._model.parse_source(source, class_name)))
                if source == 'ctor':
                    self._deps.append(Member(name, type))

        for class_name, name, type in _blackboard['get']:
            if class_name == self.umlClass.name:
                blackboard['get'].add(Member(name, type))

        return blackboard

    @property
    def ctor(self):
        all = [f'{self.this.type} {gUtil.local(self.this)}']
        deps = list(map(lambda var: f'{var.type} {var.name}', self._deps))
        return ',\n'.join(all + deps)

    @property
    def ctor_chain(self):
        all = [f'yase::ActionNode{{"{self.this.name}"}}']
        deps = list(map(lambda d: f'{gUtil.member(d)}{{{d.name}}}', self._deps))
        var = [f'{gUtil.member(self.this)}{{{gUtil.local(self.this)}}}']
        return ',\n'.join(all + deps + var)

    @property
    def deps(self):
        return self._deps

    def _gather_converter_dependencies(self):
        for child in self.umlClass.children(ChildType.MODEL):
            converter = self._model.mappings[self.name].get(child.name)
            if converter:
                self.add_dependencies(converter.dependencies)


    def _apply_callee(self, values):
        for value in values:
            value.set_callee(self.name)

    def _apply_converter(self, values):
        filtered_values = []

        for converter in filter(lambda v: isinstance(v, Converter), values):
            for value in values:
                if converter.is_consumed(value): filtered_values.append(value)
        return [v for v in values if v not in filtered_values]

    def _gather_values(self):
        values = self.umlClass.children(ChildType.PRIMITIVE)

        for child in self.umlClass.children(ChildType.MODEL):
            converter = self._model.mappings[self.name].get(child.name)
            if converter:
                values.append(converter)

        for child in self.umlClass.children(ChildType.INTERFACE):
            converter = self._model.mappings[self.name].get(child.name)
            if converter:
                values.append(converter)

        for child in self.umlClass.children(ChildType.ENUM):
            converter = self._model.mappings[self.name].get(child.name)
            if converter:
                values.append(converter)

        self._apply_callee(values)
        return self._apply_converter(values)

    @property
    def includes(self):
        includes = list()
        for dep in self.dependencies:
            if dep.include:
                includes.append(dep.include)
        for node in self.values:
            if isinstance(node, Converter):
                includes.append(node.include)
                includes.extend([d.include for d in node.dependencies])
            elif node.include():
                includes.append(node.include())
        return includes

    @property
    def children(self):
        return self._children

    @property
    def umlClass(self):
        return self._umlClass

    @property
    def blackboard_setter(self):
        return self._blackboard['set']

    @property
    def blackboard_getter(self):
        return self._blackboard['get']

def user_file_exists(output_path, user_path = 'src'):
    user_path = Path("/".join(map(lambda p: p if p != 'gen' else user_path, Path(output_path).parts)))
    return user_path.exists()

def generate(name, template, context, output_file, throw_on_fail=True):
    if CLASS_SELECTOR and not any(map(lambda c: c == name, CLASS_SELECTOR)):
        logging.debug(f'Skipping {output_file} (single class selected)')
        return

    if not FORCE_GENERATION and user_file_exists(output_file):
        logging.info(f'Skipping {output_file} (user file exists)')
        return

    logging.info(f'Creating {output_file}')

    template_machine = TemplateMachine(searchpath='templates', suffix='j2')
    template_machine.create(template,
                            context,
                            output_file,
                            throw_on_fail)

    clang_format_executable = CLANG_FORMAT if CLANG_FORMAT else "clang-format"
    try:
        call([clang_format_executable, "-i", "-style=file", output_file])
    except FileNotFoundError as e:
        logging.error("'clang-format' not found. Install it or specify path by setting '--clang-format'")

def get_from_json():
    with open('model/v1_3.json') as f:
        return json.load(f)

class PropertyCache():
    parsers = []
    nodes = []


class Generator():
    def __init__(self, nodes, graph, output_path, leaf_type):
        self.nodes = nodes
        self.graph = graph
        self.output_path = output_path
        self.leaf_type = leaf_type

    def context(self, node, filename=None) -> Dict[str, object]:
        context = {
            'ose_namespace': 'OpenScenarioEngine::v1_3',
            'ose_type': self.leaf_type,
            'node': node,
            'name': node.name,
            'util': gUtil,
            'ChildType': ChildType,
            'output_path': self.output_path
        }
        if filename:
            context['filename'] = filename
        return context

    def generate_leaf(self, node):
        if node.name in PropertyCache.nodes: return
        out_name = f'{self.output_path}/{self.leaf_type}/{node.name}'
        context = self.context(node)
        generate(node.name, f'{self.leaf_type}.h', context, f'{out_name}.h')
        generate(node.name, f'{self.leaf_type}_base.h', context, f'{out_name}_base.h', throw_on_fail=False)
        generate(node.name, f'{self.leaf_type}_impl.h', context, f'{out_name}_impl.h', throw_on_fail=False)
        generate(node.name, f'{self.leaf_type}_impl.cpp', context, f'{out_name}_impl.cpp', throw_on_fail=False)
        PropertyCache.nodes.append(node.name)

    def generate_parser(self, node, template):
        if node.name in PropertyCache.parsers: return
        filename = f'Parse{node.name}'
        out_name = f'{self.output_path}/Conversion/OscToNode/{filename}'
        context = self.context(node, filename=filename)
        generate(node.name, f'{template}.h', context, f'{out_name}.h')
        generate(node.name, f'{template}.cpp', context, f'{out_name}.cpp')
        PropertyCache.parsers.append(node.name)

    def generate_graph_nodes(self):
        for name in self.graph[:-1]:
            node = self.nodes[name]

            for list_child in node.children(ChildType.LIST):
                self.generate_parser(list_child, 'list_resolver')

            if node.children(ChildType.CHOICE):
                self.generate_parser(node, 'choice_resolver')

            # currently Nodes with start / stop trigger are never choices
            elif node.has_start_trigger or node.has_stop_trigger:
                self.generate_parser(node, 'trigger_resolver')

            else:
                self.generate_parser(node, 'parser')

def generate_files_for(umlClass, graph, model, metaInfo, ose_type, dependencies, output_path):
    logging.info(f'Resolving graph: {graph}')
    node = YaseUmlClass(umlClass, metaInfo, model)
    node.add_dependencies([Dependency(**metaInfo.dependencies[d]) for d in dependencies])
    gen = Generator(model.nodes, graph, output_path, leaf_type=(node.interface if node.interface else ose_type))
    gen.generate_leaf(node)
    gen.generate_parser(node, 'glue_logic_parser')
    gen.generate_graph_nodes()

def is_new_leaf(model, pattern, name):

    for child_name, _ in model.nodes.items():
        if name == child_name: continue
        child_graph = model.get_graph(name, child_name, trim_to_anchor=True)
        if len(child_graph):
            if re.match(pattern, child_name):
                return False
    return True

def main():
    output_path = '../engine/gen'

    metaInfo = MetaInfo(metainfo)
    model = Model(get_from_json(), metaInfo.dependencies, metaInfo.converter)

    for name, uml_class in model.nodes.items():
        for root, pattern, ose_type, dependencies in metainfo['selector']:
            graph = model.get_graph(root, name, trim_to_anchor=True)
            if len(graph) and re.match(pattern, name): # todo overhaul tree
                model.is_leaf[name] = is_new_leaf(model, pattern, name)
                if (model.is_leaf[name]):
                    generate_files_for(uml_class, graph, model, metaInfo, ose_type, dependencies, output_path)

    source_files = map(lambda p: str(p).replace('../engine/', ''),
                       list(pathlib.Path('../engine/gen').glob('**/*.cpp')) +
                       list(pathlib.Path('../engine/src').glob('**/*.cpp')))

    header_files = map(lambda p: str(p).replace('../engine/', ''),
                       list(pathlib.Path('../engine/gen').glob('**/*.h')) +
                       list(pathlib.Path('../engine/src').glob('**/*.h')))

    template_machine = TemplateMachine(searchpath='templates', suffix='j2')
    template_machine.create('generated_files.cmake',
                        {'sourcefiles': sorted(list(source_files)),
                         'headerfiles': sorted(list(header_files))},
                        '../engine/cmake/generated_files.cmake',
                        throw_on_fail=True)

if __name__ == '__main__':
    args = parse_arguments()
    setup_logging(args.debug, args.log_file)
    CLASS_SELECTOR = args.classes
    FORCE_GENERATION = args.force
    CLANG_FORMAT = args.clang_format
    main()


def main2():

    output_path = '../engine/gen'

    metaInfo = MetaInfo(metainfo)
    model = Model(get_from_json(), metaInfo.dependencies, metaInfo.converter)

    for name, uml_class in model.nodes.items():
        for root, pattern, ose_type, dependencies in metainfo['selector']:
            graph = model.get_graph(root, name, trim_to_anchor=True)

            if len(graph) and re.match(pattern, name):
                logging.info(f"Found potential {ose_type}: {name}")
                for child_name, _ in model.nodes.items():
                    if name == child_name: continue
                    child_graph = model.get_graph(name, child_name, trim_to_anchor=True)
                    params = []
                    if len(child_graph):
                        if re.match(pattern, child_name):
                            logging.info(f"-> That was a mistake - actual {ose_type} is {child_name}")
                            params.clear()
                            break
                        else:
                            params.append(child_name)
                    for param in params:
                        logging.info(f"- Parameter: {param}")
