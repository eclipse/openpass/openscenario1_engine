/*******************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#include "Node/ConditionsNode.h"

#include <stdexcept>

namespace OpenScenarioEngine::v1_3::Node
{
using namespace yase;

ConditionsNode::ConditionsNode(const std::string &name)
    : CompositeNode(name) {}

void ConditionsNode::onInit()
{
  for (auto &child : m_children_nodes)
  {
    child->onInit();
  }
}

NodeStatus ConditionsNode::tick()
{
  size_t succeeded_children = 0;
  for (auto &child : m_children_nodes)
  {
    const auto child_status = child->executeTick();
    if (child_status == NodeStatus::kFailure)
    {
      throw std::runtime_error("Conditions are not allowed to fail.");
    }
    if (child_status == NodeStatus::kSuccess)
    {
      succeeded_children++;
    }
  }

  return succeeded_children == childrenCount() ? NodeStatus::kSuccess : NodeStatus::kRunning;
}

}  // namespace OpenScenarioEngine::v1_3::Node