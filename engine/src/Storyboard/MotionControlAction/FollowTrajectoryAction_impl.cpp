/********************************************************************************
 * Copyright (c) 2021-2025 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Storyboard/MotionControlAction/FollowTrajectoryAction_impl.h"

#include <MantleAPI/Common/clothoid_spline.h>
#include <MantleAPI/Common/trajectory.h>
#include <MantleAPI/Traffic/entity_helper.h>

#include <optional>
#include <variant>

#include "Utils/EntityUtils.h"
#include "Utils/Logger.h"

namespace OpenScenarioEngine::v1_3
{

namespace detail
{

mantle_api::Trajectory ConvertPolyLine(
    const std::shared_ptr<mantle_api::IEnvironment>& environment,
    const std::string& entity,
    TrajectoryRef trajectoryRef)
{
  const auto& refEntity = EntityUtils::GetEntityByName(environment, entity);
  const auto geometric_center = refEntity.GetProperties()->bounding_box.geometric_center;

  // modifies the copy of the trajectory ref!
  for (auto& polyLinePoint : std::get<mantle_api::PolyLine>(trajectoryRef->type))
  {
    polyLinePoint.pose.position = environment->GetGeometryHelper()->TranslateGlobalPositionLocally(
        polyLinePoint.pose.position,
        polyLinePoint.pose.orientation,
        geometric_center);
  }
  return trajectoryRef.value();
}

mantle_api::Trajectory ConvertClothoidSpline(
    const std::shared_ptr<mantle_api::IEnvironment>& environment,
    const std::string& entity,
    TrajectoryRef trajectoryRef)
{
  const auto& refEntity = EntityUtils::GetEntityByName(environment, entity);
  const auto geometric_center = refEntity.GetProperties()->bounding_box.geometric_center;

  auto& clothoid_spline = std::get<mantle_api::ClothoidSpline>(trajectoryRef->type);

  for (auto& segment : clothoid_spline.segments)
  {
    if (segment.position_start.has_value())
    {
      segment.position_start.value().position = environment->GetGeometryHelper()->TranslateGlobalPositionLocally(
          segment.position_start.value().position,
          segment.position_start.value().orientation,
          geometric_center);
    }
  }

  return trajectoryRef.value();
}

}  // namespace detail

template <class... Ts>
struct overloaded : Ts...
{
  using Ts::operator()...;
};

template <class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;

void FollowTrajectoryAction::SetControlStrategy()
{
  if (!values.trajectoryRef)
  {
    Logger::Error(
        "FollowTrajectoryAction does not contain TrajectoryRef.");
    return;
  }

  for (const auto& entity : values.entities)
  {
    auto control_strategy = std::make_shared<mantle_api::FollowTrajectoryControlStrategy>();
    control_strategy->timeReference = values.timeReference;
    control_strategy->movement_domain = values.timeReference
                                            ? mantle_api::MovementDomain::kBoth
                                            : mantle_api::MovementDomain::kLateral;

    std::optional<mantle_api::Trajectory> trajectory =
        std::visit(
            overloaded{
                [&](const mantle_api::PolyLine&)
                {
                  return detail::ConvertPolyLine(mantle.environment, entity, values.trajectoryRef);
                },
                [&](const mantle_api::ClothoidSpline&)
                {
                  return detail::ConvertClothoidSpline(mantle.environment, entity, values.trajectoryRef);
                },
                [](auto)
                {
                  Logger::Error("Unsupported type of Trajectory shape");
                  return std::nullopt;
                }},
            values.trajectoryRef->type);

    if (!trajectory)
    {
      return;
    }
    control_strategy->trajectory = *trajectory;
    const auto entity_id = mantle.environment->GetEntityRepository().Get(entity)->get().GetUniqueId();
    mantle.environment->UpdateControlStrategies(entity_id, {control_strategy});
  }
}

bool FollowTrajectoryAction::HasControlStrategyGoalBeenReached(const std::string& actor)
{
  if (auto entity = mantle.environment->GetEntityRepository().Get(actor))
  {
    return mantle.environment->HasControlStrategyGoalBeenReached(
        entity.value().get().GetUniqueId(),
        mantle_api::ControlStrategyType::kFollowTrajectory);
  }
  return false;
}

mantle_api::MovementDomain FollowTrajectoryAction::GetMovementDomain() const
{
  return mantle_api::MovementDomain::kBoth;
}

}  // namespace OpenScenarioEngine::v1_3
