/*******************************************************************************
 * Copyright (c) 2025 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <string>
#include <vector>

#include "Utils/EventPriority.h"

namespace OpenScenarioEngine::v1_3
{

using Event = std::string;
using Events = std::vector<Event>;

/// Allows Events to check whether they should be skipped or overridden from other events in the same maneuver
class IEventPrioritizer
{
public:
  /// Default destructor
  virtual ~IEventPrioritizer() = default;

  /// @brief Register an Event to be part of a maneuver
  ///
  /// @param name The name of the event
  /// @param priority The priority of the event
  virtual void RegisterEvent(const Event& name, EventPriority priority) = 0;

  /// @brief Set the state of an event to started
  ///
  /// @param name The name of the event
  virtual void EventStarted(const Event& name) = 0;

  /// @brief Check whether an event should be skipped based on the priority
  ///
  /// @param name The name of the event
  /// @return Whether the event should be skipped
  [[nodiscard]] virtual bool ShouldSkipChild(const Event& name) const = 0;

  /// @brief Check whether an event should be stopped by being overridden by another event
  ///
  /// @param name The name of the event
  /// @return Whether the event should be stopped
  [[nodiscard]] virtual bool ShouldStopChild(const Event& name) const = 0;

  /// @brief Check whether started events should be stopped due to an override event starting
  virtual void UpdateOverriddenEvents() = 0;
};

}  // namespace OpenScenarioEngine::v1_3
