/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <gtest/gtest.h>
#include <openScenarioLib/generated/v1_3/impl/ApiClassImplV1_3.h>

#include "Conversion/OscToMantle/ConvertScenarioTimeReference.h"

using namespace units::literals;

TEST(ConvertScenarioTimeReferenceTest, GetNullPointerFromTimeReference)
{
  auto time_reference_ = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::TimeReferenceImpl>();
  auto timeReference = OpenScenarioEngine::v1_3::ConvertScenarioTimeReference(time_reference_);
  ASSERT_EQ(std::nullopt, timeReference);
}

TEST(ConvertScenarioTimeReferenceTest, GetTimingFromTimeReference_ThenConvertToMantleAPITimeReference)
{
  auto timing = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::TimingImpl>();
  timing->SetOffset(2.0);
  timing->SetScale(1.0);

  auto time_reference_ = std::make_shared<NET_ASAM_OPENSCENARIO::v1_3::TimeReferenceImpl>();
  time_reference_->SetTiming(timing);

  auto timeReference = OpenScenarioEngine::v1_3::ConvertScenarioTimeReference(time_reference_);

  ASSERT_EQ(1.0, timeReference->scale);
  ASSERT_EQ(units::time::second_t(2.0), timeReference->offset);
}