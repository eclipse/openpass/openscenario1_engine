/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include "Storyboard/GenericAction/AssignControllerAction_base.h"

namespace OpenScenarioEngine::v1_3
{

class AssignControllerAction : public AssignControllerActionBase
{
public:
  AssignControllerAction(Values values, Interfaces interfaces)
      : AssignControllerActionBase{std::move(values), std::move(interfaces)} {};

  bool Step() override;
};

}  // namespace OpenScenarioEngine::v1_3