/********************************************************************************
 * Copyright (c) 2023-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Common/vector.h>

namespace OpenScenarioEngine::v1_3
{

bool IsPointOutsideOfEllipse(const mantle_api::Vec3<units::length::meter_t>& point,
                             const mantle_api::Vec3<units::length::meter_t>& ellipse_center,
                             units::length::meter_t ellipse_semi_major_axis,
                             units::length::meter_t ellipse_semi_minor_axis,
                             units::angle::radian_t ellipse_angle,
                             units::length::meter_t tolerance = units::length::meter_t(0.0));

}  // namespace OpenScenarioEngine::v1_3