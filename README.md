# OpenScenarioEngine

- [User Guide](#User-Guide) (this document)
- [Architecture Documentation](doc/architecture.md)
- [Generator Documentation](doc/generator.md)

# User Guide

The **OpenScenarioEngine** is an implementation of the component _OpenSCENARIO Director_, as proposed by [ASAM **OpenSCENARIO XML**](https://www.asam.net/standards/detail/openscenario/):

> An OpenSCENARIO Director (OSC Director) is a component that interprets the OSC Model Instance and governs the progress of a scenario in a simulation. The OSC Director is the component responsible for running the scenario.

## Usage

The engine has several dependencies, which are managed by submodules which are checked out into `engine/deps`.
For a detailed overview of the dependencies please refer to the [Architecture Documentation](doc/architecture.md).

### Prerequisites

The engine internally uses the `OpenScenarioLib`, which need to be built manually (please refer to the [build instructions](https://github.com/RA-Consulting-GmbH/openscenario.api.test/blob/master/doc/main.adoc)) before configuring using CMake.

For CMake to find the library, you need to tell it where the pre-built libraries are via the `CMAKE_PREFIX_PATH`, which can be done e.g. by adding the following lines to the `.bashrc` (if you use bash):

```bash
CMAKE_PREFIX_PATH=<path_to_engine>/engine/deps/openscenario_api/cpp/build/cgReleaseMakeShared/expressionsLib:<path_to_engine>/engine/deps/openscenario_api/cpp/build/cgReleaseMakeShared/openScenarioLib:<path_to_engine>/engine/deps/openscenario_api/cpp/build/cgReleaseMakeShared/antlr4_runtime/src/antlr4_runtime/runtime/Cpp/dist:$CMAKE_PREFIX_PATH
```

### Building the Engine

> Linux
>
> ```bash
> # STARTING FROM ROOT OF REPOSITORY
>
> # get dependencies
> git submodule init
> git submodule update
>
> # building the openscenario_api
> # this might differ on your machine → see official build instructions
> cd engine/deps/openscenario_api/cpp/buildArtifact
> chmod +x generateLinux.sh
> ./generateLinux.sh shared release make parallel
>
> cd ../../../.. # back to engine
> cmake -S . -B build
> cd build
> make OpenScenarioEngine
> ```

> Windows <small>(MINGW64 - experimental)</small>
> ```bash
> # STARTING FROM ROOT OF REPOSITORY
>
> # get dependencies
> git submodule init
> git submodule update
>
> # patching openscenario_api for MinGW64
> cd engine/deps/openscenario_api
> git apply ../patches/openscenario_api/mingw64/v1.3.1.patch
>
> # building the openscenario_api (path to JAVA executable might differ!)
> cd cpp
> cmake -Wno-dev --preset="MSYS-shared-release" -DJava_JAVA_EXECUTABLE=/C/Program\ Files/Zulu/zulu-11/bin/java.exe
> cmake --build --preset="Build-MSYS-shared-release"
>
> # building the engine
> cd ../../.. # back to engine
> cmake -G "MSYS Makefiles" -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=bin -S . -B build
> cd build
> make install
> ```
> ⚠️ Note that the demo is not part the `all` target.

### Running the Demo (Using the Engine)

  The demo uses googletest/googlemock and the fakes supplied by the `MantleAPI` to show how the `OpenScenarioEngine` can be used in an executable.

  > Linux
  > ```bash
  > # STARTING FROM ROOT OF REPOSITORY
  > cd engine
  > mkdir build && cd build
  > cmake ..
  > make OpenScenarioEngineDemo # would also build OpenScenarioEngine
  > ./OpenScenarioEngineDemo ../demo/example/scenario.xosc
  > ```

  > Windows <small>(MINGW64 - experimental)</small>
  > ```bash
  > # STARTING FROM ROOT OF REPOSITORY
  > cd engine
  > cmake -G "MSYS Makefiles" -S . -B build
  > cd build
  > make OpenScenarioEngineDemo # would also build OpenScenarioEngine
  > ./OpenScenarioEngineDemo ../demo/example/scenario.xosc
  > ```
  > ⚠️ If you start the demo, Windows might tell you that some `dll`_s_ are missing. Locate and copy all of them into `build`.

# Features

## Covered Actions and Conditions

The following Actions and Conditions of [ASAM OpenSCENARIO XML](https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/index.html) (currently v1.3.0) are under development:

### Conditions

| Type              | Subtype          | Status                                                                                                       |
| ------------------| ------------- | ------------------------------------------------------------------------------------------------------------ |
| ByEntityCondition | [DistanceCondition](https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/generated/content/DistanceCondition.html) | Supports only `RelativeDistanceType::kLongitudinal && CoordinateSystem::kEntity && freespace=false`
| ByEntityCondition | [ReachPositionCondition](https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/generated/content/ReachPositionCondition.html)       | ✔️ Restricted by conversion: see [Conversions::Position](#position) below                                     |
| ByEntityCondition | [RelativeDistanceCondition](https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/generated/content/RelativeDistanceCondition.html) | Supports only `RelativeDistanceType::kLongitudinal && CoordinateSystem::kEntity`                             |
| ByEntityCondition | [RelativeSpeedCondition](https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/generated/content/RelativeSpeedCondition.html)       | ✔️ Complete                                                                                                   |
| ByEntityCondition | [SpeedCondition](https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/generated/content/SpeedCondition.html)       | ✔️ Without direction                                                                                                   |
| ByEntityCondition | [TimeHeadwayCondition](https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/generated/content/TimeHeadwayCondition.html)           | Supports only `DistanceType::kEuclidean` and `CoordinateSystem::kEntity` or `CoordinateSystem::kLane`                             |
| ByEntityCondition | [TimeToCollisionCondition](https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/generated/content/TimeToCollisionCondition.html)   | In clarification: [Issue #7](https://gitlab.eclipse.org/eclipse/openopass/openscenario1_engine/-/issues/7) |
| ByValueCondition  | [SimulationTimeCondition](https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/generated/content/SimulationTimeCondition.html)     | ✔️ Complete |
| ByValueCondition  | [UserDefinedValueCondition](https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/generated/content/UserDefinedValueCondition.html)     | ✔️ Complete |

### Actions

| Interface           | Type                       | Status     |
| --------------------| -------------------------- | ---------- |
| _none_              | [AcquirePositionAction](https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/generated/content/AcquirePositionAction.html)       | ✔️ Complete   |
| _none_              | [AssignControllerAction](https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/generated/content/AssignControllerAction.html)     | 🚧 Sets without consideration of motion domain         |
| _none_              | [AssignRouteAction](https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/generated/content/AssignRouteAction.html)               | ✔️ Restricted by conversion: see [Conversions::Position](#position)       |
| _none_              | [CustomCommandAction](https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/generated/content/CustomCommandAction.html)       | ✔️ Complete   |
| _none_              | [EnvironmentAction](https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/generated/content/EnvironmentAction.html) | ✔️ Does not interpret `RoadCondition` in `Environment`. In `Weather` does not interpret `FractionalCloudCover`, `Wind`, `DomeImage`. In `Fog` does not interpret `BoundingBox`. In `Precipitation` does not interpret `PrecipitationType`.
| _none_              | [LightStateAction](https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/generated/content/LightStateAction.html) | 🚧  Supports LightType and LightState [only the LightMode], transitionTime is ignored at the moment.
| _none_              | [TeleportAction](https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/generated/content/TeleportAction.html)                     | ✔️ Restricted by conversion: see [Conversions::Position](#position) below |
| _none_              | [TrafficSignalAction](https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/generated/content/TrafficSignalAction.html) | ✔️ Complete                                             |
| _none_              | [TrafficSignalStateAction](https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/generated/content/TrafficSignalStateAction.html) | ✔️ Complete                                             |
| _none_              | [TrafficSinkAction](https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/generated/content/TrafficSinkAction.html)               | ✔️ Does not interpret `rate` and `TrafficDefinition`, and restricted by conversion: see [Conversions::Position](#position) below |
| _none_              | [VisibilityAction](https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/generated/content/VisibilityAction.html)       | ✔️ Complete   |
| MotionControlAction | [FollowTrajectoryAction](https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/generated/content/FollowTrajectoryAction.html)     | 🚧 Supports trajectory shapes polyline and clothoid spline + (Possibility for optimization in the base class)     |
| MotionControlAction | [LaneChangeAction](https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/generated/content/LaneChangeAction.html)                 | ✔️ Complete    |
| MotionControlAction | [LaneOffsetAction](https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/generated/content/LaneOffsetAction.html)                 | 🚧  Supports only AbsoluteTargetLaneOffset. In addition, LaneOffsetActionDynamics and continuous are ignored at the moment    |
| MotionControlAction | [LateralDistanceAction](https://www.asam.net/static_downloads/ASAM_OpenSCENARIO_V1.1.1_Model_Documentation/modelDocumentation/content/LateralDistanceAction.html)       |  ✔️ Does not interpret `DynamicConstraints` and restricted by conversion: see [Conversions::Coordinate System](#coordinate-system) and [Conversions::Continuous](#continuous)    |
| MotionControlAction | [LongitudinalDistanceAction](https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/generated/content/LongitudinalDistanceAction.html) | ❌  Not yet implemented     |
| MotionControlAction | [SpeedAction](https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/generated/content/SpeedAction.html)                           | 🚧 Supports shapes linear and cubic, latter using linear shape coefficients (see [MR74](https://gitlab.eclipse.org/eclipse/openopass/openscenario1_engine/-/merge_requests/74)) |
| TrafficAction | [TrafficSwarmAction](https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/generated/content/TrafficSwarmAction.html)                           | ✔️ Does not interprete `directionOfTravelDistribution`   |

## TrafficSignalController

If defined in the road network, the engine manages [TrafficSignalController](https://publications.pages.asam.net/standards/ASAM_OpenSCENARIO/ASAM_OpenSCENARIO_XML/latest/generated/content/TrafficSignalController.html)s on its own.
The corresponding traffic light states of the traffic light phases are automatically emitted for the respective time steps, using the interface `mantle_api::IEnvironment::SetTrafficSignalState`.

Restrictions:
- Phases must define at least a single TrafficSignalState
- Delays and references between controllers not implemented yet
- TrafficSignalGroupStates (openSCENARIO 1.2) currently not supported

## Conversions

### Absolute Target Lane
Converts number (ID) of the target lane entity from string to [mantle_api::LaneId](https://gitlab.eclipse.org/eclipse/openpass/mantle-api/-/blob/master/include/MantleAPI/Map/lane_definitio.h)

### Coordinate System
Converts `NET_ASAM_OPENSCENARIO::v1_3::CoordinateSystem` enum to [OpenScenario::CoordinateSystem](https://code.in-tech.global/projects/PBESH/repos/openpass-openscenarioengine/browse/ose/src/nodes/utils/ConvertScenarioCoordinateSystem.h) enum.

| Subtype                | Status |
|------------------------| :----: |
| entity                 | ❌      |
| lane                   | ✔️      |
| road                   | ❌      |
| trajectory             | ❌      |

### Lane Change Target
Converts to [mantle_api::UniqueId](https://gitlab.eclipse.org/eclipse/openpass/mantle-api/-/blob/master/include/MantleAPI/Common/i_identifiable.h)

| Subtype                | Status |
|------------------------| :----: |
| RelativeTargetLane     | ✔️      |
| AbsoluteTargetLane     | ✔️      |

### Position

Converts to [mantle_api::Pose](https://gitlab.eclipse.org/eclipse/openpass/mantle-api/-/blob/master/include/MantleAPI/Common/pose.h)

| Subtype                | Status |
|------------------------| :----: |
| WorldPosition          | ✔️     |
| RelativeWorldPosition  | ❌     |
| RelativeObjectPosition | ❌     |
| RoadPosition           | ✔️     |
| RelativeRoadPosition   | ❌     |
| LanePosition           | ✔️     |
| RelativeLanePosition   | ✔️     |
| RoutePosition          | ❌     |
| GeoPosition            | ✔️     |
| TrajectoryPosition     | ❌     |

### Relative Distance Type
Converts `NET_ASAM_OPENSCENARIO::v1_3::IRelativeDistanceType` enum type to [OpenScenario::RelativeDistanceType](https://code.in-tech.global/projects/PBESH/repos/openpass-openscenarioengine/browse/ose/src/nodes/utils/ConvertScenarioRelativeDistanceType.h) enum.

### Relative Target Lane
Converts `NET_ASAM_OPENSCENARIO::v1_3::IRelativeTargetLane` type object to [mantle_api::UniqueId](https://gitlab.eclipse.org/eclipse/openpass/mantle-api/-/blob/master/include/MantleAPI/Common/i_identifiable.h) from its relative pose and the signed number of lanes that is offset from the reference entity's current lane.

### Route
Converts to [OPENSCENARIO::Route](https://code.in-tech.global/projects/PBESH/repos/openpass-openscenarioengine/browse/ose/src/nodes/utils/ConvertScenarioRoute.h)

| Subtype                | Status |
|------------------------| :----: |
| Route                  | ✔️      |
| CatalogReference       | ✔️      |

### Rule
Compares the quantitative variables or signals with the given condition and returns True or False. **Note**: Conversion rule for DateTime object type is not yet implemented.

| Type of the variable   | Status |
|------------------------| :----: |
| Double                 | ✔️      |
| String                 | ✔️      |
| DateTime               | ❌      |

### Speed Action Target
Converts to `OPENSCENARIO::SpeedActionTarget` which is of type `units::velocity::meters_per_second_t`.

| Subtype                | Status |
|------------------------| :----: |
| AbsoluteTargetSpeed    | ✔️      |
| RelativeTargetSpeed    | ✔️      |

### Continuous

| Subtype  | Status |
|----------| :----: |
| True     | ❌      |
| False    | ✔️      |

### Time Reference
Converts `NET_ASAM_OPENSCENARIO::v1_3::ITimeReference` object type to [mantle_api::FollowTrajectoryControlStrategy::TrajectoryTimeReference](https://gitlab.eclipse.org/eclipse/openpass/mantle-api/-/blob/master/include/MantleAPI/Traffic/control_strategy.h) when the timing information is taken into account. **Note**: For now only absolute time value is considered but not relative time value.

### Time to Collision Condition Target
Converts `NET_ASAM_OPENSCENARIO::v1_3::ITimeToCollisionConditionTarget` object type either of `mantle_api::Pose` or `Entity` depending on its property. **Note**: If the object has the property `Position`, then it is restricted by conversion [Conversions::Position](#position)

### Traffic Signal Controller
Takes in the `NET_ASAM_OPENSCENARIO::v1_3::ITrafficSignalController` object type and returns the name of the target object in the form of a string.

### Trajectory Reference
Converts `NET_ASAM_OPENSCENARIO::v1_3::ITrajectoryRef` object type to [mantle_api::Trajectory](https://gitlab.eclipse.org/eclipse/openpass/mantle-api/-/blob/master/include/MantleAPI/Common/trajectory.h)

| Subtype                | Status |
|------------------------| :----: |
| TrajectoryRef          | ✔️      |
| CatalogReference       | ✔️      |

### Transition Dynamics
Converts `NET_ASAM_OPENSCENARIO::v1_3::ITransitionDynamics` object type to [mantle_api::TransitionDynamics](https://gitlab.eclipse.org/eclipse/openpass/mantle-api/-/blob/master/include/MantleAPI/Traffic/control_strategy.h)
**Note**: Right now the property `followingMode` is not considered.

# Dependencies

| Dependency | Commit | Version | License |
| ---------- | ------ | ------- | ------- |
| [MantleAPI](https://gitlab.eclipse.org/eclipse/openpass/mantle-api) | ce2a378d | 11.0.0 | EPL 2.0 |
| [OpenSCENARIO API](https://github.com/RA-Consulting-GmbH/openscenario.api.test/) | 5980e88 | 1.4.0 | Apache 2.0 |
| [YASE](https://gitlab.eclipse.org/eclipse/openpass/yase) | d0c0e58d | | EPL 2.0 |
| [Units](https://github.com/nholthaus/units) | e27eed9 | 2.3.4 | MIT License |
| [googletest](https://github.com/google/googletest) | f8d7d77 | 1.14.0 | BSD-3-Clause License |
| [CPM](https://github.com/cpm-cmake/CPM.cmake) | 03705fc | 0.36.0 | MIT License |
| [openpass/stochastics-library](https://gitlab.eclipse.org/eclipse/openpass/stochastics-library) | 6c9dde71 | 0.11.0 | EPL 2.0 |

# Issues

Please report (and see) [here](https://gitlab.eclipse.org/eclipse/openpass/openscenario1_engine/-/issues).

# License

[Eclipse Public License 2.0](LICENSE)
