/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2025 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include "Conversion/OscToNode/ParseManeuver.h"

#include <memory>

#include "Conversion/OscToNode/ParseEvents.h"
#include "Node/ManeuverNode.h"

namespace OpenScenarioEngine::v1_3
{
yase::BehaviorNode::Ptr parse(std::shared_ptr<NET_ASAM_OPENSCENARIO::v1_3::IManeuver> maneuver)
{
  auto maneuver_node = std::make_shared<Node::ManeuverNode>("Maneuver");

  maneuver_node->setChild(parse(maneuver->GetEvents()));
  // parse(maneuver->GetParameterDeclarations())
  return maneuver_node;
}

}  // namespace OpenScenarioEngine::v1_3
