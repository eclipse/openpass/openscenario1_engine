/********************************************************************************
 * Copyright (c) 2022-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#include <MantleAPI/Test/test_utils.h>
#include <MantleAPI/Traffic/i_controller_config.h>
#include <gtest/gtest.h>

#include "Storyboard/GenericAction/AssignControllerAction_impl.h"

using testing::_;
using testing::Return;
using testing::ReturnRef;

static constexpr bool DONT_CARE{};

TEST(AssignControllerAction, AssignesControllerToEntity)
{
  auto mockEnvironment = std::make_shared<mantle_api::MockEnvironment>();
  mantle_api::ExternalControllerConfig controller;

  mantle_api::MockController mockController;
  ON_CALL(mockController, GetUniqueId()).WillByDefault(Return(1234));

  auto& mockControllerRepository = mockEnvironment->GetControllerRepository();
  ON_CALL(mockControllerRepository, Create(_)).WillByDefault(ReturnRef(mockController));

  EXPECT_CALL(*mockEnvironment, AddEntityToController(_, 1234));

  OpenScenarioEngine::v1_3::AssignControllerAction assignControllerAction(
      {{"Vehicle1"}, DONT_CARE, DONT_CARE, DONT_CARE, DONT_CARE, controller}, {mockEnvironment});
  assignControllerAction.Step();
}

/// Test Stub. Activate once we know, how to apply lateral and longitudinal
TEST(DISABLED_AssignControllerAction, GivenLateral_NotImplementedYet)
{
  auto mockEnvironment = std::make_shared<mantle_api::MockEnvironment>();
  mantle_api::ExternalControllerConfig controller;

  mantle_api::MockController mockController;
  ON_CALL(mockController, GetUniqueId()).WillByDefault(Return(1234));

  auto& mockControllerRepository = mockEnvironment->GetControllerRepository();
  ON_CALL(mockControllerRepository, Create(_)).WillByDefault(ReturnRef(mockController));

  EXPECT_CALL(*mockEnvironment, AddEntityToController(_, 1234));

  OpenScenarioEngine::v1_3::AssignControllerAction assignControllerAction(
      {{"Vehicle1"}, DONT_CARE, true, DONT_CARE, false, controller}, {mockEnvironment});
  assignControllerAction.Step();
}

/// Test Stub. Activate once we know, how to apply lateral and longitudinal
TEST(DISABLED_AssignControllerAction, GivenLongitudinal_NotImplementedYet)
{
  auto mockEnvironment = std::make_shared<mantle_api::MockEnvironment>();
  mantle_api::ExternalControllerConfig controller;

  mantle_api::MockController mockController;
  ON_CALL(mockController, GetUniqueId()).WillByDefault(Return(1234));

  auto& mockControllerRepository = mockEnvironment->GetControllerRepository();
  ON_CALL(mockControllerRepository, Create(_)).WillByDefault(ReturnRef(mockController));

  EXPECT_CALL(*mockEnvironment, AddEntityToController(_, 1234));

  OpenScenarioEngine::v1_3::AssignControllerAction assignControllerAction(
      {{"Vehicle1"}, DONT_CARE, false, DONT_CARE, true, controller}, {mockEnvironment});
  assignControllerAction.Step();
}
