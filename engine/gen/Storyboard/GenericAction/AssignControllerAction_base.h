/********************************************************************************
 * Copyright (c) 2021-2024 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>

#include "Conversion/OscToMantle/ConvertScenarioController.h"
#include "Conversion/OscToMantle/ConvertScenarioObjectController.h"
#include "Utils/EntityBroker.h"

namespace OpenScenarioEngine::v1_3
{

class AssignControllerActionBase
{
public:
  struct Values
  {
    Entities entities;
    bool activateAnimation;
    bool activateLateral;
    bool activateLighting;
    bool activateLongitudinal;
    Controller controller;
    ObjectController objectController;
  };
  struct Interfaces
  {
    std::shared_ptr<mantle_api::IEnvironment> environment;
  };

  AssignControllerActionBase(Values values, Interfaces interfaces)
      : values{std::move(values)},
        mantle{std::move(interfaces)} {};
  virtual ~AssignControllerActionBase() = default;
  virtual bool Step() = 0;

protected:
  Values values;
  Interfaces mantle;
};

}  // namespace OpenScenarioEngine::v1_3