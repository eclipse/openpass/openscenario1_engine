/********************************************************************************
 * Copyright (c) 2021-2025 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2023 Ansys, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

#pragma once

#include <MantleAPI/Execution/i_environment.h>

#include <optional>

#include "Conversion/OscToMantle/ConvertScenarioPosition.h"
#include "Conversion/OscToMantle/ConvertScenarioTrafficDefinition.h"
#include "Utils/IControllerService.h"

namespace OpenScenarioEngine::v1_3
{

class TrafficSinkActionBase
{
public:
  struct Values
  {
    double radius;
    double rate;
    std::function<std::optional<mantle_api::Pose>()> GetPosition;
    std::optional<TrafficDefinition> trafficDefinition;
  };
  struct Interfaces
  {
    std::shared_ptr<mantle_api::IEnvironment> environment;
  };

  struct OseServices
  {
    ControllerServicePtr controllerService;
  };

  TrafficSinkActionBase(Values values, Interfaces interfaces, OseServices ose_services)
      : values_{std::move(values)},
        mantle_{std::move(interfaces)},
        services_{std::move(ose_services)} {};
  virtual ~TrafficSinkActionBase() = default;
  virtual bool Step() = 0;

protected:
  Values values_;
  Interfaces mantle_;
  OseServices services_;
};

}  // namespace OpenScenarioEngine::v1_3
